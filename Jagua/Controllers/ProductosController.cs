﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class ProductosController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Productos
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            ViewBag.ddlCategoria = new SelectList(db.categoria_producto.ToList(), "id_cat_producto", "nombre");

            var producto = db.producto.Where(x=> x.id_tipo_producto == 1).Include(o => o.tipo_producto).Include(m => m.categoria_producto);
            return View(producto.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize)); 
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var producto = db.producto.Include(o => o.tipo_producto).Include(m => m.categoria_producto);
            var codigo = fc["txtCodigo"]; // A traves del fc, obtengo el valor de la caja de texto
            var descripcion = fc["txtDescripcion"];
			int categoria = string.IsNullOrEmpty(fc["ddlCategoria"].ToString()) ? Convert.ToInt32("0") : Convert.ToInt32(fc["ddlCategoria"].ToString());

			if (!string.IsNullOrEmpty(codigo)) // pregunto si no esta vacio la variable nombre
            {
                producto = producto.Where(q => q.codigo_barra.Contains(codigo));//pregunta si el campo es igual al filtro
            }

            if (!string.IsNullOrEmpty(descripcion)) // pregunto si no esta vacio la variable nombre
            {
                producto = producto.Where(q => q.descripcion.Contains(descripcion));//pregunta si el campo es igual al filtro
            }
            if (categoria > 0) // pregunto si no esta vacio la variable nombre
            {
                producto = producto.Where(q => q.id_cat_producto == categoria);//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtDescripcion = descripcion;
            ViewBag.txtCodigo = codigo;
			ViewBag.ddlCategoria = new SelectList(db.categoria_producto.ToList(), "id_cat_producto", "nombre", categoria);

			//retorno la vista filtrada
			return View(producto.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

		}

        
        //public ActionResult Reportes(FormCollection fc)
        //{
        //    //var x = DataTableExtensions.DataTableToCode(new Web.Gambling.Reportes.DtsReportes.DtsInfVendedores.DtInfVendedoresDataTable()); 
        //    var codigo = fc["txtCodigo"]; // A traves del fc, obtengo el valor de la caja de texto
        //    var descripcion = fc["txtDescripcion"];
        //    var categoria = fc["ddlCategoria"];

        //    foreach (var dato in db.producto.ToList())
        //    {
        //        RptListadoVendedores model = new RptListadoVendedores();
        //        model.nro = dato.id.ToString();
        //        var cli = db.clientes.Find(dato.id_cliente);
        //        model.cliente = cli.nombres + " " + cli.apellidos;
        //        model.vendedor = dato.nombres + " " + dato.apellidos;
        //        model.direccion = dato.direccion;
        //        var docTipo = db.tipos_documentos.Find(dato.id_tipo_documento);
        //        model.tipoDoc = docTipo.documento;
        //        model.nroDoc = dato.nro_documento;
        //        model.fechaNacimiento = dato.fecha_nac.ToString();
        //        var city = db.ciudades.Find(dato.id_ciudad);
        //        model.idCiudad = dato.id_ciudad.Value;
        //        model.ciudad = city.nombre;
        //        model.telefono = dato.telefono;
        //        var suc = db.sucursales.Find(dato.id_sucursal);
        //        model.idSucursal = dato.id_sucursal;
        //        model.sucursal = suc.nombre;
        //        model.estado = dato.estado;
        //        model.pais = dato.id_pais.ToString();
        //        model.departamento = dato.id_departamento.ToString();
        //        model.barrio = dato.id_barrio.ToString();

        //        list_model.Add(model);
        //    }

        //    if (codigo != string.Empty)
        //    {
        //        list_model = list_model.Where(x => x.nro == CodVendedor).ToList();
        //    }
        //    if (descripcion  != string.Empty)
        //    {
        //        list_model = list_model.Where(x => x.vendedor.ToLower().Contains(NombreVendedor)).ToList();
        //    }
        //    if (categoria != string.Empty)
        //    {
        //        list_model = list_model.Where(x => x.nro == CodDistribuidor).ToList();
        //    }
        //    if (list_model != null && list_model.Count() > 0)
        //    {
        //        Jagua.Datasets.DTables.ArticulosDataTable.Ar
        //        Web.Gambling.Reportes.DtsReportes.DtsInfVendedores dts = new Reportes.DtsReportes.DtsInfVendedores();

        //        var dt = new Jagua.Datasets.DTables.ArticulosDataTable
        //            //Web.Gambling.Reportes.DtsReportes.DtsInfVendedores.DtInfVendedoresDataTable();
        //        var row = dt.NewDtInfVendedoresRow();

        //        //usuarios d = Web.Gambling.Clases.SessionManager.SessionData;
        //        //string nombre = d.usuario;

        //        foreach (var item in list_model)
        //        {
        //            row = dt.NewDtInfVendedoresRow();
        //            row.usuarioImpresion = nombre;
        //            row.nro = item.nro;
        //            row.cliente = item.cliente;
        //            row.vendedor = item.vendedor;
        //            row.direccion = item.direccion;
        //            row.tipoDoc = item.tipoDoc;
        //            row.nroDoc = item.nroDoc;
        //            row.fechaNacimiento = item.fechaNacimiento;
        //            row.ciudad = item.ciudad;
        //            row.telefono = item.telefono;
        //            row.direccion = item.direccion;
        //            var pa = db.paises.Find(Convert.ToInt32(item.pais));
        //            row.pais = pa.nombre;
        //            var dpto = db.departamentos.Find(Convert.ToInt32(item.departamento));
        //            row.departamento = dpto.nombre;
        //            var ba = db.barrios.Find(Convert.ToInt32(item.barrio));
        //            row.barrio = item.barrio;
        //            row.sucursal = item.sucursal;
        //            row.estado = item.estado;
        //            dt.Rows.Add(row);
        //        }

        //        //Carga de los datatables en el dataset
        //        dts.Tables["Articulos"].Merge(dt);

        //        Jagua.Clases.Comun.ReportesParametros Pmt = new Jagua.Clases.Comun.ReportesParametros();
        //        Pmt.ReportPath = Server.MapPath("~/Reportes/ListadoArticulos.rpt");
        //        Pmt.ReportSource = (object)dts;
        //        Pmt.NombreArchivo = "Informe de Articulos";
        //        Jagua.Clases.Comun.ReportParameters.DatosReporte = Pmt;
                
        //    }
        //    ViewBag.codigo = codigo;
        //    ViewBag.descripcion = descripcion;
        //    ViewBag.ddlCategoria = new SelectList(db.categoria_producto.OrderBy(a => a.nombre), "id_cat_producto", "nombre", fc["ddlCategoria"]);

        //    return View(list_model.OrderByDescending(x => x.cliente).ToList().ToPagedList(1, 15));
        //}

        // GET: Productos/Create
        public ActionResult Create()
        {
            ViewBag.id_tipo_producto = new SelectList(db.tipo_producto.ToList(), "id_tipo_producto", "descripcion");
            ViewBag.id_cat_producto = new SelectList(db.categoria_producto.ToList(), "id_cat_producto", "nombre");
            return View();
        }

        // POST: Productos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_producto,id_tipo_producto,id_cat_producto,descripcion,codigo_barra,marca,iva,stock,stock_minimo,precio,precio_ult_compra,controla_stock,duracion,usuario_alta, fecha_alta, usuario_mod, fecha_mod, estado")] producto producto)
        {
            if (ModelState.IsValid)
            {
				producto.descripcion = Jagua.Clases.Comun.UppercaseFirst(producto.descripcion);
				producto.marca = Jagua.Clases.Comun.UppercaseFirst(producto.marca);
				producto.fecha_alta = DateTime.Now;
				producto.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                producto.estado = "A";
				db.producto.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        // GET: Productos/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
			else { 
            ViewBag.id_tipo_producto = new SelectList(db.tipo_producto, "id_tipo_producto", "descripcion", producto.id_tipo_producto);
            ViewBag.iva = producto.iva;
            ViewBag.controla_stock = producto.controla_stock;
			ViewBag.id_cat_producto = new SelectList(db.categoria_producto.ToList(), "id_cat_producto", "nombre", producto.id_cat_producto);
			ViewBag.activo = producto.estado;
            return View(producto);
			}
		}

        // POST: Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "id_producto,id_tipo_producto,id_cat_producto,descripcion,codigo_barra,marca,iva,stock,stock_minimo,precio,precio_ult_compra,controla_stock,duracion,usuario_alta, fecha_alta, usuario_mod, fecha_mod, estado")] producto producto)
        {
			try
			{
				if (ModelState.IsValid)
				{
					producto.descripcion = Jagua.Clases.Comun.UppercaseFirst(producto.descripcion);
					producto.marca = Jagua.Clases.Comun.UppercaseFirst(producto.marca);
					producto.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                    producto.fecha_mod = DateTime.Now;
					db.Entry(producto).State = EntityState.Modified;
					db.SaveChanges();
					return RedirectToAction("Index");
				}
            return View(producto);

			}
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
                return View();
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public void DisableConfirmed(int id)
		{
			producto producto = db.producto.Find(id);
			producto.estado = "I";
			producto.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
            producto.fecha_mod = DateTime.Now;
			db.SaveChanges();
			
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
