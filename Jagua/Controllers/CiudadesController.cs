﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class CiudadesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Ciudades
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.ddlDepartamento = new SelectList(db.departamento.ToList(), "id_departamento", "descripcion");
            var ciudad = db.ciudad.Include(d => d.departamento);
            return View(ciudad.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var ciudades = db.ciudad.ToList(); // genero una lista de ciudades
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            int departamento = string.IsNullOrEmpty(fc["ddlDepartamento"].ToString()) ? Convert.ToInt32("0") : Convert.ToInt32(fc["ddlDepartamento"].ToString());

            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                //contains es la funcion que filtra la busqueda que contiene el metodo de busqueda
                ciudades = ciudades.Where(q => q.descripcion.ToLower().Contains(nombre.ToLower())).ToList(); 
            }
            if (departamento > 0)
            {
                ciudades = ciudades.Where(x => x.id_departamento == departamento).ToList();
            }


            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = nombre;
            //devuelvo el ddl como ultimo argumento la variable de busqueda
            ViewBag.ddlDepartamento = new SelectList(db.departamento.ToList(), "id_departamento", "descripcion", departamento); 
            //retorno la vista filtrada
            return View(ciudades.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        // GET: Ciudades/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ciudad ciudad = db.ciudad.Find(id);
            if (ciudad == null)
            {
                return HttpNotFound();
            }
            return View(ciudad);
        }

        // GET: Ciudades/Create
        public ActionResult Create()
        {
            ViewBag.id_departamento = new SelectList(db.departamento, "id_departamento", "descripcion");
            return View();
        }

        // POST: Ciudades/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_ciudad,descripcion,id_departamento")] ciudad ciudad)
        {
            if (ModelState.IsValid)
            {
                ciudad.descripcion = Jagua.Clases.Comun.UppercaseFirst(ciudad.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                ciudad.estado = "A";
                ciudad.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                ciudad.fecha_alta = DateTime.Now;
                db.ciudad.Add(ciudad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_departamento = new SelectList(db.departamento, "id_departamento", "descripcion", ciudad.id_departamento);
            return View(ciudad);
        }

        // GET: Ciudades/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ciudad ciudad = db.ciudad.Find(id);
            if (ciudad == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_departamento = new SelectList(db.departamento, "id_departamento", "descripcion", ciudad.id_departamento);
            return View(ciudad);
        }

        // POST: Ciudades/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_ciudad,descripcion,id_departamento,estado,usuario_alta,fecha_alta")] ciudad ciudad)
        {
            if (ModelState.IsValid)
            {
                ciudad.descripcion = Jagua.Clases.Comun.UppercaseFirst(ciudad.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                ciudad.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                ciudad.fecha_mod = DateTime.Now;
                db.Entry(ciudad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_departamento = new SelectList(db.departamento, "id_departamento", "descripcion", ciudad.id_departamento);
            return View(ciudad);
        }

        // GET: Ciudades/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ciudad ciudad = db.ciudad.Find(id);
            if (ciudad == null)
            {
                return HttpNotFound();
            }
            return View(ciudad);
        }

        // POST: Ciudades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ciudad ciudad = db.ciudad.Find(id);
            db.ciudad.Remove(ciudad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            ciudad ciudad = db.ciudad.Find(id);
            ciudad.estado = "I";
            ciudad.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            ciudad.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
