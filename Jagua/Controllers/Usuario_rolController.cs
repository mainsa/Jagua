﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class Usuario_rolController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Usuario_rol
        public ActionResult Index(int? page)
        {

            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta
            var usuario_rol = db.usuario_rol.Include(u => u.rol).ToList();
            return View(usuario_rol.OrderBy(t => t.id_usuario_rol).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var usuario_rol = db.usuario_rol.Include(u => u.rol).ToList();
            var nombre = fc["searchString"]; // A traves del fc, obtengo el valor de la caja de texto


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                usuario_rol = usuario_rol.Where(q => q.descripcion.ToLower().Contains(nombre.ToLower()) ||
                q.usuarios.usuario.ToLower().Contains(nombre)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.searchString = nombre;

            //retorno la vista filtrada

            return View(usuario_rol.OrderBy(t => t.id_usuario_rol).ToPagedList(pageIndex, pageSize));

        }

        // GET: Usuario_rol/Create
        public ActionResult Create()
        {
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre");
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "usuario");
            return View();
        }

        // POST: Usuario_rol/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_usuario_rol,id_rol,descripcion,id_usuario")] usuario_rol usuario_rol)
        {
            if (ModelState.IsValid)
            {
                db.usuario_rol.Add(usuario_rol);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", usuario_rol.id_rol);
            return View(usuario_rol);
        }

        // GET: Usuario_rol/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            usuario_rol usuario_rol = db.usuario_rol.Find(id);
            if (usuario_rol == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", usuario_rol.id_rol);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "usuario", usuario_rol.id_usuario);
            return View(usuario_rol);
        }

        // POST: Usuario_rol/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_usuario_rol,id_rol,id_usuario,descripcion")] usuario_rol usuario_rol)
        {
            if (ModelState.IsValid)
            {
                db.Entry(usuario_rol).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", usuario_rol.id_rol);
            return View(usuario_rol);
        }

        // GET: Usuario_rol/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            usuario_rol usuario_rol = db.usuario_rol.Find(id);
            if (usuario_rol == null)
            {
                return HttpNotFound();
            }
            return View(usuario_rol);
        }

        // POST: Usuario_rol/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            usuario_rol usuario_rol = db.usuario_rol.Find(id);
            db.usuario_rol.Remove(usuario_rol);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            usuario_rol usuario_rol = await db.usuario_rol.FindAsync(id);
            if (usuario_rol == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", usuario_rol.id_rol);
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "usuario", usuario_rol.id_usuario);
            return PartialView("Details", usuario_rol);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
