﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;

namespace Jagua.Controllers
{
    public class FichasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Fichas
        public ActionResult Index(int id)
        {
            //var ficha = db.ficha.Include(f => f.paciente).Where(x => x.id_paciente == id).FirstOrDefault();
            var paciente = db.paciente.Where(x => x.id_paciente == id).FirstOrDefault().nombre; //entendes lo que hago aca?
            ViewBag.vacunas = new SelectList(db.vacuna.ToList(), "id_vacuna", "nombre");
            ViewBag.paciente = paciente;
            return View();
        }

        // GET: Fichas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ficha ficha = db.ficha.Find(id);
            if (ficha == null)
            {
                return HttpNotFound();
            }
            return View(ficha);
        }

        // GET: Fichas/Create
        public ActionResult Create()
        {
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre");
            ViewBag.id_vacuna = new SelectList(db.vacuna, "id_vacuna", "nombre");
            ViewBag.id_patologia = new SelectList(db.patologia, "id_patologia", "nombre");
            return View();
        }

        // POST: Fichas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_ficha,fecha,id_paciente,fecha_ult_visita,usuario_alta,fecha_alta")] ficha ficha,
            FormCollection form, [Bind(Include = "id_ficha_consulta,id_ficha,id_paciente,id_medico,fecha,peso,temperatura,sintomas,diagnostico,indicaciones,prox_consulta,fecha_alta,usuario_alta,usuario_mod,fecha_mod,estado")] paciente_consulta consulta,
            [Bind(Include = "id_ficha_vacuna,id_ficha,fecha,id_paciente,id_vacuna,fecha_aplicacion,prox_aplicacion,nro_dosis_restantes,usuario_alta,fecha_alta,usuario_mod,fecha_mod")] paciente_vacuna vacunas,
            //[Bind(Include = "id_ficha_lab,id_ficha,id_paciente,fecha,id_laboratorio,resultado,usuario_alta,fecha_alta,usuario_mod,fecha_mod")] paciente_laboratorio laboratorio,
            [Bind(Include = "id_ficha_lab,id_ficha,id_paciente,fecha,doctor_solicitante,archivo,resultado,usuario_alta,fecha_alta,usuario_mod,fecha_mod")] paciente_laboratorio laboratorio, //agregado por marina
            [Bind(Include = "id_ficha_pat,id_ficha,id_paciente,fecha,id_patologia,fecha_diagnostico,estado_patologia,usuario_alta,fecha_alta,usuario_mod,fecha_mod,estado")] paciente_patologia  patologia,
            [Bind(Include = "id_ficha_trat,id_ficha,id_paciente,fecha,id_patologia,id_medico,nombre_tratamiento,fecha_inicio,fecha_fin,detalles,usuario_alta,fecha_alta,usuario_mod,fecha_mod")] paciente_tratamientos tratamientos)
        {
           // if (ModelState.IsValid && !string.IsNullOrEmpty(form["id_medico"]))//este sacale 
            //{
                using (var context = new JaguaEntities())
                {

                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //FICHAS
                            ficha.fecha_alta = DateTime.Now;
                            ficha.usuario_alta = Clases.SessionManager.SessionData.usuario;
                            context.ficha.Add(ficha);
                            context.SaveChanges();

                            //PACIENTES CONSULTAS
                            consulta.id_ficha = ficha.id_ficha;
                            consulta.id_paciente = ficha.id_paciente;
                            consulta.fecha_alta = DateTime.Now;
                            consulta.estado = "A";
                            consulta.usuario_alta = Clases.SessionManager.SessionData.usuario;
                            context.paciente_consulta.Add(consulta);

                            //PACIENTES VACUNAS
                            vacunas.id_vacuna = string.IsNullOrEmpty(form["id_vacuna"]) ? 0 : int.Parse(form["id_vacuna"]);
                            if (vacunas.id_vacuna > 0)
                            {
                                vacunas.id_ficha = ficha.id_ficha;
                                vacunas.id_paciente = ficha.id_paciente;
                                vacunas.fecha = DateTime.Now;
                                //vacunas.id_vacuna = 4; //cambiar
                                //vacunas.prox_aplicacion = DateTime.Now;//cambiar por la fecha abm
                                vacunas.fecha_alta = DateTime.Now;
                                vacunas.usuario_alta = Clases.SessionManager.SessionData.usuario;
                                context.paciente_vacuna.Add(vacunas);
                            }
                            //LABORATORIO
                            if (laboratorio.paciente_laboratorio_detalle.Count() > 0) // modificado por marina porque se agrego tabla detalle
                            {
                                laboratorio.id_ficha = ficha.id_ficha;
                                laboratorio.id_paciente = ficha.id_paciente;
                                //laboratorio.id_laboratorio = 1;//cambiar
                                laboratorio.fecha_alta = DateTime.Now;
                                laboratorio.usuario_alta = Clases.SessionManager.SessionData.usuario;
                                context.paciente_laboratorio.Add(laboratorio);
                            }
                            //TRATAMIENTO
                            if (!string.IsNullOrEmpty(tratamientos.nombre_tratamiento))
                            {
                                tratamientos.id_ficha = ficha.id_ficha;
                                tratamientos.id_paciente = ficha.id_paciente;
                                tratamientos.id_patologia = patologia.id_patologia;
                                tratamientos.id_medico = int.Parse(form["id_medico"]);
                                tratamientos.fecha = DateTime.Now;
                                tratamientos.fecha_alta = DateTime.Now;
                                tratamientos.usuario_alta = Clases.SessionManager.SessionData.usuario;
                                context.paciente_tratamientos.Add(tratamientos);
                            }
                            //PATOLOGIA
                            if (patologia.id_patologia != null)
                            {
                                patologia.id_ficha = ficha.id_ficha;
                                patologia.id_paciente = ficha.id_paciente;
                                patologia.fecha = DateTime.Now;
                                //patologia.id_patologia = 5;//cambiar
                                patologia.fecha_alta = DateTime.Now;
                                patologia.usuario_alta = Clases.SessionManager.SessionData.usuario;
                                context.paciente_patologia.Add(patologia);
                            }

                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            Clases.Logger.Log.ErrorFormat("ERROR al insertar Fichas: {0}, {1}, {2}",ex.Message,ex.InnerException,ex.StackTrace);
                            
                        }
                    }

                    return RedirectToAction("Historial", "Pacientes", new { id = ficha.id_paciente });
                //}//
            }

            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", ficha.id_paciente);
            return RedirectToAction("Historial", "Pacientes", new { id = ficha.id_paciente });
        }

        // GET: Fichas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ficha ficha = db.ficha.Find(id);
            if (ficha == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", ficha.id_paciente);
            return View(ficha);
        }

        // POST: Fichas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_paciente,temperatura,kilogramos,observaciones,fecha,usuario_alta,usuario_modificacion,id_ficha")] ficha ficha)
        {
            if (ModelState.IsValid)
            {
                ficha.fecha_mod = DateTime.Now;
                ficha.usuario_modificacion = Clases.SessionManager.SessionData.usuario;
                db.Entry(ficha).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", ficha.id_paciente);
            return View(ficha);
        }

        // GET: Fichas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ficha ficha = db.ficha.Find(id);
            if (ficha == null)
            {
                return HttpNotFound();
            }
            return View(ficha);
        }

        // POST: Fichas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ficha ficha = db.ficha.Find(id);
            db.ficha.Remove(ficha);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult _VacunasHechas(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ficha ficha = db.ficha.Find(id);
            if (ficha == null)
            {
                return HttpNotFound();
            }
            return View(ficha);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
