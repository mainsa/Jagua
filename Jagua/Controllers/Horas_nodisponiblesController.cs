﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class Horas_NoDisponiblesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Horas_nodisponibles
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.ddlUsuario = new SelectList(db.usuarios.ToList(), "id_usuario", "nombres", "apellidos");
            var hs = db.horas_nodisponibles.Include(o => o.usuarios);

            //return View(hs.OrderByDescending(n => n.fecha_desde).ToPagedList(pageIndex, pageSize));            
            return View(hs.OrderByDescending(n => n.id_usuario).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            //var hs = db.horas_nodisponibles.Include(o => o.usuario);
            var hs = db.horas_nodisponibles.ToList();

            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            var apellido = fc["txtApellido"];

            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                //hs = hs.Where(q => q.usuario.nombre.ToLower().Contains(nombre));//pregunta si el campo es igual al filtro
                hs = hs.Where(q => q.usuarios.nombres.ToLower().Contains(nombre.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(apellido)) // pregunto si no esta vacio la variable nombre
            {
                //hs = hs.Where(q => q.usuario.apellido.ToLower().Contains(apellido);//pregunta si el campo es igual al filtro
                hs = hs.Where(q => q.usuarios.apellidos.ToLower().Contains(apellido.ToLower())).ToList();
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = nombre; 
            ViewBag.txtApellido = apellido;

            //retorno la vista filtrada
            return View(hs.OrderByDescending(n => n.fecha_desde).ToPagedList(pageIndex, pageSize));

        }

        // GET: Horas_nodisponibles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            horas_nodisponibles horas_nodisponibles = db.horas_nodisponibles.Find(id);
            if (horas_nodisponibles == null)
            {
                return HttpNotFound();
            }
            return View(horas_nodisponibles);
        }

        // GET: Horas_nodisponibles/Create
        public ActionResult Create()
        {
            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombres", "apellidos");
            return View();
        }

        // POST: Horas_nodisponibles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_hs_nodisp,id_usuario,fecha_desde,fecha_hasta,hora_desde,hora_hasta,motivo")] horas_nodisponibles horas_nodisponibles)
        {
            if (ModelState.IsValid)
            {
                db.horas_nodisponibles.Add(horas_nodisponibles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "nombres","apellidos", horas_nodisponibles.id_usuario);
            return View(horas_nodisponibles);
        }

        // GET: Horas_nodisponibles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            horas_nodisponibles horas_nodisponibles = db.horas_nodisponibles.Find(id);
            if (horas_nodisponibles == null)
            {
                return HttpNotFound();
            }
            return View(horas_nodisponibles);
        }

        // POST: Horas_nodisponibles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_hs_nodisp,id_usuario,fecha_desde,fecha_hasta,hora_desde,hora_hasta,motivo")] horas_nodisponibles horas_nodisponibles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(horas_nodisponibles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(horas_nodisponibles);
        }

        // GET: Horas_nodisponibles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            horas_nodisponibles horas_nodisponibles = db.horas_nodisponibles.Find(id);
            if (horas_nodisponibles == null)
            {
                return HttpNotFound();
            }
            return View(horas_nodisponibles);
        }

        // POST: Horas_nodisponibles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            horas_nodisponibles horas_nodisponibles = db.horas_nodisponibles.Find(id);
            db.horas_nodisponibles.Remove(horas_nodisponibles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
