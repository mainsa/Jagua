﻿using Jagua.Models;
using JaguaEntityModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jagua.Controllers
{
    public class StockController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Stock
        [HttpGet]
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.ddlCategoria = new SelectList(db.categoria_producto.ToList(), "id_cat_producto", "nombre");

            List<Stock> lista = new List<Stock>();
            var query = from a in db.producto
                        join b in db.categoria_producto on a.id_tipo_producto equals b.id_cat_producto
                        where a.id_tipo_producto == 1
                        select new Stock
                        {
                            id_producto = a.id_producto,
                            codigo_barra = a.codigo_barra,
                            id_cat_producto = a.id_cat_producto,
                            categoria = b.nombre,
                            marca = a.marca,
                            descripcion = a.descripcion,
                            //stock = string.Format(System.Globalization.CultureInfo.CreateSpecificCulture("es-ES"), "{0:#,##0.##}", a.stock), String.Format("{0:0.##}", 123.0);       
                            stock = a.stock,
                            precio_ult_compra = a.precio_ult_compra
                        };
            lista = query.OrderBy(n => n.descripcion).ToList();

            CargarReporte(lista);

            return View(lista.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc)
        {
            int pageSize = 10;
            int pageIndex = 1;

            List<Stock> lista = new List<Stock>();
            var query = from a in db.producto
                        join b in db.categoria_producto on a.id_tipo_producto equals b.id_cat_producto
                        where a.id_tipo_producto == 1
                        select new Stock
                        {
                            id_producto = a.id_producto,
                            codigo_barra = a.codigo_barra,
                            id_cat_producto = a.id_cat_producto,
                            categoria = b.nombre,
                            marca = a.marca,
                            descripcion = a.descripcion,
                            stock = a.stock,
                            precio = a.precio,
                            precio_ult_compra = a.precio_ult_compra
                        };
            lista = query.OrderBy(n => n.descripcion).ToList();

            var codigo = fc["txtCodigo"]; // A traves del fc, obtengo el valor de la caja de texto
            var descripcion = fc["txtDescripcion"];
            var categoria = fc["ddlCategoria"];

            if (!string.IsNullOrEmpty(codigo)) // pregunto si no esta vacio la variable nombre
            {
                lista = lista.Where(q => q.codigo_barra.Contains(codigo)).ToList();//pregunta si el campo es igual al filtro
            }

            if (!string.IsNullOrEmpty(descripcion)) // pregunto si no esta vacio la variable nombre
            {
                lista = lista.Where(q => q.descripcion.ToLower().Contains(descripcion.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(categoria)) // pregunto si no esta vacio la variable nombre
            {
                lista = lista.Where(q => q.id_cat_producto == Convert.ToInt32(categoria)).ToList();//pregunta si el campo es igual al filtro
            }

            CargarReporte(lista);

            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtDescripcion = descripcion;
            ViewBag.txtCodigo = codigo;
            ViewBag.ddlCategoria = new SelectList(db.categoria_producto.ToList(), "id_cat_producto", "nombre", categoria);

            //retorno la vista filtrada
            return View(lista.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        public void CargarReporte(List<Stock> lista)
        {
            Jagua.Datasets.Reportes dts = new Jagua.Datasets.Reportes();
            var dt = new Jagua.Datasets.Reportes.StockDataTable();
            var row = dt.NewStockRow();

            foreach (var item in lista)
            {
                row = dt.NewStockRow();
                row.id_producto = item.id_producto;
                row.codigo_barra = item.codigo_barra;
                row.categoria = item.categoria;
                row.marca = item.marca;
                row.descripcion = item.descripcion;
                row.stock = item.stock != null ? Convert.ToInt32(item.stock) : 0;
                row.precio = item.precio != null ? Convert.ToInt32(item.precio) : 0;
                row.precio_ult_compra = item.precio_ult_compra != null ? Convert.ToInt32(item.precio_ult_compra) : 0;
                row.usuario_impresion = Jagua.Clases.SessionManager.SessionData.usuario;
                dt.Rows.Add(row);
            }

            //Carga de los datatables en el dataset
            dts.Tables["Stock"].Merge(dt);

            Jagua.Clases.Comun.ReportesParametros Pmt = new Jagua.Clases.Comun.ReportesParametros();
            Pmt.ReportPath = Server.MapPath("~/Reportes/Stock.rpt");
            Pmt.ReportSource = (object)dts;
            Pmt.NombreArchivo = "Informe de Stock";
            Jagua.Clases.Comun.ReportParameters.DatosReporte = Pmt;

            /* Web.Gambling.Reportes.DtsReportes.DtsInfVendedores dts = new Reportes.DtsReportes.DtsInfVendedores();
             var dt = new Web.Gambling.Reportes.DtsReportes.DtsInfVendedores.DtInfVendedoresDataTable();
             var row = dt.NewDtInfVendedoresRow();

             foreach (var item in list_model)
             {
                 row = dt.NewDtInfVendedoresRow();
                 row.usuarioImpresion = nombre;
                 row.nro = item.nro;
                 dt.Rows.Add(row);
             }

             //Carga de los datatables en el dataset
             dts.Tables["DtInfVendedores"].Merge(dt);
             Web.Gambling.Clases.ReportesParametros Pmt = new Web.Gambling.Clases.ReportesParametros();
             Pmt.ReportPath = Server.MapPath("~/Reportes/RptInfVendedores.rpt");
             Pmt.ReportSource = (object)dts;
             Pmt.NombreArchivo = "Informe de Vendedores";
             Web.Gambling.Clases.ReportParameters.DatosReporte = Pmt;*/
        }


        // GET: Cajas
        public ActionResult Reporte()
        {
            return View();
        }
    }
}