﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class ProveedoresController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Proveedores
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var prov = db.proveedor.Include(o => o.proveedor_timbrado).ToList();

            return View(prov.OrderBy(n => n.razon_social).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var proveedor = db.proveedor.Include(o => o.proveedor_timbrado).ToList();
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            var ruc = fc["txtRuc"];


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                proveedor = proveedor.Where(q => q.razon_social.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(ruc)) // pregunto si no esta vacio la variable nombre
            {
                proveedor = proveedor.Where(q => q.ruc == ruc).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = nombre;
            ViewBag.txtRuc = ruc;

            //retorno la vista filtrada
            return View(proveedor.OrderBy(n => n.razon_social).ToPagedList(pageIndex, pageSize));

        }

        // GET: Proveedores/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            proveedor proveedor = db.proveedor.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            return View(proveedor);
        }

        // GET: Proveedores/Create
        public ActionResult Create()
        {
            ViewBag.id_timbrado = new SelectList(db.proveedor_timbrado.ToList(), "id_prov_timbrado", "timbrado");
            return View();
        }

        // POST: Proveedores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_proveedor,razon_social,ruc,direccion,email,contacto,telefono,id_timbrado")] proveedor proveedor)
        {
            if (ModelState.IsValid)
            {
				proveedor.razon_social = Jagua.Clases.Comun.UppercaseFirst(proveedor.razon_social);
				proveedor.direccion = Jagua.Clases.Comun.UppercaseFirst(proveedor.direccion);
				proveedor.email = (!string.IsNullOrEmpty(proveedor.email)) ? proveedor.email.ToLower():"";
				proveedor.estado = "A";
                proveedor.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                proveedor.fecha_alta = DateTime.Now;
                db.proveedor.Add(proveedor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proveedor);
        }

        // GET: Proveedores/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            proveedor proveedor = db.proveedor.Find(id);
            if (proveedor == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_timbrado = new SelectList(db.proveedor_timbrado.ToList(), "id_prov_timbrado", "timbrado", proveedor.id_timbrado);

            return View(proveedor);
        }

        // POST: Proveedores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "id_proveedor,razon_social,ruc,direccion,email,contacto,telefono,id_timbrado,estado,usuario_alta,fecha_alta")] proveedor proveedor)
        {
            if (ModelState.IsValid)
            {
				proveedor.razon_social = Jagua.Clases.Comun.UppercaseFirst(proveedor.razon_social);
				proveedor.direccion = Jagua.Clases.Comun.UppercaseFirst(proveedor.direccion);
				proveedor.email = proveedor.email.ToLower();
				proveedor.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                proveedor.fecha_mod = DateTime.Now;
				db.Entry(proveedor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proveedor);
        }

		[HttpPost]
		[ValidateAntiForgeryToken]
		public void DisableConfirmed(int id)
		{
			proveedor proveedor = db.proveedor.Find(id);
			proveedor.estado = "I";
			proveedor.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
            proveedor.fecha_mod = DateTime.Now;
			db.SaveChanges();
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
