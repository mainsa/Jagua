﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class VacunasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Vacunas
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            return View(db.vacuna.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var vacuna = db.vacuna.ToList();
            var nombre = fc["txtNombre"].ToLower(); // A traves del fc, obtengo el valor de la caja de texto


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                vacuna = vacuna.Where(q => q.descripcion.ToLower().Contains(nombre)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = fc["txtNombre"];

            //retorno la vista filtrada
            return View(vacuna.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }


        // GET: Vacunas/Create
        public ActionResult Create()
        {
            ViewBag.id_especie = new SelectList(db.especie.ToList(), "id_especie", "nombre");
            return View();
        }

        // POST: Vacunas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_vacuna,nombre,descripcion,edad,tiempo,id_especie")] vacuna vacuna)
        {
            if (ModelState.IsValid)
            {
                vacuna.nombre = Jagua.Clases.Comun.UppercaseFirst(vacuna.nombre);
                vacuna.descripcion = Jagua.Clases.Comun.UppercaseFirst(vacuna.descripcion);
                vacuna.estado = "A";
                vacuna.fecha_alta = DateTime.Now;
                vacuna.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                db.vacuna.Add(vacuna);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vacuna);
        }

        // GET: Vacunas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vacuna vacuna = db.vacuna.Find(id);
            if (vacuna == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.id_especie = new SelectList(db.especie.ToList(), "id_especie", "nombre", vacuna.id_especie);
                return View(vacuna);
            }
           
        }

        // POST: Vacunas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_vacuna,nombre,descripcion,edad,tiempo,id_especie,estado,usuario_alta,fecha_alta")] vacuna vacuna)
        {
            if (ModelState.IsValid)
            {
                vacuna.nombre = Jagua.Clases.Comun.UppercaseFirst(vacuna.nombre);
                vacuna.descripcion = Jagua.Clases.Comun.UppercaseFirst(vacuna.descripcion);
                vacuna.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
                vacuna.fecha_mod = DateTime.Now;
                db.Entry(vacuna).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vacuna);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            vacuna vacuna = db.vacuna.Find(id);
            vacuna.estado = "I";
            vacuna.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            vacuna.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
