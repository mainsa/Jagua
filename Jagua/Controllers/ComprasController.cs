﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jagua.Models;
using PagedList;
using JaguaEntityModel;

namespace Jagua.Controllers
{
    public class ComprasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Compras
        [HttpGet]
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            List<Models.Compras> lista = new List<Models.Compras>();
            var query = from a in db.compras
                        join b in db.compras_detalle on a.id_compras equals b.id_compras_cabecera
                        join c in db.producto on b.id_producto equals c.id_producto
                        join d in db.proveedor on a.id_proveedor equals d.id_proveedor
                        select new Compras
                        {
                            id_compras = a.id_compras,
                            fecha = a.fecha,
                            nro_factura = a.nro_factura,
                            ruc = d.ruc,
                            razon_social = d.razon_social,
                            tipo_pago = a.tipo_pago,
                            importe_total = a.importe_total,
                            estado = a.estado,
                            usuario_alta = a.usuario_alta,
                            fecha_alta = a.fecha_alta
                        };
            lista = query.OrderByDescending(n => n.fecha).ToList();

            CargarReporte(lista);

            return View(lista.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(int? page, FormCollection fc)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            List<Models.Compras> lista = new List<Models.Compras>();
            var query = from a in db.compras
                        join b in db.compras_detalle on a.id_compras equals b.id_compras_cabecera
                        join d in db.proveedor on a.id_proveedor equals d.id_proveedor
                        select new Compras
                        {
                            id_compras = a.id_compras,
                            fecha = a.fecha,
                            nro_factura = a.nro_factura,
                            ruc = d.ruc,
                            razon_social = d.razon_social,
                            tipo_pago = a.tipo_pago,
                            importe_total = a.importe_total,
                            estado = a.estado,
                            usuario_alta = a.usuario_alta,
                            fecha_alta = a.fecha_alta
                        };
            lista = query.OrderByDescending(n => n.fecha).ToList();

            var factura = fc["txtFactura"];
            var proveedor = fc["txtProveedor"];
            var estado = fc["ddlEstado"];

            if (!string.IsNullOrEmpty(factura)) // pregunto si no esta vacio la variable nombre
            {
                lista = lista.Where(q => q.nro_factura.Contains(factura)).ToList();
            }
            if (!string.IsNullOrEmpty(proveedor)) // pregunto si no esta vacio la variable nombre
            {
                lista = lista.Where(q => q.razon_social.ToLower().Contains(proveedor.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable nombre
            {
                lista = lista.Where(q => q.estado.ToLower() == estado.ToLower()).ToList();
            }

            ViewBag.ddlEstado = estado;

            CargarReporte(lista);

            return View(lista.ToPagedList(pageIndex, pageSize));
        }

        public void CargarReporte(List<Compras> lista)
        {
            Jagua.Datasets.Reportes dts = new Jagua.Datasets.Reportes();
            var dt = new Jagua.Datasets.Reportes.ComprasDataTable();
            var row = dt.NewComprasRow();

            foreach (var item in lista)
            {
                row = dt.NewComprasRow();
                row.id_compras = item.id_compras;
                row.fecha = item.fecha;
                row.nro_factura = item.nro_factura;
                row.ruc = item.ruc;
                row.razon_social = item.razon_social;
                row.tipo_pago = item.tipo_pago;
                row.importe_total = item.importe_total;
                row.estado= item.estado;
                row.usuario_impresion = Jagua.Clases.SessionManager.SessionData.usuario;
                dt.Rows.Add(row);
            }

            //Carga de los datatables en el dataset
            dts.Tables["Compras"].Merge(dt);

            Jagua.Clases.Comun.ReportesParametros Pmt = new Jagua.Clases.Comun.ReportesParametros();
            Pmt.ReportPath = Server.MapPath("~/Reportes/Compras.rpt");
            Pmt.ReportSource = (object)dts;
            Pmt.NombreArchivo = "Informe de Compras";
            Jagua.Clases.Comun.ReportParameters.DatosReporte = Pmt;
        }


        // GET: Cajas
        public ActionResult Reporte()
        {
            return View();
        }

        // GET: Compras/Create
        public ActionResult Create()
        {
            return View();
        }
        //POST: Compras/Create
        [HttpPost]
        public JsonResult Create(compras datos, compras_detalle[] datosDetalle)
        {
            try
            {
                if (datos != null && datosDetalle != null)
                {
                    datos.estado = "C";
                    datos.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                    datos.fecha_alta = DateTime.Now;
                    db.compras.Add(datos);
                    db.SaveChanges();

                    //se guarda el detalle
                    foreach (var datosdetalle in datosDetalle)
                    {
                        var guardarDetalle = new compras_detalle();
                        guardarDetalle.id_compras_cabecera = datos.id_compras;
                        guardarDetalle.id_producto = datosdetalle.id_producto;
                        guardarDetalle.producto = datosdetalle.producto;
                        guardarDetalle.cantidad = datosdetalle.cantidad;
                        guardarDetalle.precio_unitario = datosdetalle.precio_unitario;
                        guardarDetalle.iva10 = datosdetalle.iva10;
                        guardarDetalle.iva5 = datosdetalle.iva5;
                        guardarDetalle.exentas = datosdetalle.exentas;
                        guardarDetalle.gravadas = datosdetalle.gravadas;
                        guardarDetalle.subtotal = datosdetalle.subtotal;
                        db.compras_detalle.Add(guardarDetalle);
                        //Se agrega al stock las compras
						var producto = db.producto.Where(x => x.id_producto == datosdetalle.id_producto).FirstOrDefault();
						producto.stock = producto.stock == null ? datosdetalle.cantidad : (producto.stock + datosdetalle.cantidad);
						producto.precio_ult_compra = datosdetalle.precio_unitario;
						db.Entry(producto).State = EntityState.Modified;
						db.SaveChanges();
					}
                    db.SaveChanges();
                    return Json(new { Status = "Success" });
                }
                else
                {
                    ModelState.AddModelError("", "No se puede registrar los cabecera sin detalle de ventas ");
                    var result = "0";
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
                var result = "1";
                return Json(result);
            }
        }


        public JsonResult ProductDetails(string search)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var ProductosList = (from N in db.producto
                                     where (N.id_producto.ToString().Contains(search) || N.descripcion.ToLower().Contains(search.ToLower()) || N.codigo_barra.Contains(search))
                                     select new { N.id_producto, N.descripcion});
                return Json(ProductosList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        public void Anular(int id)
        {
            compras compras = db.compras.Find(id);
            compras.estado = "A";
            compras.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            compras.fecha_mod = DateTime.Now;
            db.SaveChanges();

            //Descontar stock
            var detalle = db.compras_detalle.ToList().Where(x => x.id_compras_cabecera == compras.id_compras);
            foreach (var item in detalle)
            {
                var producto = db.producto.Where(x => x.id_producto == item.id_producto).FirstOrDefault();
                producto.stock = producto.stock >= item.cantidad ? (producto.stock - item.cantidad) : 0;
                producto.precio_ult_compra = 0;
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
