﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class PaginasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: paginas
        public async Task<ActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta
            var datos = await db.paginas.OrderBy(n => n.pagina).ToListAsync();
            return View(datos.ToPagedList(pageIndex, pageSize));
        

        }
        [HttpPost]
        public async Task<ActionResult> Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var datos = await db.paginas.OrderBy(n => n.pagina).ToListAsync();
            var nombre = fc["searchString"]; // A traves del fc, obtengo el valor de la caja de texto


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                datos = datos.Where(q => q.pagina.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.searchString = nombre;

            //retorno la vista filtrada
            return View(datos.ToPagedList(pageIndex, pageSize));

        }

        // GET: paginas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: paginas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id_pagina,pagina,url")] paginas paginas)
        {
            if (ModelState.IsValid)
            {
                db.paginas.Add(paginas);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(paginas);
        }

        // GET: paginas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paginas paginas = await db.paginas.FindAsync(id);
            if (paginas == null)
            {
                return HttpNotFound();
            }
            return View(paginas);
        }

        // POST: paginas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id_pagina,pagina,url")] paginas paginas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paginas).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(paginas);
        }

        // GET: paginas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paginas paginas = await db.paginas.FindAsync(id);
            if (paginas == null)
            {
                return HttpNotFound();
            }
            return View(paginas);
        }

        // POST: paginas/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            paginas paginas = await db.paginas.FindAsync(id);
            db.paginas.Remove(paginas);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
