﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    [ControlFilter]
    public class ClientesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Clientes Categoria de persona para cliente es 1
        public ActionResult Index(int? page, string currentFilter)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            ViewBag.CurrentFilter = currentFilter;
            var lista = db.persona.Include(o => o.tipo_documento).Include(x => x.cliente).OrderByDescending(n => n.nombre).Where(x => x.id_cat_persona == 1).ToList();
            if (SessionManager.SessionData.usuario_rol.First().rol.nombre.ToLower().Equals("cliente"))
            {
                lista = lista.Where(t => t.cliente.Any(c => c.id_cliente.Equals(SessionManager.SessionData.id_cliente))).ToList();
            }
            if (!string.IsNullOrEmpty(currentFilter))
            {
                var ArrcurrentFilter = currentFilter.Split('_');
                if (ArrcurrentFilter.Length > 0)
                {
                    var cedula = ArrcurrentFilter[0];
                    var nombre = ArrcurrentFilter[1]; // A traves del fc, obtengo el valor de la caja de texto
                    var apellido = ArrcurrentFilter[2];
                    var estado = ArrcurrentFilter[3];
                    if (!string.IsNullOrEmpty(cedula)) // pregunto si no esta vacio la variable nombre
                    {
                        lista = lista.Where(q => q.nro_documento == cedula).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
                    {
                        lista = lista.Where(q => q.nombre.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(apellido)) // pregunto si no esta vacio la variable nombre
                    {
                        lista = lista.Where(q => q.apellido.ToLower().Contains(apellido.ToLower())).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable nombre
                    {
                        lista = lista.Where(q => q.estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
                    }
                    ViewBag.txtCedula = cedula;
                    ViewBag.txtNombre = nombre;
                    ViewBag.txtApellido = apellido;
                    ViewBag.estado = estado;

                }
            }
            ViewBag.id_promocion = new SelectList(db.promocion, "id_promocion", "descripcion");
            return View(lista.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            string estado = string.IsNullOrEmpty(fc["ddlEstado"]) ? "" : fc["ddlEstado"];

            var clientes = db.persona.Where(x => x.id_cat_persona == 1).ToList();
            if (SessionManager.SessionData.usuario_rol.First().rol.nombre.ToLower().Equals("cliente"))
            {
                clientes = clientes.Where(t => t.cliente.Any(c => c.id_cliente.Equals(SessionManager.SessionData.id_cliente))).ToList();
            }
            var cedula = fc["txtCedula"];
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            var apellido = fc["txtApellido"];


            if (!string.IsNullOrEmpty(cedula)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.nro_documento == cedula).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.nombre.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(apellido)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.apellido.ToLower().Contains(apellido.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtCedula = cedula;
            ViewBag.txtNombre = nombre;
            ViewBag.txtApellido = apellido;
            ViewBag.estado = estado;
            ViewBag.CurrentFilter = cedula + "_" + nombre + "_" + apellido + "_" + estado;
            ViewBag.id_promocion = new SelectList(db.promocion, "id_promocion", "descripcion");
            //retorno la vista filtrada
            return View(clientes.OrderByDescending(n => n.nombre).ToPagedList(pageIndex, pageSize));

        }
        // GET: Clientes/Create
        public ActionResult Create()
        {
            ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion");
            ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion");
            ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion");
            ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion");
            ViewBag.nacionalidad = new SelectList(db.pais, "id_pais", "gentilicio");
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento, "id_tipo_documento", "abreviatura");
            ViewBag.estado = "A";
            return View();
        }

        // POST: Clientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_persona,nombre,apellido,id_tipo_documento,nro_documento,nacionalidad,id_barrio,id_ciudad,email,direccion,nro_telefono,nro_celular,sexo")] persona cliente, FormCollection datos)
        {
            if (ModelState.IsValid)
            {
                string estado = string.IsNullOrEmpty(datos["ddlEstado"]) ? "A" : datos["ddlEstado"];
                //desmenuzo el objeto para darle formato de primera letra en mayuscula
                cliente.nombre = Jagua.Clases.Comun.UppercaseFirst(cliente.nombre);
                cliente.apellido = Jagua.Clases.Comun.UppercaseFirst(cliente.apellido);
                cliente.direccion = Jagua.Clases.Comun.UppercaseFirst(cliente.direccion);
                cliente.id_cat_persona = 1;
                cliente.estado = estado;
                cliente.fecha_alta = DateTime.Now;
                cliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();

                JaguaEntityModel.cliente datosCliente = new JaguaEntityModel.cliente();
                datosCliente.id_persona = cliente.id_persona;
                datosCliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                datosCliente.fecha_alta = DateTime.Now;
                datosCliente.estado = estado;

                db.cliente.Add(datosCliente);
                db.persona.Add(cliente);
                db.SaveChanges();
                ViewBag.estado = estado;
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            persona cliente = db.persona.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            if (SessionManager.isSessionValid)
                ViewBag.usuarioRol = SessionManager.SessionData.usuario_rol.First().rol.nombre.ToLower();
            ciudad ciudad = db.ciudad.Find(cliente.id_ciudad);
            if (cliente.id_barrio != null)
            { //Pregunto si esta cargado el barrio para traer los demas datos
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion", cliente.id_barrio);
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion", cliente.barrio.id_ciudad);
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion", cliente.barrio.ciudad.id_departamento);
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion", cliente.barrio.ciudad.departamento.id_pais);
            }
            else if (ciudad != null)
            {
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion", ciudad.departamento.id_pais);
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion", ciudad.id_departamento);
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion", cliente.id_ciudad);
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion", cliente.id_barrio);
            }
            else
            {
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion");
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion");
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion");
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion");
            }
            ViewBag.estado = cliente.estado;
            var nac = db.pais.Where(x => x.gentilicio == cliente.nacionalidad).FirstOrDefault();
            var gentilicio = nac != null ? Convert.ToInt32(nac.id_pais) : 1;
            ViewBag.nacionalidad = new SelectList(db.pais, "id_pais", "gentilicio", gentilicio);
            ViewBag.estado = cliente.estado;
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento, "id_tipo_documento", "abreviatura", cliente.id_tipo_documento);

            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "id_persona,nombre,apellido,id_tipo_documento,nro_documento,nacionalidad,id_barrio,id_ciudad,email,direccion,nro_telefono,nro_celular,sexo,id_cat_persona,estado")] persona cliente, FormCollection datos)
        {
            if (ModelState.IsValid)
            {
                string estado = string.IsNullOrEmpty(datos["ddlEstado"]) ? "A" : datos["ddlEstado"];
                cliente.estado = estado;
                cliente.fecha_mod = DateTime.Now;
                cliente.usuario_mod = SessionManager.SessionData.usuario;

                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.estado = cliente.estado;
            return View(cliente);
        }
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            persona cliente = await db.persona.FindAsync(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.estado = cliente.estado;
            ciudad ciudad = await db.ciudad.FindAsync(cliente.id_ciudad);
            if (cliente.id_barrio != null)
            { //Pregunto si esta cargado el barrio para traer los demas datos
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion", cliente.id_barrio);
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion", cliente.barrio.id_ciudad);
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion", cliente.barrio.ciudad.id_departamento);
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion", cliente.barrio.ciudad.departamento.id_pais);
            }
            else if (ciudad != null)
            {
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion", ciudad.departamento.id_pais);
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion", ciudad.id_departamento);
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion", cliente.id_ciudad);
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion", cliente.id_barrio);
            }
            else
            {
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion");
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion");
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion");
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion");
            }

            var nac = db.pais.Where(x => x.gentilicio == cliente.nacionalidad).FirstOrDefault();
            var gentilicio = nac != null ? Convert.ToInt32(nac.id_pais) : 1;
            ViewBag.nacionalidad = new SelectList(db.pais, "id_pais", "gentilicio", gentilicio);
            ViewBag.estado = cliente.estado;
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento, "id_tipo_documento", "abreviatura", cliente.id_tipo_documento);
            return PartialView("Details", cliente);
        }

        [HttpPost]
        public ActionResult DisableConfirmed(int id)
        {
            bool success = true;
            string mensaje = string.Empty;
            try
            {
                persona persona = db.persona.Find(id);
                cliente cliente = db.cliente.Where(x => x.id_persona == id).FirstOrDefault();


                persona.estado = "I";
                persona.usuario_mod = SessionManager.SessionData.id_usuario.ToString();
                persona.fecha_mod = DateTime.Now;

                cliente.estado = "I";
                cliente.usuario_mod = SessionManager.SessionData.id_usuario.ToString();
                cliente.fecha_mod = DateTime.Now;

                db.SaveChanges();
                success = true;
                mensaje = "El registro fue desactivado";
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "Error al desactivar clientes!";
                Clases.Logger.Log.ErrorFormat("Error al buscar DatosCalendario: {0}, {1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [HttpPost]
        public ActionResult PuntosCanjeados(int idCliente)
        {
            bool success = true;
            string mensaje = string.Empty;
            string nombre = string.Empty;
            string puntos = string.Empty;
            string puntosCanje = string.Empty;
            List<puntaje_cabecera> puntajes_ = new List<puntaje_cabecera>();
            try
            {
                puntajes_ = db.puntaje_cabecera.Where(t => t.id_cliente.Equals(idCliente)).ToList();
                if (puntajes_.Any())
                {
                    var cliente = puntajes_.FirstOrDefault().cliente.persona;
                    var puntosCanjeados = (from pc in db.puntaje_canjeado
                                           join p in db.puntaje_cabecera on pc.id_puntaje equals p.id_puntaje
                                           where p.id_cliente == idCliente
                                           select pc).ToList();
                    nombre = cliente.nombre + " " + cliente.apellido;
                    puntos = puntajes_.Sum(t => t.cantidad_puntos).ToString("N0");
                    puntosCanje = puntosCanjeados.Sum(t => t.cantidad).Value.ToString("N0");
                    if (int.Parse(puntos) >= int.Parse(puntosCanje))
                        puntos = (int.Parse(puntos) - int.Parse(puntosCanje)).ToString("N0");
                    else
                        puntos = (int.Parse(puntosCanje) - int.Parse(puntos)).ToString("N0");
                }
                else
                {
                    success = false;
                    mensaje = "No tiene puntos!";
                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "Error al buscar PuntosCanjeados";
                Clases.Logger.Log.ErrorFormat("Error al buscar DatosCalendario: {0}, {1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje, nombre, puntos, puntosCanje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CanjearPuntos(int idCliente, int canjear, int idPromocion)
        {
            bool success = true;
            string mensaje = string.Empty;
            
            List<puntaje_cabecera> puntajes_ = new List<puntaje_cabecera>();
            try
            {
                puntajes_ = db.puntaje_cabecera.Where(t => t.id_cliente.Equals(idCliente)).ToList();//buscamos los puntos del cliente
                if (puntajes_.Any())
                {
                    var puntos = puntajes_.Sum(t => t.cantidad_puntos);//cuantos puntos en total tiene
                    var objCanjeados = (from pc in db.puntaje_canjeado
                                        join p in db.puntaje_cabecera on pc.id_puntaje equals p.id_puntaje
                                        where p.id_cliente == idCliente
                                        select pc).ToList();

                    var puntosCanje = objCanjeados.Sum(t => t.cantidad).Value;
                    if (puntos >= puntosCanje)
                        puntos = int.Parse(puntos.ToString()) - puntosCanje;
                    else
                        puntos = puntosCanje - int.Parse(puntos.ToString());
                    if (puntos >= canjear)//si es mayor o igual puede seguir 3 usare 2
                    {
                        puntaje_canjeado puntosCanjeados;
                        puntaje_cabecera puntajecab;
                        int canjeados = 0;
                        int cantidad = 0;
                        foreach (var item in puntajes_)//recorremos y vamos insertando los puntos 
                        {
                            if (canjear > canjeados)//si es mayo a 0 sigue agregando de los puntos que faltan
                            {
                                if (int.Parse(item.cantidad_puntos.ToString()) >= canjear)
                                    cantidad = canjear;
                                else
                                    cantidad = int.Parse(item.cantidad_puntos.ToString());
                                
                                puntosCanjeados = new puntaje_canjeado();
                                puntosCanjeados.id_puntaje = item.id_puntaje;
                                puntosCanjeados.id_promocion = idPromocion;
                                puntosCanjeados.cantidad = cantidad;
                                puntosCanjeados.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                puntosCanjeados.fecha_alta = DateTime.Now;
                                db.puntaje_canjeado.Add(puntosCanjeados);
                                db.SaveChanges();
                                canjeados += cantidad;

                                //Comentado porque resta del original, trio dinamico
                                //puntajecab = db.puntaje_cabecera.Find(item.id_puntaje);
                                //if (puntajecab != null)
                                //{
                                //    if (int.Parse(item.cantidad_puntos.ToString()) >= cantidad)
                                //        puntajecab.cantidad_puntos = int.Parse(item.cantidad_puntos.ToString()) - cantidad;
                                //    else
                                //        puntajecab.cantidad_puntos = int.Parse(item.cantidad_puntos.ToString()) - cantidad;
                                //    puntajecab.usuario_mod = SessionManager.SessionData.id_usuario.ToString();
                                //    puntajecab.fecha_mod = DateTime.Now;
                                //    db.Entry(puntajecab).State = EntityState.Modified;
                                //    db.SaveChanges();
                                //}
                                
                            }
                            else
                                break;
                       }
                        success = true;
                        mensaje = "Los puntos se canjearon correctamente!";
                    }
                    else
                    {
                        success = false;
                        mensaje = "Los puntos no son suficientes para el canje!";
                    }

                }
                else
                {
                    success = false;
                    mensaje = "No tiene puntos!";
                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "Error al buscar PuntosCanjeados";
                Clases.Logger.Log.ErrorFormat("Error al buscar DatosCalendario: {0}, {1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

    }
}
