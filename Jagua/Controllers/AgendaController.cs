﻿using JaguaEntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Jagua.Controllers
{
    public class AgendaController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Agenda
        public ActionResult Calendario()
        {

            return View();
        }
        [HttpPost]
        public ActionResult DatosCalendario(int id)
        {
            bool success = true;
            string mensaje = string.Empty;
            List<Eventos> Eventos_ = new List<Eventos>();
            try
            {
                var desde = DateTime.Now.AddDays(-30); // Cantidad de dias para atras que muestra la cita
                var citas = db.cita.Where(t => t.id_estado != 6 && t.id_estado != 4 && t.fecha_cita.Value >= desde).ToList();
                foreach (var item in citas)
                {
                    Eventos_.Add(new Eventos
                    {
                        id = item.id_cita,
                        title = "Cita: " + item.paciente.nombre + " Fecha: " + item.fecha_cita.Value.ToString("yyyy-MM-dd") + " Hora: " + item.hora.ToString(@"hh\:mm"),
                        start = item.fecha_cita.Value.ToString("yyyy-MM-dd") + " " + item.hora,
                        end = item.fecha_cita.Value.ToString("yyyy-MM-dd") + " " + item.hora

                    });
                }
                var promociones = db.promocion.Where(t => t.fecha_desde.Value >= desde && t.estado != "I").ToList();
                foreach (var item in promociones)
                {
                    Eventos_.Add(new Eventos
                    {
                        id = item.id_promocion,
                        title = "Promoción: " + item.descripcion + " - " + item.producto.descripcion,
                        start = (item.fecha_desde != null ? item.fecha_desde.Value.ToString("yyyy-MM-dd") : null),
                        end = (item.fecha_hasta != null ? item.fecha_hasta.Value.ToString("yyyy-MM-dd") : null)
                    });
                }
                var pacienteconsulta = db.paciente_consulta.Where(t => t.prox_consulta != null && t.estado.Equals("A")).ToList();
                pacienteconsulta = pacienteconsulta.Where(t => t.prox_consulta.Value >= desde).ToList();
                //foreach (var item in pacienteconsulta)
                //{
                //    Eventos_.Add(new Eventos
                //    {
                //        title = "Próxima Consulta: " + item.paciente.nombre + " Fecha: " + item.prox_consulta.Value.ToString("yyyy-MM-dd"),
                //        start = item.prox_consulta.Value.ToString("yyyy-MM-dd"),
                //        end = item.prox_consulta.Value.ToString("yyyy-MM-dd")
                //    });
                //}
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "Error al buscar Datos Citas";
                Clases.Logger.Log.ErrorFormat("Error al buscar DatosCalendario: {0}, {1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje, Eventos_ }, JsonRequestBehavior.AllowGet);

            //return Json(String.Format("'Success':'false','Error':'Ha habido un error al insertar el registro.'"));
        }
        public async Task<ActionResult> PartialShow(int id)
        {
            cita cita = await db.cita.FindAsync(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            ViewBag.cedula = cita.cliente.persona.nro_documento;
            ViewBag.id_cliente = cita.id_cliente;
            ViewBag.nombre = cita.cliente.persona.nombre + " " + cita.cliente.persona.apellido;
            ViewBag.txtMascota = cita.paciente.nombre;
            ViewBag.txtFecha = cita.fecha_cita.Value.ToString("yyyy-MM-dd");
            ViewBag.txtHora = cita.hora;
            ViewBag.estado = cita.id_estado.ToString();
            ViewBag.ddlEstado = new SelectList(db.cita_estado, "id_cita_estado", "estado", cita.id_estado);
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", cita.id_paciente);
            ViewBag.id_usuario_alta = new SelectList(db.usuarios, "id_usuario", "usuario1", cita.id_usuario_alta);
            ViewBag.id_usuario_asignado = new SelectList(db.persona, "id_persona", "usuario1", cita.id_cliente);
            return PartialView("Details", cita);
        }
        public async Task<ActionResult> PartialShowPromocion(int id)
        {
            promocion promocion = await db.promocion.FindAsync(id);
            if (promocion == null)
            {
                return HttpNotFound();
            }
            ViewBag.puntos = Convert.ToInt32(Math.Floor(promocion.puntos));
            if (promocion.fecha_desde != null)
                ViewBag.Desde = promocion.fecha_desde.Value.ToString("yyyy-MM-dd");
            if (promocion.fecha_hasta != null)
                ViewBag.Hasta = promocion.fecha_hasta.Value.ToString("yyyy-MM-dd");
            ViewBag.estado = promocion.estado;
            ViewBag.id_producto = new SelectList(db.producto, "id_producto", "descripcion", promocion.id_producto);
            return PartialView(@"~/Views/Promociones/Details.cshtml", promocion);
        }
        public class Eventos
        {
            public int id { get; set; }
            public string title { get; set; }
            public string start { get; set; }
            public string end { get; set; }

        }
    }
}
