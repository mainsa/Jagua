﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    [ControlFilter]
    public class UsuariosController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Usuarios
        public ActionResult Index(int? page, string currentFilter)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta
            ViewBag.CurrentFilter = currentFilter;
            var usuarios = db.usuarios.Where(t => t.estado == "A").OrderBy(n => n.nombres).ToList();
            ViewBag.estado = string.Empty;

            if (!string.IsNullOrEmpty(currentFilter))
            {
                var ArrcurrentFilter = currentFilter.Split('_');
                if (ArrcurrentFilter.Length > 0)
                {
                    var nombre = ArrcurrentFilter[0];
                    var estado = ArrcurrentFilter[1];
                    if (!string.IsNullOrEmpty(nombre))
                    {
                        usuarios = usuarios.Where(q => q.nombres.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable estado
                    {
                        usuarios = usuarios.Where(t => t.estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
                    }
                    ViewBag.searchString = nombre;
                    ViewBag.estado = estado;
                }
            }
            return View(usuarios.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            ViewBag.estado = string.Empty;
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            var user = db.usuarios.OrderBy(n => n.nombres).ToList();
            string nombre = fc["searchString"]; // A traves del fc, obtengo el valor de la caja de texto
            string estado = string.IsNullOrEmpty(fc["searchEstado"]) ? "" : fc["searchEstado"];
            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                user = user.Where(q => q.nombres.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable estado
            {
                user = user.Where(t => t.estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.searchString = nombre;
            ViewBag.estado = estado;
            ViewBag.CurrentFilter = nombre + "_" + estado;
            //retorno la vista filtrada
            return View(user.ToPagedList(pageIndex, pageSize));

        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int idrol = 0;
            int idCliente = 0;
            usuarios usuario = db.usuarios.Find(id);
            if (usuario.usuario_rol.Any())
            {
                idrol = usuario.usuario_rol.FirstOrDefault().id_rol;
            }
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", idrol);
            if (usuario.id_cliente != null)
                idCliente = usuario.id_cliente.Value;
            List<modclientes> clientes = (from c in db.cliente
                                          join p in db.persona on c.id_persona equals p.id_persona
                                          where c.estado == "A"
                                          where p.estado == "A"
                                          select new modclientes
                                          {
                                              id_cliente = c.id_cliente,
                                              id_persona = c.id_persona,
                                              nombre = p.nombre + " " + p.apellido
                                          }).ToList();
            ViewBag.idCliente = new SelectList(clientes, "id_cliente", "nombre", idCliente);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre");
            List<modclientes> clientes = (from c in db.cliente
                                          join p in db.persona on c.id_persona equals p.id_persona
                                          where c.estado == "A"
                                          where p.estado == "A"
                                          select new modclientes
                                          {
                                              id_cliente = c.id_cliente,
                                              id_persona = c.id_persona,
                                              nombre = p.nombre + " " + p.apellido
                                          }).ToList();
            ViewBag.idCliente = new SelectList(clientes, "id_cliente", "nombre");
            ViewBag.estado = "A";
            return View();
        }

        // POST: Usuarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection datos)
        {
            //[Bind(Include = "id_usuario,nombres,apellidos,usuario,password,direccion,email,nro_celular,nro_telefono")] usuarios usuario,
            usuarios usuario = new usuarios();
            List<modclientes> clientes = new List<modclientes>();
            if (datos.Count > 0)
            {
                using (var context = new JaguaEntities())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string estado = string.IsNullOrEmpty(datos["ddlEstado"]) ? "A" : datos["ddlEstado"];
                            usuario.password = datos["password"];
                            //usuario.id_usuario = datos[""];
                            usuario.nombres = datos["nombres"];
                            usuario.apellidos = datos["apellidos"];
                            usuario.usuario = datos["usuario"];
                            usuario.direccion = datos["direccion"];
                            usuario.email = datos["email"];
                            usuario.nro_celular = datos["nro_celular"];
                            usuario.nro_telefono = datos["nro_telefono"];
                            string usrpassword = Comun.MD5(usuario.password);
                            usuario.password = usrpassword;
                            usuario.fecha_alta = DateTime.Now;
                            usuario.estado = estado;
                            usuario.usuario_alta = SessionManager.SessionData.id_usuario.ToString();

                            int idrol = int.Parse(datos["id_rol"].ToString());
                            var rolNombre = db.rol.Find(idrol).nombre;
                            if (!string.IsNullOrEmpty(datos["TipoPersona"]))
                            {
                                int idpersona = 0;
                                if (!string.IsNullOrEmpty(datos["id_persona"]))
                                {
                                    idpersona = int.Parse(datos["id_persona"]);
                                    var personas = db.persona.Find(idpersona);
                                    if (personas != null)
                                    {
                                        if (personas.cliente.Any())
                                        {
                                            usuario.id_cliente = personas.cliente.FirstOrDefault().id_cliente;
                                        }
                                        else if (personas.empleado.Any())
                                        {
                                            usuario.id_empleado = personas.empleado.FirstOrDefault().id_empleado;
                                        }
                                    }
                                }
                                if (datos["TipoPersona"].Equals("CL") && idpersona == 0)
                                {
                                    //SI EL USUARIO NO EXISTE EN CLIENTE CREAMOS
                                    if (rolNombre.ToUpper().Contains("CLIENTE") || rolNombre.ToUpper().Contains("CLIENTES"))
                                    {
                                        int idCliente = ValidarClientes(usuario.nombres, usuario.apellidos, usuario.email);
                                        if (idCliente == 0)//No existe el cliente
                                        {
                                            persona cliente = new persona();
                                            //desmenuzo el objeto para darle formato de primera letra en mayuscula
                                            cliente.nombre = Comun.UppercaseFirst(usuario.nombres);
                                            cliente.apellido = Comun.UppercaseFirst(usuario.apellidos);
                                            cliente.email = usuario.email;
                                            cliente.direccion = usuario.direccion;
                                            cliente.nro_celular = usuario.nro_celular;
                                            cliente.nro_telefono = usuario.nro_telefono;
                                            cliente.id_cat_persona = 1;
                                            cliente.estado = estado;
                                            cliente.fecha_alta = DateTime.Now;
                                            cliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                            context.persona.Add(cliente);
                                            context.SaveChanges();

                                            cliente datosCliente = new cliente();
                                            datosCliente.id_persona = cliente.id_persona;
                                            datosCliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                            datosCliente.fecha_alta = DateTime.Now;
                                            datosCliente.estado = estado;
                                            context.cliente.Add(datosCliente);
                                            context.SaveChanges();

                                            usuario.id_cliente = datosCliente.id_cliente;
                                        }
                                        else
                                        {
                                            //Ya existe un cliente con esos datos
                                            usuario.id_cliente = idCliente;
                                        }

                                    }
                                }
                                else if (datos["TipoPersona"].Equals("EM") && idpersona == 0)
                                {
                                    persona cliente = new persona();
                                    //desmenuzo el objeto para darle formato de primera letra en mayuscula
                                    cliente.nombre = Comun.UppercaseFirst(usuario.nombres);
                                    cliente.apellido = Comun.UppercaseFirst(usuario.apellidos);
                                    cliente.email = usuario.email;
                                    cliente.direccion = usuario.direccion;
                                    cliente.nro_celular = usuario.nro_celular;
                                    cliente.nro_telefono = usuario.nro_telefono;
                                    cliente.id_cat_persona = 2;
                                    cliente.estado = estado;
                                    cliente.fecha_alta = DateTime.Now;
                                    cliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                    context.persona.Add(cliente);
                                    context.SaveChanges();

                                    empleado datosEmpleados = new empleado();
                                    datosEmpleados.id_persona = cliente.id_persona;
                                    datosEmpleados.fecha_ingreso = DateTime.Now.ToString("dd/MM/yyyy");
                                    datosEmpleados.sueldo = 0;
                                    datosEmpleados.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                    datosEmpleados.fecha_alta = DateTime.Now;
                                    datosEmpleados.estado = estado;
                                    context.empleado.Add(datosEmpleados);
                                    context.SaveChanges();

                                    usuario.id_empleado = datosEmpleados.id_empleado;
                                }
                            }
                            context.usuarios.Add(usuario);
                            context.SaveChanges();

                            //SE GUARDA EL ROL DEL USUARIO

                            usuario_rol usuario_rol = new usuario_rol();
                            usuario_rol.id_usuario = usuario.id_usuario;
                            usuario_rol.id_rol = idrol;
                            usuario_rol.descripcion = rolNombre;
                            context.usuario_rol.Add(usuario_rol);
                            context.SaveChanges();
                            //---------------------------
                            dbContextTransaction.Commit();
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            Clases.Logger.Log.ErrorFormat("ERROR al insertar Fichas: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);

                        }
                    }
                }
            }
            clientes = (from c in db.cliente
                        join p in db.persona on c.id_persona equals p.id_persona
                        where c.estado == "A"
                        where p.estado == "A"
                        select new modclientes
                        {
                            id_cliente = c.id_cliente,
                            id_persona = c.id_persona,
                            nombre = p.nombre + " " + p.apellido
                        }).ToList();
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre");
            ViewBag.idCliente = new SelectList(clientes, "id_cliente", "nombre");
            ViewBag.estado = "A";
            return View(usuario);
        }
        private int ValidarClientes(string nombre, string apellido, string email)
        {
            int idCliente = 0;
            var valclientes = (from c in db.cliente
                               join p in db.persona on c.id_persona equals p.id_persona
                               select new
                               {
                                   p.id_persona,
                                   c.id_cliente,
                                   p.nombre,
                                   p.apellido,
                                   p.email
                               }).ToList();
            valclientes = valclientes.Where(t => t.nombre.ToLower().Contains(nombre.ToLower()) && t.apellido.ToLower().Contains(apellido.ToLower()) && t.email.ToLower().Contains(email.ToLower())).ToList();
            if (valclientes.Any())
                idCliente = valclientes.FirstOrDefault().id_cliente;//ya existe 
            return idCliente;
        }
        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int idrol = 0;
            int idCliente = 0;
            usuarios usuario = db.usuarios.Find(id);
            if (usuario.usuario_rol.Any())
            {
                idrol = usuario.usuario_rol.FirstOrDefault().id_rol;
            }
            ViewBag.estado = usuario.estado;
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", idrol);
            if (usuario.id_cliente != null)
                idCliente = usuario.id_cliente.Value;
            List<modclientes> clientes = (from c in db.cliente
                                          join p in db.persona on c.id_persona equals p.id_persona
                                          where c.estado == "A"
                                          where p.estado == "A"
                                          select new modclientes
                                          {
                                              id_cliente = c.id_cliente,
                                              id_persona = c.id_persona,
                                              nombre = p.nombre + " " + p.apellido
                                          }).ToList();
            ViewBag.idCliente = new SelectList(clientes, "id_cliente", "nombre", idCliente);
            if (usuario.id_cliente != null)
            {
                ViewBag.id_persona = usuario.id_cliente.Value;
                ViewBag.TipoPersona = "CL";
            }
            else if (usuario.id_empleado != null)
            {
                ViewBag.id_persona = usuario.id_empleado.Value;
                ViewBag.TipoPersona = "EM";
            }
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection datos)
        {
            //[Bind(Include = "id_usuario,nombres,apellidos,usuario,direccion,email,nro_celular,nro_telefono")]usuarios usuario,
            usuarios usuario = new usuarios();
            int idrol = 0;
            int idCliente = 0;
            int idpersona = 0;
            if (datos.Count > 0)
            {
                using (var context = new JaguaEntities())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string estado = string.IsNullOrEmpty(datos["ddlEstado"]) ? "A" : datos["ddlEstado"];
                            ViewBag.estado = estado;
                            usuario = context.usuarios.Find(int.Parse(datos["id_usuario"]));
                            var password = datos["PasswordEdit"];
                            if (!string.IsNullOrEmpty(password))
                            {
                                string usrpassword = Clases.Comun.MD5(password);
                                usuario.password = usrpassword;
                            }

                            usuario.id_usuario = int.Parse(datos["id_usuario"]);
                            usuario.nombres = datos["nombres"];
                            usuario.apellidos = datos["apellidos"];
                            usuario.usuario = datos["usuario"];
                            usuario.direccion = datos["direccion"];
                            usuario.email = datos["email"];
                            usuario.nro_celular = datos["nro_celular"];
                            usuario.nro_telefono = datos["nro_telefono"];
                            usuario.fecha_modificacion = DateTime.Now;
                            usuario.usuario_modificacion = SessionManager.SessionData.id_usuario.ToString();
                            usuario.estado = estado;
                            if (usuario.id_empleado != null)
                                idpersona = usuario.id_empleado.Value;
                            else if (usuario.id_empleado != null)
                                idpersona = usuario.id_cliente.Value;

                            idrol = int.Parse(datos["id_rol"].ToString());
                            var rolNombre = db.rol.Find(idrol).nombre;
                            if (!string.IsNullOrEmpty(datos["TipoPersona"]))
                            {
                                if (!string.IsNullOrEmpty(datos["id_persona"]))
                                {
                                    idpersona = int.Parse(datos["id_persona"]);
                                    var personas = db.persona.Find(idpersona);
                                    if (personas != null)
                                    {
                                        if (personas.cliente.Any())
                                        {
                                            usuario.id_cliente = personas.cliente.FirstOrDefault().id_cliente;
                                        }
                                        else if (personas.empleado.Any())
                                        {
                                            usuario.id_empleado = personas.empleado.FirstOrDefault().id_empleado;
                                        }
                                    }
                                }
                                if (datos["TipoPersona"].Equals("CL") && idpersona == 0)
                                {
                                    //SI EL USUARIO NO EXISTE EN CLIENTE CREAMOS
                                    if (rolNombre.ToUpper().Contains("CLIENTE") || rolNombre.ToUpper().Contains("CLIENTES"))
                                    {
                                        idCliente = ValidarClientes(usuario.nombres, usuario.apellidos, usuario.email);
                                        if (idCliente == 0)//No existe el cliente
                                        {
                                            persona cliente = new persona();
                                            //desmenuzo el objeto para darle formato de primera letra en mayuscula
                                            cliente.nombre = Comun.UppercaseFirst(usuario.nombres);
                                            cliente.apellido = Comun.UppercaseFirst(usuario.apellidos);
                                            cliente.email = usuario.email;
                                            cliente.direccion = usuario.direccion;
                                            cliente.nro_celular = usuario.nro_celular;
                                            cliente.nro_telefono = usuario.nro_telefono;
                                            cliente.id_cat_persona = 1;
                                            cliente.estado = estado;
                                            cliente.fecha_alta = DateTime.Now;
                                            cliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                            context.persona.Add(cliente);
                                            context.SaveChanges();

                                            cliente datosCliente = new cliente();
                                            datosCliente.id_persona = cliente.id_persona;
                                            datosCliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                            datosCliente.fecha_alta = DateTime.Now;
                                            datosCliente.estado = estado;
                                            context.cliente.Add(datosCliente);
                                            context.SaveChanges();

                                            usuario.id_cliente = datosCliente.id_cliente;
                                        }
                                        else
                                        {
                                            //Ya existe un cliente con esos datos
                                            usuario.id_cliente = idCliente;
                                        }

                                    }
                                }
                                else if (datos["TipoPersona"].Equals("EM") && idpersona == 0)
                                {
                                    persona cliente = new persona();
                                    //desmenuzo el objeto para darle formato de primera letra en mayuscula
                                    cliente.nombre = Comun.UppercaseFirst(usuario.nombres);
                                    cliente.apellido = Comun.UppercaseFirst(usuario.apellidos);
                                    cliente.email = usuario.email;
                                    cliente.direccion = usuario.direccion;
                                    cliente.nro_celular = usuario.nro_celular;
                                    cliente.nro_telefono = usuario.nro_telefono;
                                    cliente.id_cat_persona = 2;
                                    cliente.estado = estado;
                                    cliente.fecha_alta = DateTime.Now;
                                    cliente.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                    context.persona.Add(cliente);
                                    context.SaveChanges();

                                    empleado datosEmpleados = new empleado();
                                    datosEmpleados.id_persona = cliente.id_persona;
                                    datosEmpleados.fecha_ingreso = DateTime.Now.ToString("dd/MM/yyyy");
                                    datosEmpleados.sueldo = 0;
                                    datosEmpleados.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                    datosEmpleados.fecha_alta = DateTime.Now;
                                    datosEmpleados.estado = estado;
                                    context.empleado.Add(datosEmpleados);
                                    context.SaveChanges();

                                    usuario.id_empleado = datosEmpleados.id_empleado;
                                }
                            }
                            context.Entry(usuario).State = EntityState.Modified;
                            context.SaveChanges();
                            ViewBag.estado = usuario.estado;
                            //ACTUALIZAR ROL USUARIO


                            usuario_rol usuario_rol = usuario.usuario_rol.Where(t => t.id_usuario.Equals(usuario.id_usuario) && t.id_rol.Equals(idrol)).FirstOrDefault();
                            if (usuario_rol == null)
                            {
                                //ELIMINAR ROL ANTERIOR
                                var usuariosroles = usuario.usuario_rol.Where(t => t.id_usuario.Equals(usuario.id_usuario) && t.id_rol != idrol).ToList();
                                foreach (var item in usuariosroles)
                                {
                                    context.Entry(item).State = EntityState.Deleted;
                                }
                                context.SaveChanges();
                                //NUEVO ROL USUARIO
                                usuario_rol = new usuario_rol();
                                usuario_rol.id_usuario = usuario.id_usuario;
                                usuario_rol.id_rol = idrol;
                                usuario_rol.descripcion = rolNombre;
                                context.usuario_rol.Add(usuario_rol);
                                context.SaveChanges();
                            }
                            dbContextTransaction.Commit();
                            return RedirectToAction("Index");
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            Clases.Logger.Log.ErrorFormat("ERROR al insertar Fichas: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);

                        }
                    }
                }


            }
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", idrol);
            List<modclientes> clientes = (from c in db.cliente
                                          join p in db.persona on c.id_persona equals p.id_persona
                                          where c.estado == "A"
                                          where p.estado == "A"
                                          select new modclientes
                                          {
                                              id_cliente = c.id_cliente,
                                              id_persona = c.id_persona,
                                              nombre = p.nombre + " " + p.apellido
                                          }).ToList();
            ViewBag.idCliente = new SelectList(clientes, "id_cliente", "nombre", idCliente);

            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            usuarios usuario = db.usuarios.Find(id);
            usuario.estado = "I";
            //usuario.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            //usuario.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }
        public ActionResult PartialShowPersonas(string tipo)
        {
            var datos = db.persona.ToList();
            if (tipo.Equals("EM"))
            {
                datos = datos.Where(t => t.empleado.Any()).ToList();
            }
            else if (tipo.Equals("CL"))
            {
                datos = datos.Where(t => t.cliente.Any()).ToList();
            }
            return PartialView("_modalPersonas", datos);
        }

        public ActionResult BuscarCliEmp(string nombre, string tipo)
        {
            bool success = false;
            string mensaje = string.Empty;
            var datos = db.persona.ToList();
            try
            {
                if (tipo.Equals("EM"))
                {
                    datos = datos.Where(t => t.empleado.Any()).ToList();
                    datos = datos.Where(t => t.nombre.ToLower().Contains(nombre) || t.apellido.ToLower().Contains(nombre)).ToList();
                }
                else if (tipo.Equals("CL"))
                {
                    datos = datos.Where(t => t.cliente.Any()).ToList();
                    datos = datos.Where(t => t.nombre.ToLower().Contains(nombre) || t.apellido.ToLower().Contains(nombre)).ToList();
                }
                string tipopers = "";
                foreach (var item in datos)
                {
                    if (tipo.Equals("EM"))
                        tipopers = "EMPLEADO";
                    else if (tipo.Equals("CL"))
                        tipopers = "CLIENTE";
                    mensaje += "<tr>" +
                        "<td><span>" + item.id_persona + "</span></td> " +
                        "<td><span>" + item.nombre + " " + item.apellido + "</span></td> " +
                        "<td><span>" + tipopers + "</span>" +
                        "</td> " +
                        "<td><a href=\"javascript:Sel_Paciente(" + item.id_persona + ",'" + item.nombre + "','" + item.apellido + "');\" class='btn btn-primary'>Seleccionar</a></td> " +
                    "</tr>";
                }
                success = true;
            }catch(Exception ex)
            {
                success = false;
                Logger.Log.ErrorFormat("ERROR al BuscarCliEmp: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public class modclientes
        {
            public int id_cliente { get; set; }
            public Nullable<int> id_persona { get; set; }
            public string nombre { get; set; }

        }
    }
}
