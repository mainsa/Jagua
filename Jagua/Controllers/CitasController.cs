﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using Jagua.Models;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    [ControlFilter]
    public class CitasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Horas_nodisponibles (MATRIX)
        [HttpGet]
        public ActionResult Index(int? page, string currentFilter)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;


            var citas = db.cita.Include(o => o.cita_estado).ToList();
            var listaProd = db.producto.Where(o => o.id_tipo_producto == 2).ToList();
            int estado = 0;
            int servicio = 0;
            if (!string.IsNullOrEmpty(currentFilter))
            {
                var ArrcurrentFilter = currentFilter.Split('_');
                if (ArrcurrentFilter.Length > 0)
                {
                    var nombre = ArrcurrentFilter[0];
                    var apellido = ArrcurrentFilter[1]; // A traves del fc, obtengo el valor de la caja de texto
                    estado = int.Parse((string.IsNullOrEmpty(ArrcurrentFilter[2]) ? "0" : ArrcurrentFilter[2]));
                    var desde = ArrcurrentFilter[3];
                    var hasta = ArrcurrentFilter[4];
                    servicio = int.Parse((string.IsNullOrEmpty(ArrcurrentFilter[5]) ? "0" : ArrcurrentFilter[5]));

                    if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
                    {
                        citas = citas.Where(q => q.cliente.persona.nombre.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(apellido)) // pregunto si no esta vacio la variable nombre
                    {
                        citas = citas.Where(q => q.cliente.persona.apellido.ToLower().Contains(apellido.ToLower())).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (estado > 0) // pregunto si no esta vacio la variable nombre
                    {
                        citas = citas.Where(q => q.id_estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(desde) && !string.IsNullOrEmpty(hasta))
                    {
                        var fdesde = DateTime.Parse(desde + " 00:00:00");
                        var fhasta = DateTime.Parse(hasta + " 23:59:59");
                        citas = citas.Where(q => q.fecha_cita >= fdesde && q.fecha_cita <= fhasta).ToList();
                    }
                    if (servicio > 0)
                    {
                        citas = citas.Where(t => t.cita_detalle.Any(cd => cd.id_servicio.Equals(servicio))).ToList();
                    }

                    ViewBag.txtNombre = nombre;
                    ViewBag.txtApellido = apellido;
                    ViewBag.estado = estado;
                    ViewBag.txtFechaDesde = desde;
                    ViewBag.txtFechaHasta = hasta;

                }
            }
            ViewBag.ddlEstado = new SelectList(db.cita_estado.ToList(), "id_cita_estado", "descripcion", estado);
            ViewBag.id_producto = new SelectList(listaProd, "id_producto", "descripcion", servicio);
            return View(citas.OrderByDescending(n => n.fecha_cita).ToPagedList(pageIndex, pageSize));
        }

        // (MATRIX)
        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var citas = db.cita.ToList();
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            var apellido = fc["txtApellido"];
            var desde = fc["txtFechaDesde"];
            var hasta = fc["txtFechaHasta"];
            int estado = int.Parse((string.IsNullOrEmpty(fc["ddlEstado"].ToString()) ? "0" : fc["ddlEstado"].ToString()));
            int servicio = int.Parse((string.IsNullOrEmpty(fc["id_producto"].ToString()) ? "0" : fc["id_producto"].ToString()));


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                citas = citas.Where(q => q.cliente.persona.nombre.ToLower().Contains(nombre.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(apellido)) // pregunto si no esta vacio la variable nombre
            {
                citas = citas.Where(q => q.cliente.persona.apellido.ToLower().Contains(apellido.ToLower())).ToList();
            }
            if (estado > 0)
            {
                citas = citas.Where(q => q.id_estado == estado).ToList();
            }
            if (!string.IsNullOrEmpty(desde) && !string.IsNullOrEmpty(hasta))
            {
                var fdesde = DateTime.Parse(desde + " 00:00:00");
                var fhasta = DateTime.Parse(hasta + " 23:59:59");
                citas = citas.Where(q => q.fecha_cita >= fdesde && q.fecha_cita <= fhasta).ToList();
            }
            if (servicio > 0)
            {
                citas = citas.Where(t => t.cita_detalle.Any(cd => cd.id_servicio.Equals(servicio))).ToList();
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = nombre;
            ViewBag.txtApellido = apellido;
            ViewBag.txtFechaDesde = desde;
            ViewBag.txtFechaHasta = hasta;
            ViewBag.ddlEstado = new SelectList(db.cita_estado.ToList(), "id_cita_estado", "descripcion", estado);
            var listaProd = db.producto.Where(o => o.id_tipo_producto == 2).ToList();
            ViewBag.id_producto = new SelectList(listaProd, "id_producto", "descripcion", servicio);
            ViewBag.CurrentFilter = nombre + "_" + apellido + "_" + estado + "_" + desde + "_" + hasta + "_" + servicio;
            //retorno la vista filtrada
            return View(citas.OrderByDescending(n => n.fecha_cita).ToPagedList(pageIndex, pageSize));

        }

        // (MATRIX)
        //Metodo para usar el autocomplete de jquery ui en clientes
        public JsonResult ClienteSearch(string search, FormCollection fc)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var ClienteList = (from N in db.cliente
                                   where (N.persona.nro_documento.Contains(search) || N.persona.nombre.Contains(search) || N.persona.apellido.Contains(search))
                                   select new { N.persona.nro_documento, N.persona.nombre, N.persona.apellido, N.id_cliente });
                return Json(ClienteList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }
        // (MATRIX)
        public JsonResult ProductDetails(string search)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var ProductosList = (from N in db.producto
                                     where (N.id_producto.ToString().Contains(search) || N.descripcion.ToLower().Contains(search.ToLower()) || N.codigo_barra.Contains(search)) && N.id_tipo_producto == 2
                                     select new { N.id_producto, N.descripcion, N.precio });
                return Json(ProductosList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }
        public ActionResult CargarCita()
        {
            ViewBag.idCliente = 0;
            return View();
        }
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------
        // GET: Citas/Create
        public ActionResult Create()
        {
            ViewBag.idCliente = 0;
            return View();
        }

        //POST: Citas/Create
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Create(cita datos, CitaDetalle[] datosDetalle)
        {
            bool success = true;
            string mensaje = string.Empty;
            cita_detalle guardarDetalle;
            try
            {
                if (datos != null && datosDetalle != null)
                {
                    using (var context = new JaguaEntities())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                //se guarda la cabecera
                                datos.id_usuario_alta = 1;
                                datos.id_estado = 2;
                                //datos.fecha_alta = DateTime.Now;
                                context.cita.Add(datos);
                                context.SaveChanges();

                                //se guarda el detalle
                                foreach (var detalle in datosDetalle)
                                {
                                    guardarDetalle = new cita_detalle();
                                    guardarDetalle.id_cita = datos.id_cita;
                                    guardarDetalle.id_servicio = detalle.id_servicio;
                                    guardarDetalle.id_encargado = detalle.id_encargado;
                                    context.cita_detalle.Add(guardarDetalle);
                                }
                                context.SaveChanges();
                                dbContextTransaction.Commit();
                                success = true;
                                mensaje = "Se registro la Cita!";

                            }
                            catch (Exception ex)
                            {
                                success = false;
                                mensaje = "No se pudo registrar la Cita!";
                                dbContextTransaction.Rollback();
                                Clases.Logger.Log.ErrorFormat("ERROR al Editar Citas: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);

                            }
                        }
                    }
                }
                else
                {
                    success = false;
                    mensaje = "No se pudo registrar la Cita!";
                    ModelState.AddModelError("", "No se puede registrar los datos sin detalle de cita ");
                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "No se pudo registrar la Cita!";
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }
        // --------------------------------------------------------------------------------------------------------------------------------------------------------------------


        // GET: Citas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cita cita = await db.cita.FindAsync(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            ViewBag.cedula = cita.cliente.persona.nro_documento;
            ViewBag.id_cliente = cita.id_cliente;
            ViewBag.nombre = cita.cliente.persona.nombre + " " + cita.cliente.persona.apellido;
            ViewBag.txtMascota = cita.paciente.nombre;
            ViewBag.txtFecha = cita.fecha_cita.Value.ToString("dd/MM/yyyy");
            ViewBag.txtHora = cita.hora.ToString(@"hh\:mm");
            ViewBag.estado = cita.id_estado.ToString();
            ViewBag.ddlEstado = new SelectList(db.cita_estado, "id_cita_estado", "descripcion", cita.id_estado);
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", cita.id_paciente);
            ViewBag.id_usuario_alta = new SelectList(db.usuarios, "id_usuario", "usuario1", cita.id_usuario_alta);
            ViewBag.id_usuario_asignado = new SelectList(db.persona, "id_persona", "usuario1", cita.id_cliente);
            return PartialView("Details", cita);
        }

        //// GET: Citas/Create
        //public ActionResult Create()
        //{
        //    ViewBag.id_estado = new SelectList(db.cita_estado, "id_cita_estado", "estado");
        //    ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre");
        //    ViewBag.id_usuario_alta = new SelectList(db.usuario, "id_usuario", "usuario1");
        //    ViewBag.id_usuario_asignado = new SelectList(db.usuario, "id_usuario", "usuario1");
        //    return View();
        //}

        //// POST: Citas/Create
        //// Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        //// más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id_cita,id_usuario_alta,id_usuario_asignado,fecha_cita,hora,id_paciente,id_estado")] cita cita)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.cita.Add(cita);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.id_estado = new SelectList(db.cita_estado, "id_cita_estado", "estado", cita.id_estado);
        //    ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", cita.id_paciente);
        //    ViewBag.id_usuario_alta = new SelectList(db.usuario, "id_usuario", "usuario1", cita.id_usuario_alta);
        //    ViewBag.id_usuario_asignado = new SelectList(db.usuario, "id_usuario", "usuario1", cita.id_usuario_asignado);
        //    return View(cita);
        //}

        // GET: Citas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cita cita = db.cita.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            ViewBag.cedula = cita.cliente.persona.nro_documento;
            ViewBag.id_cliente = cita.id_cliente;
            ViewBag.nombre = cita.cliente.persona.nombre + " " + cita.cliente.persona.apellido;
            ViewBag.txtMascota = cita.paciente.nombre;
            ViewBag.txtFecha = cita.fecha_cita.Value.ToString("yyyy-MM-dd");
            ViewBag.txtHora = cita.hora;
            ViewBag.estado = cita.id_estado.ToString();
            ViewBag.id_estado = new SelectList(db.cita_estado, "id_cita_estado", "descripcion", cita.id_estado);
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", cita.id_paciente);
            ViewBag.id_usuario_alta = new SelectList(db.usuarios, "id_usuario", "usuario1", cita.id_usuario_alta);
            ViewBag.id_usuario_asignado = new SelectList(db.persona, "id_persona", "usuario1", cita.id_cliente);
            return View(cita);
        }

        // POST: Citas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(cita datos, CitaDetalle[] datosDetalle)
        {
            bool success = true;
            string mensaje = string.Empty;
            cita_detalle guardarDetalle;
            cita_detalle editDetalle;

            try
            {
                if (datos != null && datosDetalle != null)
                {
                    using (var context = new JaguaEntities())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                //se guarda la cabecera
                                var cabcita = context.cita.Find(datos.id_cita);
                                cabcita.fecha_cita = datos.fecha_cita;
                                cabcita.hora = datos.hora;
                                cabcita.id_paciente = datos.id_paciente;
                                cabcita.id_cliente = datos.id_cliente;
                                cabcita.id_estado = 2;//datos.id_estado;
                                //datos.id_usuario_alta = Clases.SessionManager.SessionData.id_usuario;
                                //datos.id_estado = 2;
                                //datos.fecha_alta = DateTime.Now;
                                context.Entry(cabcita).State = EntityState.Modified;
                                context.SaveChanges();

                                //se guarda el detalle

                                foreach (var detalle in datosDetalle)
                                {

                                    if (detalle.id_cita_detalle > 0)
                                    {
                                        editDetalle = context.cita_detalle.Find(detalle.id_cita_detalle);
                                        if (editDetalle != null)
                                        {
                                            editDetalle.id_cita = datos.id_cita;
                                            editDetalle.id_servicio = detalle.id_servicio;
                                            editDetalle.id_encargado = detalle.id_encargado;
                                            context.Entry(editDetalle).State = EntityState.Modified;
                                        }
                                    }
                                    else
                                    {
                                        guardarDetalle = new cita_detalle();
                                        guardarDetalle.id_cita = datos.id_cita;
                                        guardarDetalle.id_servicio = detalle.id_servicio;
                                        guardarDetalle.id_encargado = detalle.id_encargado;
                                        context.cita_detalle.Add(guardarDetalle);
                                    }
                                }
                                context.SaveChanges();
                                dbContextTransaction.Commit();
                                success = true;
                                mensaje = "Se actualizo la Cita!";

                            }
                            catch (Exception ex)
                            {
                                success = false;
                                mensaje = "No se pudo registrar la Cita!";
                                dbContextTransaction.Rollback();
                                Clases.Logger.Log.ErrorFormat("ERROR al Editar Citas: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);

                            }
                        }
                    }
                }
                else
                {
                    success = false;
                    mensaje = "No se pudo registrar la Cita!";
                    ModelState.AddModelError("", "No se puede registrar los datos sin detalle de cita ");
                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "No se pudo registrar la Cita!";
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        // GET: Citas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cita cita = db.cita.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            return View(cita);
        }

        // POST: Citas/Delete/5
        [HttpPost, ActionName("EstadoConfirmed")]
        public ActionResult EstadoConfirmed(int id, string estado)
        {
            //Como en delete no va a borrar solo va a cambiar de estado use para el cancelar esa funciona
            //y le cambie el estado a X que es cancelar
            bool success = false;
            string mensaje = string.Empty;
            try
            {
                var citaestado = db.cita_estado.Where(t => t.estado.Equals(estado));//Obtenemos el id estado filtrado por P,C,F etc.
                if (citaestado.Any())
                {
                    cita cita = db.cita.Find(id);//buscamos la cita para actualizar
                    cita.id_estado = citaestado.FirstOrDefault().id_cita_estado;//le guardamos el id estado que corresponde
                    //id_cita_estado estado  descripcion
                    //2   P PENDIENTE
                    //3   C CONFIRMADA
                    //4   F FINALIZADA
                    //5   R RECHAZADA
                    //6   X CANCELADA
                    //7   E ENVIADO
                    db.Entry(cita).State = EntityState.Modified;
                    db.SaveChanges();
                    success = true;
                    //COMO YA TENEMOS UNA FUNCION PARA CANCELAR UAMOS ESA MISMA FUNCIONA PARA CONFIRMAR Y FINALIZAR YA QUE RECIBE EL ESTADO 
                    // Y ACTUALIZA PARA NO CREAR OTRA FUNCION USAMOS EL MISMO PODEMOS CAMBIAR 
                    //COMO RECIBIMOS EL TIPO ESTADO USAMOS PARA SABER QUE MENSAJE LE RETORNAMOS
                    switch (estado)
                    {
                        case "C":
                            mensaje = "Se Confirmo la Cita!";
                            break;
                        case "F":
                            mensaje = "Se Finalizo la Cita!";
                            break;
                        case "X":
                            mensaje = "Se Cancelo la Cita!";
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "No se pudo cancelar la Cita!";
                Clases.Logger.Log.ErrorFormat("ERROR al cancelar Citas: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);

            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult PartialShowPacientes(int idCliente)
        {
            var data = db.paciente.Where(t => t.id_cliente.Equals(idCliente)).ToList();
            return PartialView("_modalPacientes", data);
        }
        public ActionResult DeleteCitasDet(int idCitaDet)
        {
            bool success = true;
            string mensaje = string.Empty;
            try
            {
                var data = db.cita_detalle.Find(idCitaDet);
                if (data != null)
                {
                    db.cita_detalle.Remove(data);
                    db.SaveChanges();
                    success = true;
                    mensaje = "Registro eliminado!";
                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "No se pudo registrar la Cita!";
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }
    }
}
