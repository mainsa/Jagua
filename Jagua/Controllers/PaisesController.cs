﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class PaisesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Paises
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            return View(db.pais.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var paises = db.pais.ToList(); // genero una lista de paises
            var nombre = fc["txtNombre"].ToLower(); // A traves del fc, obtengo el valor de la caja de texto
            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                paises = paises.Where(q => q.descripcion.ToLower().Contains(nombre)).ToList(); //contains es la funcion que filtra la busqueda que contiene el metodo de busqueda
            }
            ViewBag.txtNombre = fc["txtNombre"]; //aca le asigno la variable que contiene la busqueda, para que aparezca al retornar

            return View(paises.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
            
        }

        // GET: Paises/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Paises/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_pais,descripcion,gentilicio")] pais pais)
        {
            if (ModelState.IsValid)
            {
                pais.descripcion = Jagua.Clases.Comun.UppercaseFirst(pais.descripcion);
                pais.estado = "A";
                pais.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                pais.fecha_alta = DateTime.Now;
                db.pais.Add(pais);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pais);
        }

        // GET: Paises/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pais pais = db.pais.Find(id);
            if (pais == null)
            {
                return HttpNotFound();
            }
            return View(pais);
        }

        // POST: Paises/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_pais,descripcion,gentilicio,estado,usuario_alta,fecha_alta")] pais pais)
        {
            if (ModelState.IsValid)
            {
                pais.descripcion = Jagua.Clases.Comun.UppercaseFirst(pais.descripcion);
                pais.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
                pais.fecha_mod = DateTime.Now;
                db.Entry(pais).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pais);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            pais pais = db.pais.Find(id);
            pais.estado = "I";
            pais.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            pais.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
