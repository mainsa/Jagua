﻿using Jagua.Models;
using JaguaEntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jagua.Controllers
{
    public class CitaModelsController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: CitaModels
        public ActionResult NuevaCita()
        {
            CitaModel citaModel = new CitaModel();
            //citaModel.cabecera = new cita();
            //citaModel.detalle = new List<cita_detalle>();

            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre");
            ViewBag.id_estado = new SelectList(db.cita_estado, "id_cita_estado", "descripcion");
            ViewBag.id_usuario_alta = new SelectList(db.usuarios, "id_usuario", "nombre", "apellido");
            ViewBag.id_usuario_asignado = new SelectList(db.usuarios, "id_usuario", "nombre", "apellido");

            return View(citaModel);
            //return View(citaModel.OrderByDescending(n => n.fecha_cita).ToPagedList(pageIndex, pageSize));
        }
    }
}