﻿using Jagua.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Jagua.Controllers
{
    [ControlFilter]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var datos = SessionManager.SessionData;
            ViewBag.UsuarioNombre = datos.nombres + " " + datos.apellidos;
            ViewBag.UsuarioLogeado = datos.usuario;
            return View();
        }
    }
}