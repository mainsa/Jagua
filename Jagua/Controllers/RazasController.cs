﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class RazasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Razas
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta


            ViewBag.ddlEspecie = new SelectList(db.especie.ToList(), "id_especie", "nombre");
            var raza = db.raza.Include(r => r.especie);
            return View(raza.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var razas = db.raza.ToList(); // genero una lista de razas
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            int especie = string.IsNullOrEmpty(fc["ddlEspecie"].ToString()) ? Convert.ToInt32("0") : Convert.ToInt32(fc["ddlEspecie"].ToString());

            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                //contains es la funcion que filtra la busqueda que contiene el metodo de busqueda
                razas = razas.Where(q => q.descripcion.ToLower().Contains(nombre.ToLower())).ToList(); 
            }
            if (especie > 0)
            {
                razas = razas.Where(x => x.id_especie == especie).ToList();
            }


            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = nombre;
            //devuelvo el ddl como ultimo argumento la variable de busqueda
            ViewBag.ddlEspecie = new SelectList(db.especie.ToList(), "id_especie", "nombre", especie); 
            //retorno la vista filtrada
            return View(razas.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        // GET: Razas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            raza raza = db.raza.Find(id);
            if (raza == null)
            {
                return HttpNotFound();
            }
            return View(raza);
        }

        // GET: Razas/Create
        public ActionResult Create()
        {
            ViewBag.id_especie = new SelectList(db.especie, "id_especie", "nombre");
            return View();
        }

        // POST: Razas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_raza,descripcion,id_especie")] raza raza)
        {
            if (ModelState.IsValid)
            {
                raza.descripcion = Jagua.Clases.Comun.UppercaseFirst(raza.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                raza.estado = "A";
                raza.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                raza.fecha_alta = DateTime.Now;
                db.raza.Add(raza);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_especie = new SelectList(db.especie, "id_especie", "nombre", raza.id_especie);
            return View(raza);
        }

        // GET: Razas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            raza raza = db.raza.Find(id);
            if (raza == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_especie = new SelectList(db.especie, "id_especie", "nombre", raza.id_especie);
            return View(raza);
        }

        // POST: Razas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_raza,descripcion,id_especie,estado,usuario_alta,fecha_alta")] raza raza)//aca hay que agregarle los nuevos campos, sino va a vaciar
        {
            if (ModelState.IsValid)
            {
                raza.descripcion = Jagua.Clases.Comun.UppercaseFirst(raza.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                raza.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                raza.fecha_mod = DateTime.Now;
                db.Entry(raza).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_especie = new SelectList(db.especie, "id_especie", "nombre", raza.id_especie);
            return View(raza);
        }

        // GET: Razas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            raza raza = db.raza.Find(id);
            if (raza == null)
            {
                return HttpNotFound();
            }
            return View(raza);
        }

        // POST: Razas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            raza raza = db.raza.Find(id);
            db.raza.Remove(raza);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            raza razas = db.raza.Find(id);
            razas.estado = "I";
            razas.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            razas.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
