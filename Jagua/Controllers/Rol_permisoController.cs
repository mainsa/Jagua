﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;
using Jagua.Clases;

namespace Jagua.Controllers
{
    public class rol_permisoController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: rol_permiso
        public async Task<ActionResult> Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            var rolpermiso = await db.rol_permiso.ToListAsync();
            return View(rolpermiso.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public async Task<ActionResult> Index(FormCollection fc, int? page)
        {
            var rol_permiso = await db.rol_permiso.ToListAsync();
            var nombre = fc["searchString"]; // A traves del fc, obtengo el valor de la caja de texto


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                nombre = nombre.ToLower();
                rol_permiso = rol_permiso.Where(q => q.rol.nombre.ToLower().Contains(nombre)
                || q.permiso.nombre.ToLower().Contains(nombre)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.searchString = nombre;

            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            if (rol_permiso == null)
            {
                return HttpNotFound();
            }
            return View(rol_permiso.ToPagedList(pageIndex, pageSize));
        }

        // GET: rol_permiso/Create
        public ActionResult Create()
        {
            rol_permiso model = new rol_permiso();
            ViewBag.id_permiso = new SelectList(db.permiso, "id_permiso", "nombre");
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre");
            return View(model);
        }

        // POST: rol_permiso/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id_rol_permiso,id_rol,id_permiso,descripcion")] rol_permiso rol_permiso)
        {
            if (ModelState.IsValid)
            {
                db.rol_permiso.Add(rol_permiso);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(rol_permiso);
        }

        // GET: rol_permiso/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rol_permiso rol_permiso = await db.rol_permiso.FindAsync(id);
            if (rol_permiso == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_permiso = new SelectList(db.permiso, "id_permiso", "nombre", rol_permiso.id_permiso);
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", rol_permiso.id_rol);
            return View(rol_permiso);
        }

        // POST: rol_permiso/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id_rol_permiso,id_rol,id_permiso,descripcion")] rol_permiso rol_permiso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rol_permiso).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(rol_permiso);
        }

        // GET: rol_permiso/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rol_permiso rol_permiso = await db.rol_permiso.FindAsync(id);
            if (rol_permiso == null)
            {
                return HttpNotFound();
            }
            return View(rol_permiso);
        }

        // POST: rol_permiso/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            rol_permiso rol_permiso = await db.rol_permiso.FindAsync(id);
            db.rol_permiso.Remove(rol_permiso);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
   

    }


}
