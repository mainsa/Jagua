﻿using JaguaEntityModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Jagua.Controllers
{
    public class ServiciosController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Productos
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta


            var producto = db.producto.Where(x => x.id_tipo_producto == 2);
            return View(producto.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var producto = db.producto.Where(x => x.id_tipo_producto == 2);
            var descripcion = fc["txtDescripcion"];

            if (!string.IsNullOrEmpty(descripcion)) // pregunto si no esta vacio la variable nombre
            {
                producto = producto.Where(q => q.descripcion.Contains(descripcion));//pregunta si el campo es igual al filtro
            }

            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtDescripcion = descripcion;

            //retorno la vista filtrada
            return View(producto.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "id_producto,descripcion,precio,duracion")] producto servicio)
        {
            if (ModelState.IsValid)
            {
                servicio.descripcion = Jagua.Clases.Comun.UppercaseFirst(servicio.descripcion);
                servicio.id_tipo_producto = 2;
                servicio.fecha_alta = DateTime.Now;
                servicio.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                servicio.estado = "A";
                db.producto.Add(servicio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto servicio = db.producto.Find(id);
            if (servicio == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.estado = servicio.estado;

                return View(servicio);
            }

        }

        [HttpPost]
        public ActionResult Edit([Bind(Include = "id_producto,id_tipo_producto,descripcion,precio,duracion,usuario_alta,fecha_alta")] producto servicio)
        {
            if (ModelState.IsValid)
            {
                servicio.descripcion = Jagua.Clases.Comun.UppercaseFirst(servicio.descripcion);
                servicio.fecha_mod = DateTime.Now;
                servicio.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                db.Entry(servicio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            producto servicio = db.producto.Find(id);
            servicio.estado = "I";
            servicio.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            servicio.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }
    }
}