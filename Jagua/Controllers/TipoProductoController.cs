﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class TipoProductoController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: TipoProducto
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var tp = db.tipo_producto.ToList();
            return View(tp.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var tp = db.tipo_producto.ToList();
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                tp = tp.Where(q => q.descripcion.ToLower().Contains(nombre)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = fc["txtNombre"];

            //retorno la vista filtrada
            return View(tp.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        // GET: TipoProducto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoProducto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_tipo_producto,descripcion")] tipo_producto tipo_producto)
        {
            if (ModelState.IsValid)
            {
                tipo_producto.descripcion = Jagua.Clases.Comun.UppercaseFirst(tipo_producto.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                tipo_producto.estado = "A";
                tipo_producto.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                tipo_producto.fecha_alta = DateTime.Now;
                db.tipo_producto.Add(tipo_producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipo_producto);
        }

        // GET: TipoProducto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_producto tipo_producto = db.tipo_producto.Find(id);
            if (tipo_producto == null)
            {
                return HttpNotFound();
            }
            return View(tipo_producto);
        }

        // POST: TipoProducto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_tipo_producto,descripcion,estado,usuario_alta,fecha_alta")] tipo_producto tipo_producto)
        {
            if (ModelState.IsValid)
            {
                tipo_producto.descripcion = Jagua.Clases.Comun.UppercaseFirst(tipo_producto.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                tipo_producto.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                tipo_producto.fecha_mod = DateTime.Now;
                db.Entry(tipo_producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipo_producto);
        }

        // GET: TipoProducto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tipo_producto tipo_producto = db.tipo_producto.Find(id);
            if (tipo_producto == null)
            {
                return HttpNotFound();
            }
            return View(tipo_producto);
        }

        // POST: TipoProducto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tipo_producto tipo_producto = db.tipo_producto.Find(id);
            db.tipo_producto.Remove(tipo_producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            tipo_producto tipo_producto = db.tipo_producto.Find(id);
            tipo_producto.estado = "I";
            tipo_producto.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            tipo_producto.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
