﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    [ControlFilter]
    public class ClinicaController : Controller
    {
        private JaguaEntities db = new JaguaEntities();
        // GET: Clinica
        public ActionResult Empleados(int? page)
        {
            int pageSize = 5;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            var lista = db.persona.Include(o => o.categoria_persona).OrderByDescending(n => n.nombre).Where(x => x.id_cat_persona != 1);
            return View(lista.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Empleados(FormCollection fc, int? page)
        {
            int pageSize = 5;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var clientes = db.persona.Where(x => x.id_cat_persona != 1).ToList();

            var cedula = fc["txtCedula"];
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            var apellido = fc["txtApellido"];


            if (!string.IsNullOrEmpty(cedula)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.nro_documento == cedula).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.nombre.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(apellido)) // pregunto si no esta vacio la variable nombre
            {
                clientes = clientes.Where(q => q.apellido.ToLower().Contains(apellido.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtCedula = cedula;
            ViewBag.txtNombre = fc["txtNombre"];
            ViewBag.txtApellido = fc["txtApellido"];

            //retorno la vista filtrada
            return View(clientes.OrderByDescending(n => n.nombre).ToPagedList(pageIndex, pageSize));

        }
        // GET: Clientes/Create
        public ActionResult AgregarEmpleado()
        {
            ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion");
            ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion");
            ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion");
            ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion");
            ViewBag.nacionalidad = new SelectList(db.pais, "id_pais", "gentilicio");
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento, "id_tipo_documento", "abreviatura");
            ViewBag.id_cat_persona = new SelectList(db.categoria_persona.Where(x => x.id_cat_persona != 1), "id_cat_persona", "descripcion");

            return View();
        }

        // POST: Clientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgregarEmpleado([Bind(Include = "id_persona,nombre,apellido,id_tipo_documento,nro_documento,nacionalidad,id_barrio,id_ciudad,email,direccion,nro_telefono,nro_celular,sexo,id_cat_persona,fecha_ingreso,sueldo")] persona persona)
        {
            if (ModelState.IsValid)
            {
                //desmenuzo el objeto para darle formato de primera letra en mayuscula
                persona.nombre = Jagua.Clases.Comun.UppercaseFirst(persona.nombre);
                persona.apellido = Jagua.Clases.Comun.UppercaseFirst(persona.apellido);
                persona.direccion = Jagua.Clases.Comun.UppercaseFirst(persona.direccion);
                persona.id_cat_persona = persona.id_cat_persona;
                persona.estado = "A";
                persona.fecha_alta = DateTime.Now;
                persona.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();

                empleado empleado = new empleado();
                empleado.id_persona = empleado.id_persona;
                empleado.fecha_ingreso = empleado.fecha_ingreso != null ? empleado.fecha_ingreso : "";
                empleado.sueldo = empleado.sueldo;
                empleado.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                empleado.fecha_alta = DateTime.Now;
                empleado.estado = "A";

                db.empleado.Add(empleado);
                db.persona.Add(persona);
                db.SaveChanges();
                return RedirectToAction("Empleados");
            }

            return View(persona);
        }

        // GET: Clientes/Edit/5
        public ActionResult EditarEmpleado(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            persona persona = db.persona.Find(id);
            if (persona == null)
            {
                return HttpNotFound();
            }
            ciudad ciudad = db.ciudad.Find(persona.id_ciudad);
            if (persona.id_barrio != null)
            { //Pregunto si esta cargado el barrio para traer los demas datos
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion", persona.id_barrio);
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion", persona.barrio.id_ciudad);
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion", persona.barrio.ciudad.id_departamento);
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion", persona.barrio.ciudad.departamento.id_pais);
            }
            else if (ciudad != null)
            {
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion", ciudad.departamento.id_pais);
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion", ciudad.id_departamento);
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion", persona.id_ciudad);
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion", persona.id_barrio);
            }
            else
            {
                ViewBag.ddlPais = new SelectList(db.pais, "id_pais", "descripcion");
                ViewBag.ddlDepartamento = new SelectList(db.departamento, "id_departamento", "descripcion");
                ViewBag.id_ciudad = new SelectList(db.ciudad, "id_ciudad", "descripcion");
                ViewBag.id_barrio = new SelectList(db.barrio, "id_barrio", "descripcion");
            }

            var nac = db.pais.Where(x => x.gentilicio == persona.nacionalidad).FirstOrDefault();
            var gentilicio = nac != null ? Convert.ToInt32(nac.id_pais) : 1;
            ViewBag.nacionalidad = new SelectList(db.pais, "id_pais", "gentilicio", gentilicio);
            ViewBag.estado = persona.estado;
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento, "id_tipo_documento", "abreviatura", persona.id_tipo_documento);
            ViewBag.id_cat_persona = new SelectList(db.categoria_persona.Where(x => x.id_cat_persona != 1), "id_cat_persona", "descripcion", persona.id_cat_persona);

            return View(persona);
        }

        // POST: Clientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarEmpleado([Bind(Include = "id_persona,nombre,apellido,id_tipo_documento,nro_documento,nacionalidad,id_barrio,id_ciudad,email,direccion,nro_telefono,nro_celular,sexo,id_cat_persona,fecha_ingreso,sueldo,estado")] persona persona)
        {
            if (ModelState.IsValid)
            {
                persona.fecha_mod = DateTime.Now;
                persona.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();

                var empleado = db.empleado.Where(x => x.id_persona == persona.id_persona).FirstOrDefault();
                empleado.fecha_ingreso = empleado.fecha_ingreso != null ? empleado.fecha_ingreso : "";
                empleado.sueldo = empleado.sueldo;
                empleado.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                empleado.fecha_mod = DateTime.Now;
                empleado.estado = empleado.estado;


                db.persona.Add(persona);
                db.Entry(persona).State = EntityState.Modified;
                db.empleado.Add(empleado);
                db.Entry(empleado).State = EntityState.Modified;

                db.SaveChanges();
                return RedirectToAction("Empleados");
            }
            return View(persona);
        }

        [HttpGet]
        public ActionResult Consulta()
        {
            //ViewBag.id_paciente = new SelectList(db.paciente.ToList(), "id_paciente", "nombre");
            ViewBag.id_tipo_documento_nuevo = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            ViewBag.id_medico = new SelectList(db.empleado.Include("persona").Where(x => x.persona.id_cat_persona == 2).ToList(), "id_empleado", "persona.nombre");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Consulta([Bind(Include = "id_ficha_consulta,id_ficha,id_paciente,id_medico,fecha,peso,temperatura,sintomas,diagnostico,indicaciones,prox_consulta")] paciente_consulta consulta)
        {
            if (ModelState.IsValid)
            {
                var id_ficha = db.ficha.Where(x => x.id_paciente == consulta.id_paciente).FirstOrDefault().id_ficha;
                consulta.id_ficha = id_ficha;
                consulta.sintomas = Clases.Comun.UppercaseFirst(consulta.sintomas);
                consulta.diagnostico = Clases.Comun.UppercaseFirst(consulta.diagnostico);
                consulta.indicaciones = Clases.Comun.UppercaseFirst(consulta.indicaciones);
                consulta.fecha_alta = DateTime.Now;
                consulta.usuario_alta = Clases.SessionManager.SessionData.usuario;
                db.paciente_consulta.Add(consulta);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(consulta);
        }

        [HttpGet]
        public ActionResult Vacunas()
        {
            //ViewBag.id_paciente = new SelectList(db.paciente.ToList(), "id_paciente", "nombre");
            ViewBag.id_tipo_documento_nuevo = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            ViewBag.id_medico = new SelectList(db.empleado.Include("persona").Where(x => x.persona.id_cat_persona == 2).ToList(), "id_empleado", "persona.nombre");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Vacunas([Bind(Include = "id_ficha_vacuna,id_ficha,id_paciente,fecha,id_vacuna,fecha_aplicacion,prox_aplicacion,nro_dosis_restantes")] paciente_vacuna vacuna)
        {
            if (ModelState.IsValid)
            {
                vacuna.id_ficha = db.ficha.Where(x => x.id_paciente == vacuna.id_paciente).FirstOrDefault().id_ficha;
                vacuna.fecha_alta = DateTime.Now;
                vacuna.usuario_alta = Clases.SessionManager.SessionData.usuario;
                db.paciente_vacuna.Add(vacuna);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(vacuna);
        }

        public JsonResult getVacunas(int id_paciente)
        {
            int id_especie = Convert.ToInt32(db.paciente.Where(x => x.id_paciente == id_paciente).First().id_especie);

            if (!String.IsNullOrEmpty(id_especie.ToString()))
            {
                //List<paciente> pacientes = new List<paciente>();
                var vacunas = (from a in db.vacuna
                               where a.id_especie == id_especie
                               select new
                               { a.id_vacuna, a.nombre });
                //var pacientes = pac.Select(a => "<option value='" + a.id_paciente + "'>" + a.nombre + "</option>"); // por si quiero cargar del lado del servidor descomentar esta linea
                return Json(vacunas, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        [HttpGet]
        public ActionResult Laboratorio()
        {
            ViewBag.id_laboratorio = new SelectList(db.laboratorio.ToList().OrderBy(x => x.nombre), "id_laboratorio", "nombre");
            ViewBag.id_tipo_documento_nuevo = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            ViewBag.id_doctor = new SelectList(db.empleado.Include("persona").Where(x => x.persona.id_cat_persona == 2).ToList(), "id_empleado", "persona.nombre");
            return View();
        }

        [HttpPost]
        public JsonResult Laboratorio(paciente_laboratorio laboratorio, paciente_laboratorio_detalle[] detalle)
        {
            try
            {
                if (laboratorio != null && detalle != null)
                {
                    laboratorio.id_ficha = db.ficha.Where(x => x.id_paciente == laboratorio.id_paciente).FirstOrDefault().id_ficha;
                    laboratorio.fecha_alta = DateTime.Now;
                    laboratorio.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                    db.paciente_laboratorio.Add(laboratorio);
                    db.SaveChanges();

                    //se guarda el detalle
                    foreach (var datosdetalle in detalle)
                    {
                        var guardarDetalle = new paciente_laboratorio_detalle();
                        guardarDetalle.id_ficha_lab = laboratorio.id_ficha_lab;
                        guardarDetalle.id_laboratorio = datosdetalle.id_laboratorio;

                        db.paciente_laboratorio_detalle.Add(guardarDetalle);
                    }
                    db.SaveChanges();
                    return Json(new { Status = "Success" });
                }
                else
                {
                    ModelState.AddModelError("", "No se puede registrar los datos");
                    var result = "0";
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
                var result = "1";
                return Json(result);
            }
        }

        public ActionResult getListadoTratamientos(int id_paciente)
        {
            var tratamientos = new List<paciente_tratamientos>();
            try
            {
                tratamientos = db.paciente_tratamientos.Where(t => t.id_paciente == id_paciente).OrderByDescending(x => x.fecha).ToList();
            }
            catch (Exception ex)
            {
            }
            return PartialView("_Tratamientos", tratamientos);
        }
        public ActionResult getDetalleTratamiento(int id_ficha_trat)
        {
            var detalle = new List<paciente_tratamientos_detalles>();
            try
            {
                detalle = db.paciente_tratamientos_detalles.Where(t => t.id_ficha_trat == id_ficha_trat).ToList();
                ViewBag.id_tipo_documento_nuevo = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
                ViewBag.id_medico = new SelectList(db.empleado.Include("persona").Where(x => x.persona.id_cat_persona == 2).ToList(), "id_empleado", "persona.nombre");
                ViewBag.id_patologia = new SelectList(db.patologia.ToList(), "id_patologia", "nombre");
                var tratamiento = db.paciente_tratamientos.Where(x => x.id_ficha_trat == id_ficha_trat).ToList();
            }
            catch (Exception ex)
            {
            }
            return PartialView("_TratamientoDetalles", detalle);
        }
        [HttpGet]
        public ActionResult TratamientoDetalles(int? id)
        {
            var detalle = new List<paciente_tratamientos_detalles>();
            detalle = db.paciente_tratamientos_detalles.Where(t => t.id_ficha_trat == id).OrderByDescending(x => x.fecha).ToList();
            var cabecera = db.paciente_tratamientos.Where(x => x.id_ficha_trat == id).FirstOrDefault();
            //Cargo la cabecera que estara disabled
            ViewBag.id_paciente = cabecera.id_paciente;
            ViewBag.paciente = db.paciente.Find(cabecera.id_paciente).nombre;
            var cliente = db.paciente.Find(cabecera.id_paciente).cliente.persona;
            ViewBag.cliente = cliente.nombre + " " + cliente.apellido;
            ViewBag.id_ficha_trat = cabecera.id_ficha_trat;
            ViewBag.fecha = cabecera.fecha.Value;
            ViewBag.id_patologia = cabecera.patologia.nombre;
            ViewBag.id_medico = cabecera.empleado.persona.nombre + " " + cabecera.empleado.persona.apellido;
            ViewBag.nombre_tratamiento = cabecera.nombre_tratamiento;
            ViewBag.fecha_inicio = cabecera.fecha_inicio.Value.ToString("yyyy-MM-dd");
            ViewBag.fecha_fin = cabecera.fecha_fin != null ? cabecera.fecha_fin.Value.ToString("yyyy-MM-dd") : "";
            ViewBag.id_fin_proceso = cabecera.id_ficha_trat;
            if (cabecera.estado == "P")
            {
                ViewBag.estado = "EN PROCESO";
            }
            else if (cabecera.estado == "F")
            {
                ViewBag.estado = "FINALIZADO";
            }
            else
                ViewBag.estado = "";


            return View(detalle);
        }

        [HttpPost]
        public ActionResult TratamientoDetalles([Bind(Include = "id_ficha_trat_det,id_ficha_trat,fecha,detalles,medicacion,avances,observaciones")] paciente_tratamientos_detalles tratamientos_detalles)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    tratamientos_detalles.detalles = Jagua.Clases.Comun.UppercaseFirst(tratamientos_detalles.detalles);
                    tratamientos_detalles.medicacion = tratamientos_detalles.detalles != null ? Jagua.Clases.Comun.UppercaseFirst(tratamientos_detalles.medicacion) : "";
                    tratamientos_detalles.avances = tratamientos_detalles.avances != null ? Jagua.Clases.Comun.UppercaseFirst(tratamientos_detalles.avances) : "";
                    tratamientos_detalles.observaciones = tratamientos_detalles.observaciones != null ? Jagua.Clases.Comun.UppercaseFirst(tratamientos_detalles.observaciones) : "";
                    tratamientos_detalles.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                    tratamientos_detalles.fecha_alta = DateTime.Now;
                    db.paciente_tratamientos_detalles.Add(tratamientos_detalles);
                    db.SaveChanges();
                    return RedirectToAction("TratamientoDetalles", "Clinica", tratamientos_detalles.id_ficha_trat_det);
                }

            }
            catch (Exception e)
            {

            }
            return RedirectToAction("TratamientoDetalles", "Clinica", tratamientos_detalles.id_ficha_trat_det);
        }

        [HttpGet]
        public ActionResult Tratamientos()
        {
            ViewBag.id_tipo_documento_nuevo = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            ViewBag.id_medico = new SelectList(db.empleado.Include("persona").Where(x => x.persona.id_cat_persona == 2).ToList(), "id_empleado", "persona.nombre");
            ViewBag.id_patologia = new SelectList(db.patologia.ToList(), "id_patologia", "nombre");
            return View();
        }
        [HttpPost]
        public ActionResult Tratamientos(paciente_tratamientos pactratamientos)
        {
            bool success = false;
            string mensajes = string.Empty;
            try
            {
                paciente_tratamientos tratamientos = new paciente_tratamientos();
                var id_ficha = db.ficha.Where(x => x.id_paciente == pactratamientos.id_paciente).FirstOrDefault().id_ficha;
                tratamientos.id_ficha = id_ficha;
                tratamientos.id_paciente = pactratamientos.id_paciente;
                tratamientos.nombre_tratamiento = Clases.Comun.UppercaseFirst(pactratamientos.nombre_tratamiento);
                tratamientos.estado = pactratamientos.estado;
                tratamientos.fecha_inicio = pactratamientos.fecha_inicio;
                tratamientos.fecha_fin = pactratamientos.fecha_fin;
                tratamientos.estado_patologia = pactratamientos.estado_patologia;
                tratamientos.fecha = pactratamientos.fecha;
                tratamientos.id_patologia = pactratamientos.id_patologia;
                tratamientos.id_medico = pactratamientos.id_medico;
                tratamientos.fecha_alta = DateTime.Now;
                tratamientos.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                db.paciente_tratamientos.Add(tratamientos);

                paciente_patologia pac_patologia = new paciente_patologia();
                pac_patologia.id_ficha = id_ficha;
                pac_patologia.id_paciente = pactratamientos.id_paciente;
                pac_patologia.fecha = pactratamientos.fecha;
                pac_patologia.id_patologia = pactratamientos.id_patologia; 
                pac_patologia.fecha_diagnostico = pactratamientos.fecha;
                pac_patologia.estado_patologia = pactratamientos.estado_patologia;
                pac_patologia.estado = pactratamientos.estado;
                pac_patologia.fecha_alta = DateTime.Now;
                pac_patologia.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                db.paciente_patologia.Add(pac_patologia);

                db.SaveChanges();
                success = true;
                mensajes = "Tratamiento Guardado!";

            }
            catch (Exception ex)
            {
                success = false;
                mensajes = "Error al Guardar Tratamientos!";
                Clases.Logger.Log.ErrorFormat("ERROR al Guardar Tratamientos: {0},{1}", ex.Message, ex.InnerException);
            }

            return Json(new { success, mensajes }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FinalizarProceso(int? id_ficha_trat)
        {
            bool success = false;
            string mensaje = string.Empty;
            try
            {
                paciente_tratamientos paciente_tratamientos = db.paciente_tratamientos.Find(id_ficha_trat);
                paciente_tratamientos.estado = "F";
                paciente_tratamientos.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
                paciente_tratamientos.fecha_mod = DateTime.Now;
                db.SaveChanges();
                success = true;
                mensaje = "El tratamiento ha finalizado!";
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = ex.Message;
                Clases.Logger.Log.ErrorFormat("ERROR FinalizarProceso: {0},{1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteDetalle(int? id_ficha_trat_det)
        {
            bool success = false;
            string mensaje = string.Empty;
            try
            {
                paciente_tratamientos_detalles paciente_tratamientos_detalles = db.paciente_tratamientos_detalles.Find(id_ficha_trat_det);
                db.paciente_tratamientos_detalles.Remove(paciente_tratamientos_detalles);
                db.SaveChanges();
                success = true;
                mensaje = "Detalle Eliminado";
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = ex.Message;
                Clases.Logger.Log.ErrorFormat("ERROR DeleteDetalle: {0},{1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            persona persona = db.persona.Find(id);
            cliente cliente = db.cliente.Where(x => x.id_persona == id).FirstOrDefault();


            persona.estado = "I";
            persona.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
            persona.fecha_mod = DateTime.Now;

            cliente.estado = "I";
            cliente.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
            cliente.fecha_mod = DateTime.Now;

            db.SaveChanges();
        }

        public JsonResult PacienteSearch(string search, FormCollection fc)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var PacienteList = (from a in db.paciente
                                    join b in db.cliente on a.id_cliente equals b.id_cliente
                                    join c in db.persona on b.id_persona equals c.id_persona
                                    where (a.nombre.Contains(search))
                                    select new { a.id_paciente, paciente = a.nombre, a.id_cliente, cliente_nombre = c.nombre, cliente_apellido = c.apellido, a.fecha_nac });
                //ViewBag.id_cliente = PacienteList.FirstOrDefault().id_cliente;
                //var nombre = db.cliente.Include("persona").Where(x => x.id_cliente == PacienteList.FirstOrDefault().id_cliente).FirstOrDefault();
                //ViewBag.cliente = nombre.persona.nombre + " " + nombre.persona.apellido;
                return Json(PacienteList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        public JsonResult ClienteSearch(string search, FormCollection fc)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var ClienteList = (from a in db.persona
                                   join b in db.cliente on a.id_persona equals b.id_persona
                                   join c in db.paciente on b.id_cliente equals c.id_cliente
                                   where (a.nro_documento.Contains(search) || a.nombre.Contains(search) || a.apellido.Contains(search)) && a.id_cat_persona == 1 //categoria persona 1 es cliente
                                   select new { a.nro_documento, c.id_paciente, paciente = c.nombre, b.id_cliente, cliente_nombre = a.nombre, cliente_apellido = a.apellido, c.fecha_nac });
                return Json(ClienteList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        public JsonResult getMascotas(int id_cliente)
        {
            if (!String.IsNullOrEmpty(id_cliente.ToString()))
            {
                //List<paciente> pacientes = new List<paciente>();
                var pacientes = (from a in db.paciente
                                 where a.id_cliente == id_cliente
                                 select new
                                 { a.id_paciente, a.nombre });
                //var pacientes = pac.Select(a => "<option value='" + a.id_paciente + "'>" + a.nombre + "</option>"); // por si quiero cargar del lado del servidor descomentar esta linea
                return Json(pacientes, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        public JsonResult FichaSearch(int search, FormCollection fc)
        {
            if (search > 0)
            {
                var fichas = (from a in db.ficha
                              join b in db.paciente on a.id_paciente equals b.id_paciente
                              join c in db.cliente on b.id_cliente equals c.id_cliente
                              join d in db.persona on c.id_persona equals d.id_persona
                              where (a.id_ficha == search)
                              select new { a.id_ficha, b.id_paciente, paciente = b.nombre, b.id_cliente, cliente_nombre = d.nombre, cliente_apellido = d.apellido, b.fecha_nac });


                if (fichas.ToList().Count() > 0)
                {
                    var data = fichas;
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var data = 0;
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return null;
            }

        }

        [HttpPost]
        public JsonResult guardarNuevoCliente([Bind(Include = "nombre,apellido,id_tipo_documento,nro_documento")] persona cliente)
        {
            if (ModelState.IsValid)
            {
                var clientes = db.persona.Where(x => x.nro_documento == cliente.nro_documento).FirstOrDefault();
                if (clientes == null)
                {
                    //desmenuzo el objeto para darle formato de primera letra en mayuscula
                    cliente.nombre = Jagua.Clases.Comun.UppercaseFirst(cliente.nombre);
                    cliente.apellido = Jagua.Clases.Comun.UppercaseFirst(cliente.apellido);
                    cliente.id_cat_persona = 1;
                    cliente.estado = "A";
                    cliente.fecha_alta = DateTime.Now;
                    cliente.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();

                    cliente datosCliente = new cliente();
                    datosCliente.id_persona = cliente.id_persona;
                    datosCliente.usuario_alta = Clases.SessionManager.SessionData.usuario;
                    datosCliente.fecha_alta = DateTime.Now;
                    datosCliente.estado = "A";

                    db.persona.Add(cliente);
                    db.cliente.Add(datosCliente);
                    db.SaveChanges();

                    return Json(new { Status = "Success" });
                }
                else
                {
                    // retorno cero para leer en el lado del cliente que ya existe la cedula
                    var data = "0";
                    return Json(data);
                }
            }
            return null;
        }

        public JsonResult getEdad(int id_paciente)
        {
            if (!String.IsNullOrEmpty(id_paciente.ToString()))
            {
                var data = (from a in db.paciente
                            where a.id_paciente == id_paciente
                            select new
                            { a.fecha_nac });
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        //public PartialViewResult _VacunasHechas(int id_paciente)
        //{
        //    try
        //    {
        //        if (id_paciente > 0)
        //        {
        //            var vacunas = (from a in db.paciente_vacuna
        //                           join b in db.vacuna on a.id_vacuna equals b.id_vacuna
        //                           where (a.id_paciente == id_paciente)
        //                           select new { b.id_vacuna, b.nombre, a.fecha_aplicacion });

        //            return PartialView("_VacunasHechas", vacunas);
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return null;
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult getListadoVacunas(int id_paciente)
        {
            var vacunas = new List<paciente_vacuna>();
            try
            {
                vacunas = db.paciente_vacuna.Where(t => t.id_paciente == id_paciente).ToList();
            }
            catch (Exception ex)
            {
            }
            return PartialView("_VacunasHechas", vacunas);
        }
    }
}