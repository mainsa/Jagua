﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class DepartamentosController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Departamentos
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            ViewBag.ddlPais = new SelectList(db.pais.ToList(), "id_pais", "descripcion");
            var departamento = db.departamento.Include(d => d.pais);
            return View(departamento.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var departamentos = db.departamento.ToList(); // genero una lista de paises
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            int pais = string.IsNullOrEmpty(fc["ddlPais"].ToString()) ? Convert.ToInt32("0") : Convert.ToInt32(fc["ddlPais"].ToString());

            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                departamentos = departamentos.Where(q => q.descripcion.ToLower().Contains(nombre.ToLower())).ToList(); //contains es la funcion que filtra la busqueda que contiene el metodo de busqueda
            }
            if (pais > 0)
            {
                departamentos = departamentos.Where(x => x.id_pais == pais).ToList();
            }


            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = nombre; 
            ViewBag.ddlPais = new SelectList(db.pais.ToList(), "id_pais", "descripcion", pais); //devuelvo el ddl como ultimo argumento la variable de busqueda
            //retorno la vista filtrada
            return View(departamentos.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        // GET: Departamentos/Create
        public ActionResult Create()
        {
            ViewBag.id_pais = new SelectList(db.pais, "id_pais", "descripcion");
            return View();
        }

        // POST: Departamentos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_departamento,descripcion,id_pais,estado")] departamento departamento)
        {
            if (ModelState.IsValid)
            {
                departamento.descripcion = Jagua.Clases.Comun.UppercaseFirst(departamento.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                departamento.estado = "A";
                departamento.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                departamento.fecha_alta = DateTime.Now;
                db.departamento.Add(departamento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_pais = new SelectList(db.pais, "id_pais", "descripcion", departamento.id_pais);
            return View(departamento);
        }

        // GET: Departamentos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            departamento departamento = db.departamento.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_pais = new SelectList(db.pais, "id_pais", "descripcion", departamento.id_pais);
            return View(departamento);
        }

        // POST: Departamentos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_departamento,descripcion,id_pais,estado,usuario_alta,fecha_alta")] departamento departamento)
        {
            if (ModelState.IsValid)
            {
                departamento.descripcion = Jagua.Clases.Comun.UppercaseFirst(departamento.descripcion); //Esta linea lo que hace es pone la primera letra de cada palabra en mayuscula
                departamento.usuario_mod = Clases.SessionManager.SessionData.usuario.ToLower();
                departamento.fecha_mod = DateTime.Now;
                db.Entry(departamento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_pais = new SelectList(db.pais, "id_pais", "descripcion", departamento.id_pais);
            return View(departamento);
        }

        // POST: Departamentos/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            departamento departamento = db.departamento.Find(id);
            departamento.estado = "I";
            departamento.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            departamento.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
