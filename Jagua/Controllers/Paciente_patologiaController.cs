﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;

namespace Jagua.Controllers
{
    public class Paciente_patologiaController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Paciente_patologia
        public ActionResult Index()
        {
            var paciente_patologia = db.paciente_patologia.Include(p => p.paciente).Include(p => p.patologia);
            return View(paciente_patologia.ToList());
        }

        // GET: Paciente_patologia/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paciente_patologia paciente_patologia = db.paciente_patologia.Find(id);
            if (paciente_patologia == null)
            {
                return HttpNotFound();
            }
            return View(paciente_patologia);
        }

        // GET: Paciente_patologia/Create
        public ActionResult Create()
        {
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre");
            ViewBag.id_patologia = new SelectList(db.patologia, "id_patologia", "descripcion");
            return View();
        }

        // POST: Paciente_patologia/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_pac_pato,id_paciente,id_patologia,fecha")] paciente_patologia paciente_patologia)
        {
            if (ModelState.IsValid)
            {
                db.paciente_patologia.Add(paciente_patologia);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", paciente_patologia.id_paciente);
            ViewBag.id_patologia = new SelectList(db.patologia, "id_patologia", "descripcion", paciente_patologia.id_patologia);
            return View(paciente_patologia);
        }

        // GET: Paciente_patologia/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paciente_patologia paciente_patologia = db.paciente_patologia.Find(id);
            if (paciente_patologia == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", paciente_patologia.id_paciente);
            ViewBag.id_patologia = new SelectList(db.patologia, "id_patologia", "descripcion", paciente_patologia.id_patologia);
            return View(paciente_patologia);
        }

        // POST: Paciente_patologia/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_pac_pato,id_paciente,id_patologia,fecha")] paciente_patologia paciente_patologia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paciente_patologia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_paciente = new SelectList(db.paciente, "id_paciente", "nombre", paciente_patologia.id_paciente);
            ViewBag.id_patologia = new SelectList(db.patologia, "id_patologia", "descripcion", paciente_patologia.id_patologia);
            return View(paciente_patologia);
        }

        // GET: Paciente_patologia/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paciente_patologia paciente_patologia = db.paciente_patologia.Find(id);
            if (paciente_patologia == null)
            {
                return HttpNotFound();
            }
            return View(paciente_patologia);
        }

        // POST: Paciente_patologia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            paciente_patologia paciente_patologia = db.paciente_patologia.Find(id);
            db.paciente_patologia.Remove(paciente_patologia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
