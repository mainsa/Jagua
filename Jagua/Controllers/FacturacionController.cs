﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Jagua.Models;
using JaguaEntityModel;
using PagedList;


namespace Jagua.Controllers
{
    public class FacturacionController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Facturacion
        [HttpGet]
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            //var x = DataTableExtensions.DataTableToCode(new Jagua.Datasets.Ticket.DT_CabeceraDataTable());
            //var y = DataTableExtensions.DataTableToCode(new Jagua.Datasets.Ticket.DT_DetalleDataTable());


            var operaciones = db.ventas_cabecera.Include(d => d.caja).Include(e => e.cliente).Include(f => f.medio_pago).Where(q => q.cobrado == "S");

            return View(operaciones.ToList().OrderByDescending(n => n.fecha).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var operaciones = db.ventas_cabecera.Where(q => q.cobrado == "S").ToList();
            var fecha_desde = fc["txtFechaDesde"]; // A traves del fc, obtengo el valor de la caja de texto
            var fecha_hasta = fc["txtFechaHasta"]; // A traves del fc, obtengo el valor de la caja de texto
            var estado = fc["ddlEstado"]; // A traves del fc, obtengo el valor de la caja de texto
            var cliente = fc["txtCliente"]; // A traves del fc, obtengo el valor de la caja de texto
            var factura = fc["txtFactura"]; // A traves del fc, obtengo el valor de la caja de texto
            var producto = fc["txtProducto"];

            if (!string.IsNullOrEmpty(fecha_desde)) // pregunto si no esta vacio la variable nombre
            {
                var fecha = DateTime.Parse(fecha_desde);
                operaciones = operaciones.Where(q => q.fecha >= fecha).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(fecha_hasta))
            {
                fecha_hasta = fecha_hasta + " 23:59:59"; //23.59 para que tome busqueda del dia
                var fecha = DateTime.Parse(fecha_hasta);
                operaciones = operaciones.Where(q => q.fecha <= fecha).ToList();
            }
            if (!string.IsNullOrEmpty(estado))
            {
                operaciones = operaciones.Where(q => q.estado == estado).ToList();
            }
            //if (!string.IsNullOrEmpty(cliente))
            //{
            //    operaciones = operaciones.Where(q => q.persona.nombre.Contains(cliente) || q.cliente.persona.apellido.Contains(cliente) || q.cliente.persona.nro_documento==cliente ).ToList();
            //}
            if (!string.IsNullOrEmpty(factura))
            {
                operaciones = operaciones.Where(q => q.comprobante.Contains(factura)).ToList();
            }
            //if (!string.IsNullOrEmpty(producto))
            //{
            //    foreach (var item in operaciones) {
            //        var detalle_productos = db.ventas_detalle.Where(x => x.id_venta_cabecera == item.id_ventas_cabecera).ToList();
            //        detalle_productos = detalle_productos.Where(q => q.descripcion.Contains(producto)).ToList();
            //    }
            //}
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            //ViewBag.txtNombre = nombre;

            //retorno la vista filtrada
            return View(operaciones.OrderByDescending(n => n.fecha).ToPagedList(pageIndex, pageSize));

        }

        [HttpGet]
        public ActionResult CargarVenta()
        {
            var id_usuario = Jagua.Clases.SessionManager.SessionData.id_usuario;
            var cajas = db.caja.Where(x => x.id_usuario == id_usuario).ToList();
            if (cajas.Count() > 0) {
                var usuario = Jagua.Clases.SessionManager.SessionData.usuario.ToLower();
                var aperturas = db.apertura_cierre.Where(x => x.usuario_apertura.ToLower() == usuario & x.fecha_cierre == null).ToList();
                if (aperturas.Count() > 0)
                {
                    ViewBag.id_tipo_comprobante = new SelectList(db.tipo_comprobante.ToList(), "id_tipo_comprobante", "descripcion", 3);
                    ViewBag.id_medio_pago = new SelectList(db.medio_pago.ToList(), "id_medio_pago", "descripcion");
                    ViewBag.id_tipo_documento = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
                    return View();
                }
                else
                {
                    TempData["msg"] = "<script>swal({text: 'Debe realizar la apertura de una caja',type: 'warning'});</script>";
                    return RedirectToAction("AperturaCierre", "Cajas");
                }
            }
            else {
                TempData["msg"] = "<script>swal({text: 'No posee una caja habilitada para este usuario, habilite una caja',type: 'warning'});</script>";
                return RedirectToAction("Create", "Cajas");
            }
        }

        [HttpPost]
        public JsonResult CargarVenta(ventas_cabecera cabecera, ventas_detalle[] datosDetalle) //recibo como parametro la cabecera y el detalle que me envia desde la vista
        {
            ViewBag.id_tipo_comprobante = new SelectList(db.tipo_comprobante.ToList(), "id_tipo_comprobante", "descripcion");
            ViewBag.id_medio_pago = new SelectList(db.medio_pago.ToList(), "id_medio_pago", "descripcion");
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            try
            {
                if (cabecera != null && datosDetalle != null)
                {
                    cabecera.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                    cabecera.fecha_alta = DateTime.Now;
                    db.ventas_cabecera.Add(cabecera);
                    db.SaveChanges();

					var nro_item = 0;

					//se guarda el detalle
					foreach (var datosdetalle in datosDetalle)
                    {
                        var guardarDetalle = new ventas_detalle();
                        guardarDetalle.id_venta_cabecera = cabecera.id_ventas_cabecera;
						guardarDetalle.nro_item = nro_item + 1;
                        guardarDetalle.id_producto = datosdetalle.id_producto;
                        guardarDetalle.codigo_barra = datosdetalle.codigo_barra;
                        guardarDetalle.descripcion = datosdetalle.descripcion;
                        guardarDetalle.cantidad = datosdetalle.cantidad;
                        guardarDetalle.precio_unitario = datosdetalle.precio_unitario;
                        guardarDetalle.iva10 = datosdetalle.iva10;
                        guardarDetalle.iva5 = datosdetalle.iva5;
                        guardarDetalle.exentas = datosdetalle.exentas;
                        guardarDetalle.gravadas = datosdetalle.gravadas;
                        guardarDetalle.subtotal = datosdetalle.subtotal;
						db.ventas_detalle.Add(guardarDetalle);
                        db.SaveChanges();
                        //Actualizar el stock
                        var producto = db.producto.Where(x => x.id_producto == datosdetalle.id_producto).FirstOrDefault();
						producto.stock = (producto.stock - datosdetalle.cantidad);
						db.Entry(producto).State = EntityState.Modified;
						db.SaveChanges();
					}
                    db.SaveChanges();

                    //CALCULAMOS LOS PUNTOS ---------------
                    Clases.Puntajes.VerificarPuntajes(cabecera.id_cliente, cabecera.id_ventas_cabecera);
                    //-------------------------------------

                    Imprimir(cabecera, datosDetalle);

                    return Json(new { Status = "Success" });
                }
                else
                {
                    ModelState.AddModelError("", "No se puede registrar los cabecera sin detalle de ventas ");
                    var result = "0";
                    return Json(result);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
                var result = "1";
                return Json(result);
            }
            
        }

        //Metodo para usar el autocomplete de jquery ui en clientes
        public JsonResult ClienteSearch(string search, FormCollection fc)
       { 
            if(!String.IsNullOrEmpty(search))
            {
                var ClienteList = (from a in db.persona
                                   join b in db.cliente on a.id_persona equals b.id_persona
                            where (a.nro_documento.Contains(search) || a.nombre.Contains(search) || a.apellido.Contains(search)) && a.id_cat_persona == 1 //categoria persona 1 es cliente
                            select new { a.nro_documento, a.nombre, a.apellido, b.id_cliente});
            return Json(ClienteList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        public JsonResult ProductDetails(string search)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var ProductosList = (from N in db.producto
                                   where (N.id_producto.ToString().Contains(search) || N.descripcion.ToLower().Contains(search.ToLower()) || N.codigo_barra.Contains(search))
                                   select new { N.id_producto, N.descripcion, N.codigo_barra, N.precio, N.iva });
                return Json(ProductosList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

		public JsonResult getCaja()
		{
            var id_usuario = Jagua.Clases.SessionManager.SessionData.id_usuario;
			if (!String.IsNullOrEmpty(id_usuario.ToString()))
			{
				var data = (from a in db.caja
                            join b in db.apertura_cierre on a.id_caja equals b.id_caja
							where (a.id_usuario ==id_usuario & b.fecha_cierre == null)
							select new { a.descripcion, a.id_caja });
                if (data.ToList().Count() > 0)
                {
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("0", JsonRequestBehavior.AllowGet);
                }
		    }
			else
			{
				return null;
			}
		}

	    public JsonResult getNumeracion(string id)
		{
			if (!String.IsNullOrEmpty(id))
			{
				var id_tipo_comp = Convert.ToInt32(id);
				var data = (from N in db.numeracion_documentos
							  where (N.id_tipo_comprobante == id_tipo_comp)
							  select new { N.nro_actual, N.nro_hasta });

				return Json(data, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return null;
			}

		}

		[HttpPost]
		public JsonResult guardarNuevoCliente([Bind(Include = "nombre,apellido,id_tipo_documento,nro_documento")] persona cliente)
		{
			if (ModelState.IsValid)
			{
				var clientes = db.persona.Where(x => x.nro_documento == cliente.nro_documento).FirstOrDefault();
				if ( clientes == null) { 
				//desmenuzo el objeto para darle formato de primera letra en mayuscula
				cliente.nombre = Jagua.Clases.Comun.UppercaseFirst(cliente.nombre);
				cliente.apellido = Jagua.Clases.Comun.UppercaseFirst(cliente.apellido);
				cliente.id_cat_persona = 1;
				cliente.estado = "A";
				cliente.fecha_alta = DateTime.Now;
				cliente.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();

                    cliente datosCliente = new cliente();
				datosCliente.id_persona = cliente.id_persona;
				datosCliente.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();
                    datosCliente.fecha_alta = DateTime.Now;
				datosCliente.estado = "A";

				db.persona.Add(cliente);
				db.cliente.Add(datosCliente);
				db.SaveChanges();

				return Json(new {Status = "Success" });
				}
				else
				{
					// retorno cero para leer en el lado del cliente que ya existe la cedula
					var data = "0";
					return Json(data);
				}
			}
			return null;
		}

        public void Imprimir(ventas_cabecera cabecera, ventas_detalle[] datosDetalle)
        {
            try
            {
                Jagua.Datasets.Reportes dts = new Datasets.Reportes();
                //Datatable Cabecera
                var dt1 = new Jagua.Datasets.Reportes.Ticket_CabeceraDataTable();
                var row1 = dt1.NewTicket_CabeceraRow();
                //Datatable Detalle
                var dt2 = new Jagua.Datasets.Reportes.Ticket_DetalleDataTable();
                var row2 = dt2.NewTicket_DetalleRow();

                //Carga de Cabecera
                row1 = dt1.NewTicket_CabeceraRow();
                row1.id_ventas_cabecera = cabecera.id_ventas_cabecera.ToString();
                row1.fecha = cabecera.fecha.Value.ToString("dd/MM/yyyy");
                var caja = db.caja.Where(x => x.id_caja == cabecera.id_caja).FirstOrDefault();
                row1.caja = caja.descripcion;
                row1.comprobante = cabecera.comprobante.ToString();
                row1.cliente = cabecera.nombres;
                row1.cant_articulos = cabecera.cant_articulos.ToString();
                row1.medio_pago = cabecera.medio_pago.descripcion;
                row1.importe_total = cabecera.importe_total.ToString();
                row1.usuario = Clases.SessionManager.SessionData.usuario.ToLower();
                dt1.Rows.Add(row1);

                //Carga de detalle
                foreach (var item in datosDetalle)
                {
                    row2 = dt2.NewTicket_DetalleRow();
                    row2.id_venta_cabecera = item.id_venta_cabecera.ToString();
                    row2.nro_item = item.nro_item.ToString();
                    row2.id_producto = item.id_producto.ToString();
                    row2.codigo_barra = item.codigo_barra;
                    row2.descripcion = item.descripcion;
                    row2.cantidad = item.cantidad.ToString();
                    row2.precio = item.precio_unitario.ToString();
                    row2.iva5 = item.iva5.ToString();
                    row2.iva10 = item.iva10.ToString();
                    row2.gravadas = item.gravadas.ToString();
                    row2.exentas = item.exentas.ToString();
                    row2.subtotal = item.subtotal.ToString();
                    dt2.Rows.Add(row2);
                }

                //Carga de los datatables en el dataset
                dts.Tables["Ticket_Cabecera"].Merge(dt1);
                dts.Tables["Ticket_Detalle"].Merge(dt2);

                //pasa datos 
                Jagua.Clases.Comun.ReportesParametros Pmt = new Clases.Comun.ReportesParametros();
                //Pmt.ReportPath = Server.MapPath("~/Reportes/Ticket.rpt");
                Pmt.ReportSource = (object)dts;
                Pmt.NombreArchivo = "Ticket";
                Clases.Comun.ReportParameters.DatosReporte = Pmt;

                //Para el uso de este metodo la impresora a ser usada debe estar como predeterminada

                CrystalDecisions.CrystalReports.Engine.ReportDocument CReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                if (Jagua.Clases.Comun.ReportParameters.DatosReporte != null)
                {
                    string archivo = Jagua.Clases.Comun.ReportParameters.DatosReporte.NombreArchivo;
                    CReport.Load(Server.MapPath("~/Reportes/Ticket.rpt")); //se pone la ruta del reporte que se va a enviar a la impresora
                     CReport.SetDataSource(Jagua.Clases.Comun.ReportParameters.DatosReporte.ReportSource);
                    //CReport.SetDataSource((object)dts);
                    CReport.PrintToPrinter(1, false, 0, 0);
                }

                RedirectToAction("Index");
            }
            catch (Exception e)
            {

            }

        }

        public void ImprimirTicket(int? id)
        {

        }

        public void Anular(int id)
        {
            ventas_cabecera ventas = db.ventas_cabecera.Find(id);
            ventas.estado = "A";
            ventas.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            ventas.fecha_mod = DateTime.Now;
            db.SaveChanges();

            //Descontar stock
            var detalle = db.ventas_detalle.ToList().Where(x => x.id_venta_cabecera == id);
            foreach (var item in detalle)
            {
                var producto = db.producto.Where(x => x.id_producto == item.id_producto).FirstOrDefault();
                producto.stock = producto.stock >= item.cantidad ? (producto.stock + item.cantidad) : 0;
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

    }
}