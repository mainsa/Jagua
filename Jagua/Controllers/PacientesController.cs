﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using Jagua.Models;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    [ControlFilter]
    public class PacientesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: pacientes
        public ActionResult Index(int? page, string currentFilter)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.ddlRaza = new SelectList(db.raza.ToList(), "id_raza", "descripcion");
            var paciente = db.paciente.Include(d => d.raza).Include(x => x.cliente.persona);
            if (SessionManager.SessionData.usuario_rol.First().rol.nombre.ToLower().Equals("cliente"))
            {
                paciente = paciente.Where(t => t.id_cliente == SessionManager.SessionData.id_cliente);
            }
            if (!string.IsNullOrEmpty(currentFilter))
            {
                var ArrcurrentFilter = currentFilter.Split('_');
                if (ArrcurrentFilter.Length > 0)
                {
                    int id_paciente = (string.IsNullOrEmpty(ArrcurrentFilter[0]) ? 0 : int.Parse(ArrcurrentFilter[0]));
                    var nombre = (string.IsNullOrEmpty(ArrcurrentFilter[1]) ? string.Empty : ArrcurrentFilter[0]);
                    var cliente = (string.IsNullOrEmpty(ArrcurrentFilter[2]) ? string.Empty : ArrcurrentFilter[0]);
                    int raza = (string.IsNullOrEmpty(ArrcurrentFilter[3]) ? 0 : int.Parse(ArrcurrentFilter[0]));
                    if (id_paciente > 0)
                    {
                        paciente = paciente.Where(x => x.id_paciente == id_paciente);
                    }
                    if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
                    {
                        paciente = paciente.Where(q => q.nombre.ToLower().Contains(nombre.ToLower()));
                    }
                    if (!string.IsNullOrEmpty(cliente)) // pregunto si no esta vacio la variable nombre
                    {
                        paciente = paciente.Where(q => q.cliente.persona.nombre.ToLower().Contains(nombre.ToLower()) || q.cliente.persona.apellido.ToLower().Contains(nombre.ToLower()) || q.cliente.persona.nro_documento == cliente);
                    }
                    if (raza > 0)
                    {
                        paciente = paciente.Where(x => x.id_raza == raza);
                    }

                    ViewBag.txtPaciente = id_paciente.ToString();
                    ViewBag.txtNombre = nombre;
                    ViewBag.txtCliente = cliente;
                }
            }
            return View(paciente.OrderByDescending(n => n.nombre).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            string currentFilter = string.Empty;
            var paciente = db.paciente.ToList(); // genero una lista de pacientes
            int id_paciente = string.IsNullOrEmpty(fc["txtPaciente"].ToString()) ? 0 : Convert.ToInt32(fc["txtPaciente"].ToString());
            var nombre = fc["txtNombre"]; // A traves del fc, obtengo el valor de la caja de texto
            var cliente = fc["txtCliente"];
            int raza = string.IsNullOrEmpty(fc["ddlRaza"].ToString()) ? Convert.ToInt32("0") : Convert.ToInt32(fc["ddlRaza"].ToString());

            if (id_paciente > 0)
            {
                paciente = paciente.Where(x => x.id_paciente.Equals(id_paciente)).ToList();
            }
            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                paciente = paciente.Where(q => q.nombre.ToLower().Contains(nombre.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(cliente)) // pregunto si no esta vacio la variable nombre
            {
                paciente = paciente.Where(q => q.cliente.persona.nombre.ToLower().Contains(nombre.ToLower()) || q.cliente.persona.apellido.ToLower().Contains(nombre.ToLower()) || q.cliente.persona.nro_documento == cliente).ToList();
            }
            if (raza > 0)
            {
                paciente = paciente.Where(x => x.id_raza == raza).ToList();
            }
            currentFilter = id_paciente + "_" + nombre + "_" + cliente + "_" + raza;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.txtPaciente = id_paciente.ToString();
            ViewBag.txtNombre = nombre;
            ViewBag.txtCliente = cliente;
            ViewBag.ddlRaza = new SelectList(db.raza.ToList(), "id_raza", "descripcion", raza);
            if (SessionManager.SessionData.usuario_rol.First().rol.nombre.ToLower().Equals("cliente"))
            {
                paciente = paciente.Where(t => t.id_cliente == SessionManager.SessionData.id_cliente).ToList();
            }
            //retorno la vista filtrada
            return View(paciente.OrderByDescending(n => n.nombre).ToPagedList(pageIndex, pageSize));

        }

        // GET: pacientes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paciente paciente = await db.paciente.FindAsync(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_especie = new SelectList(db.especie, "id_especie", "nombre", paciente.id_especie);
            ViewBag.id_raza = new SelectList(db.raza, "id_raza", "descripcion", paciente.id_raza);
            //ViewBag.id_cliente = new SelectList(db.persona, "id_persona", "nombre", paciente.id_cliente);
            var cliente = db.cliente.Include(x => x.persona).Where(x => x.id_cliente == paciente.id_cliente).FirstOrDefault();
            ViewBag.cliente = cliente.persona.nombre + " " + cliente.persona.apellido;
            ViewBag.sexo = paciente.sexo;
            ViewBag.fecha_nac = paciente.fecha_nac;
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            return PartialView("Details", paciente);
        }

        // GET: pacientes/Create
        public ActionResult Create()
        {
            ViewBag.id_especie = new SelectList(db.especie.ToList(), "id_especie", "nombre");
            ViewBag.id_raza = new SelectList(db.raza.ToList(), "id_raza", "descripcion");
            ViewBag.id_cliente = new SelectList(db.persona.ToList(), "id_persona", "nombre", "apellido");
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            return View();
        }

        // POST: pacientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_paciente,id_cliente,nombre,fecha_nac,id_especie,id_raza,sexo,color,tamaño")] paciente paciente)
        {
            if (ModelState.IsValid)
            {
                //Guardar Paciente
                paciente.nombre = Jagua.Clases.Comun.UppercaseFirst(paciente.nombre);
                paciente.fecha_alta = DateTime.Now;
                paciente.usuario_alta = Clases.SessionManager.SessionData.usuario;
                db.paciente.Add(paciente);
                //Crear y guardar ficha
                ficha ficha = new ficha();
                ficha.fecha = DateTime.Now;
                ficha.id_paciente = paciente.id_paciente;
                ficha.fecha_ult_visita = DateTime.Now;
                db.ficha.Add(ficha);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_raza = new SelectList(db.raza.ToList(), "id_raza", "descripcion", paciente.id_raza);
            ViewBag.id_cliente = new SelectList(db.persona.ToList(), "id_persona", "nombre", "apellido", paciente.id_cliente);
            //ViewBag.id_cliente = new SelectList(db.cliente, "id_cliente", "nombre", paciente.id_cliente);
            return View(paciente);
        }

        // GET: pacientes/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paciente paciente = db.paciente.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_especie = new SelectList(db.especie.ToList(), "id_especie", "nombre", paciente.id_especie);
            ViewBag.id_raza = new SelectList(db.raza.ToList(), "id_raza", "descripcion", paciente.id_raza);
            //ViewBag.id_cliente = new SelectList(db.persona, "id_persona", "nombre", paciente.id_cliente);
            var cliente = db.cliente.Include(x => x.persona).Where(x => x.id_cliente == paciente.id_cliente).FirstOrDefault();
            ViewBag.cliente = cliente.persona.nombre + " " + cliente.persona.apellido;
            ViewBag.sexo = paciente.sexo;
            ViewBag.fecha_nac = paciente.fecha_nac;
            ViewBag.id_tipo_documento = new SelectList(db.tipo_documento.ToList(), "id_tipo_documento", "abreviatura");
            return View(paciente);
        }

        // POST: pacientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "id_paciente,id_cliente,nombre,fecha_nac,id_especie,id_raza,sexo,color,tamaño")] paciente paciente)
        {
            if (ModelState.IsValid)
            {
                paciente.nombre = Jagua.Clases.Comun.UppercaseFirst(paciente.nombre);
                paciente.fecha_mod = DateTime.Now;
                paciente.usuario_mod = Clases.SessionManager.SessionData.usuario;
                db.Entry(paciente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(paciente);
        }

        // GET: pacientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            paciente paciente = db.paciente.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        // POST: pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            paciente paciente = db.paciente.Find(id);
            db.paciente.Remove(paciente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult PacienteSearch(string search, FormCollection fc)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var PacienteList = (from a in db.paciente
                                    where (a.nombre.Contains(search))
                                    select new { a.nombre });
                return Json(PacienteList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }

        public JsonResult ClienteSearch(string search, FormCollection fc)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var ClienteList = (from a in db.persona
                                   join b in db.cliente on a.id_persona equals b.id_persona
                                   where (a.nro_documento.Contains(search) || a.nombre.Contains(search) || a.apellido.Contains(search)) && a.id_cat_persona == 1 //categoria persona 1 es cliente
                                   select new { a.nro_documento, a.nombre, a.apellido, b.id_cliente });
                return Json(ClienteList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }
        // GET: Historial Clinico
        public ActionResult Historial(int id, int? page, string currentFilter)
        {
            ViewBag.idPaciente_ = id;
            int pageSize = 20;
            int pageIndex = 1;
            ViewBag.CurrentFilter = currentFilter;
            ViewBag.txtProcedimiento = currentFilter;
            string ddlProcedimiento = "";
            string procedimiento = "";
            if (!string.IsNullOrEmpty(currentFilter))
            {
                var Arr = currentFilter.Split('_');
                if (Arr.Length > 0)
                {
                    ddlProcedimiento = Arr[1];
                    procedimiento = Arr[0];
                }
            }
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            //var ficha = db.ficha.Include(f => f.paciente).Where(x => x.id_paciente == id).FirstOrDefault();
            var paciente = db.paciente.Where(x => x.id_paciente == id).FirstOrDefault();
            var cli = db.cliente.Include(x => x.persona).Where(x => x.id_cliente == paciente.id_cliente).FirstOrDefault();
            var cliente = cli.persona.nro_documento + " " + cli.persona.nombre + " " + cli.persona.apellido;

            List<Historial> historial = new List<Historial>();
            if (ddlProcedimiento == "LABORATORIO" || ddlProcedimiento == string.Empty)
            {
                var laboratorio = (from a in db.paciente
                                   join b in db.paciente_laboratorio on a.id_paciente equals b.id_paciente
                                   join c in db.paciente_laboratorio_detalle on b.id_ficha_lab equals c.id_ficha_lab
                                   join d in db.laboratorio on c.id_laboratorio equals d.id_laboratorio
                                   where a.id_paciente == id
                                   select new Historial
                                   {
                                       id = b.id_ficha_lab,
                                       id_paciente = a.id_paciente,
                                       fecha = b.fecha.Value,
                                       descripcion = d.nombre,
                                       resultado = b.resultado
                                   }).OrderBy(x => x.fecha).ToList();
                //modificado por marina
                if (laboratorio.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "LABORATORIO",
                        descripcion = string.Empty
                    });
                    foreach (var item in laboratorio)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id,
                            id_paciente = item.id_paciente,
                            descripcion = item.descripcion,
                            fecha = item.fecha,
                            resultado = item.resultado
                        });
                    }
                    //[dbo].[paciente_patologia]
                    var paciente_patologia = db.paciente_patologia.Where(t => t.id_paciente.Value.Equals(id)).OrderBy(x => x.fecha).ToList();
                    if (paciente_patologia.Any())
                    {
                        historial.Add(new Models.Historial
                        {
                            procedimiento = "PATOLOGIA",
                            descripcion = string.Empty
                        });
                        foreach (var item in paciente_patologia)
                        {
                            historial.Add(new Models.Historial
                            {
                                id = item.id_ficha_pat,
                                id_paciente = item.id_paciente.Value,
                                descripcion = item.patologia.descripcion,
                                fecha = item.fecha.Value,
                                resultado = "Estado: " + item.estado_patologia
                            });
                        }
                    }
                }
            }
            if (ddlProcedimiento == "CONSULTA" || ddlProcedimiento == string.Empty)
            {
                //[dbo].[paciente_consulta]
                var paciente_consulta = db.paciente_consulta.Where(t => t.id_paciente.Value.Equals(id)).OrderBy(x => x.fecha).ToList();
                if (paciente_consulta.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "CONSULTAS",
                        descripcion = string.Empty
                    });
                    foreach (var item in paciente_consulta)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id_ficha_consulta,
                            id_paciente = item.id_paciente.Value,
                            descripcion = item.indicaciones,
                            fecha = item.fecha.Value,
                            resultado = item.diagnostico
                        });
                    }
                }
            }

            if (ddlProcedimiento.Contains("LABORATORIO") || ddlProcedimiento.Contains("BAÑO") || ddlProcedimiento.Contains("PELUQUERIA") || ddlProcedimiento.Contains("TRATAMIENTO") || ddlProcedimiento == string.Empty)
            {
                //[dbo].[paciente_tratamientos]
                var paciente_tratamientos = db.paciente_tratamientos.Where(t => t.id_paciente.Equals(id)).OrderBy(x => x.fecha).ToList();
                if (paciente_tratamientos.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "TRATAMIENTOS",
                        descripcion = string.Empty
                    });
                    foreach (var item in paciente_tratamientos)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id_ficha_trat,
                            id_paciente = item.id_paciente,
                            descripcion = item.nombre_tratamiento,
                            fecha = item.fecha.Value,
                            resultado = (item.paciente_tratamientos_detalles.Any()) ? item.paciente_tratamientos_detalles.FirstOrDefault().detalles : string.Empty
                        });
                    }
                }
            }
            if (ddlProcedimiento == "VACUNA" || ddlProcedimiento == string.Empty)
            {
                //[dbo].[paciente_vacuna]
                var paciente_vacuna = db.paciente_vacuna.Where(t => t.id_paciente.Value.Equals(id)).OrderBy(x => x.fecha).ToList();
                if (paciente_vacuna.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "VACUNAS",
                        descripcion = string.Empty
                    });
                    foreach (var item in paciente_vacuna)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id_ficha_vacuna,
                            id_paciente = item.id_paciente.Value,
                            descripcion = item.vacuna.descripcion,
                            fecha = item.fecha.Value,
                            resultado = item.prox_aplicacion != null ? "Próxima Aplicación: " + item.prox_aplicacion.Value.ToString("dd/MM/yyyy") : ""
                        });
                    }
                }
            }
            if (!string.IsNullOrEmpty(procedimiento))
            {
                procedimiento = procedimiento.ToLower();
                historial = historial.Where(t => t.descripcion.ToLower().Contains(procedimiento)).ToList();
            }
            ViewBag.ddlProcedimiento = new SelectList(db.servicios.ToList(), "descripcion", "descripcion");
            ViewBag.id_paciente = paciente.id_paciente;
            ViewBag.paciente = paciente.nombre;
            ViewBag.cliente = cliente;
            //ViewBag.visita = string.IsNullOrEmpty(paciente.ult_visita.ToString()) ? "" : paciente.ult_visita.ToString();
            //ViewBag.vacunas = new SelectList(db.vacuna.ToList(), "id_vacuna", "nombre");

            if (historial.Count() > 0)
            {
                return View(historial.ToList().ToPagedList(pageIndex, pageSize));
            }
            else
            {
                return View(historial.ToPagedList(pageIndex, pageSize));
            }



        }
        [HttpPost]
        public ActionResult Historial(FormCollection form)
        {
            ViewBag.idPaciente_ = form["idPaciente_"];
            ViewBag.txtProcedimiento = form["txtProcedimiento"];
            int id = ViewBag.idPaciente_ != null ? int.Parse(ViewBag.idPaciente_) : 0;
            string procedimiento = string.IsNullOrEmpty(ViewBag.txtProcedimiento) ? string.Empty : ViewBag.txtProcedimiento;
            string ddlProcedimiento = string.IsNullOrEmpty(form["ddlProcedimiento"]) ? string.Empty : form["ddlProcedimiento"].ToUpper();
            ViewBag.CurrentFilter = procedimiento + "_" + ddlProcedimiento;
            int pageSize = 20;
            int pageIndex = 1;
            //var ficha = db.ficha.Include(f => f.paciente).Where(x => x.id_paciente == id).FirstOrDefault();
            var paciente = db.paciente.Where(x => x.id_paciente == id).FirstOrDefault();
            var cli = db.cliente.Include(x => x.persona).Where(x => x.id_cliente == paciente.id_cliente).FirstOrDefault();
            var cliente = cli.persona.nro_documento + " " + cli.persona.nombre + " " + cli.persona.apellido;

            List<Historial> historial = new List<Historial>();
            if (ddlProcedimiento == "LABORATORIO" || ddlProcedimiento == string.Empty)
            {
                var laboratorio = (from a in db.paciente
                                   join b in db.paciente_laboratorio on a.id_paciente equals b.id_paciente
                                   join c in db.paciente_laboratorio_detalle on b.id_ficha_lab equals c.id_ficha_lab
                                   join d in db.laboratorio on c.id_laboratorio equals d.id_laboratorio
                                   where a.id_paciente == id
                                   select new Historial
                                   {
                                       id = b.id_ficha_lab,
                                       id_paciente = a.id_paciente,
                                       fecha = b.fecha.Value,
                                       descripcion = d.nombre,
                                       resultado = b.resultado
                                   }
                                   ).OrderBy(x => x.fecha).ToList();
                //modificado por marina
                if (laboratorio.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "LABORATORIO",
                        descripcion = string.Empty
                    });
                    foreach (var item in laboratorio)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id,
                            id_paciente = item.id_paciente,
                            descripcion = item.descripcion,
                            fecha = item.fecha,
                            resultado = item.resultado
                        });
                    }
                    //[dbo].[paciente_patologia]
                    var paciente_patologia = db.paciente_patologia.Where(t => t.id_paciente.Value.Equals(id)).OrderBy(x => x.fecha).ToList();
                    if (paciente_patologia.Any())
                    {
                        historial.Add(new Models.Historial
                        {
                            procedimiento = "PATOLOGIA",
                            descripcion = string.Empty
                        });
                        foreach (var item in paciente_patologia)
                        {
                            historial.Add(new Models.Historial
                            {
                                id = item.id_ficha_pat,
                                id_paciente = item.id_paciente.Value,
                                descripcion = item.patologia.descripcion,
                                fecha = item.fecha.Value,
                                resultado = "Estado: " + item.estado_patologia
                            });
                        }
                    }
                }
            }
            if (ddlProcedimiento == "CONSULTA" || ddlProcedimiento == string.Empty)
            {
                //[dbo].[paciente_consulta]
                var paciente_consulta = db.paciente_consulta.Where(t => t.id_paciente.Value.Equals(id)).OrderBy(x => x.fecha).ToList();
                if (paciente_consulta.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "CONSULTAS",
                        descripcion = string.Empty
                    });
                    foreach (var item in paciente_consulta)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id_ficha_consulta,
                            id_paciente = item.id_paciente.Value,
                            descripcion = item.indicaciones,
                            fecha = item.fecha.Value,
                            resultado = item.diagnostico
                        });
                    }
                }
            }

            if (ddlProcedimiento.Contains("LABORATORIO") || ddlProcedimiento.Contains("BAÑO") || ddlProcedimiento.Contains("PELUQUERIA") || ddlProcedimiento.Contains("TRATAMIENTO") || ddlProcedimiento == string.Empty
                )
            {
                //[dbo].[paciente_tratamientos]
                var paciente_tratamientos = db.paciente_tratamientos.Where(t => t.id_paciente.Equals(id)).OrderBy(x => x.fecha).ToList();
                if (paciente_tratamientos.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "TRATAMIENTOS",
                        descripcion = string.Empty
                    });
                    foreach (var item in paciente_tratamientos)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id_ficha_trat,
                            id_paciente = item.id_paciente,
                            descripcion = item.nombre_tratamiento,
                            fecha = item.fecha.Value,
                            resultado = (item.paciente_tratamientos_detalles.Any()) ? item.paciente_tratamientos_detalles.FirstOrDefault().detalles : string.Empty
                        });
                    }
                }
            }
            if (ddlProcedimiento == "VACUNA" || ddlProcedimiento == string.Empty)
            {
                //[dbo].[paciente_vacuna]
                var paciente_vacuna = db.paciente_vacuna.Where(t => t.id_paciente.Value.Equals(id)).OrderBy(x => x.fecha).ToList();
                if (paciente_vacuna.Any())
                {
                    historial.Add(new Models.Historial
                    {
                        procedimiento = "VACUNAS",
                        descripcion = string.Empty
                    });
                    foreach (var item in paciente_vacuna)
                    {
                        historial.Add(new Models.Historial
                        {
                            id = item.id_ficha_vacuna,
                            id_paciente = item.id_paciente.Value,
                            descripcion = item.vacuna.descripcion,
                            fecha = item.fecha.Value,
                            resultado = item.prox_aplicacion != null ? "Próxima Aplicación: " + item.prox_aplicacion.Value.ToString("dd/MM/yyyy") : ""
                        });
                    }
                }
            }
            if (!string.IsNullOrEmpty(procedimiento))
            {
                procedimiento = procedimiento.ToLower();
                historial = historial.Where(t => t.descripcion.ToLower().Contains(procedimiento)).ToList();
            }
            ViewBag.ddlProcedimiento = new SelectList(db.servicios.ToList(), "descripcion", "descripcion", ddlProcedimiento);
            ViewBag.id_paciente = paciente.id_paciente;
            ViewBag.paciente = paciente.nombre;
            ViewBag.cliente = cliente;
            //ViewBag.visita = string.IsNullOrEmpty(paciente.ult_visita.ToString()) ? "" : paciente.ult_visita.ToString();
            ViewBag.vacunas = new SelectList(db.vacuna.ToList(), "id_vacuna", "nombre");

            if (historial.Count() > 0)
            {
                return View(historial.ToList().ToPagedList(pageIndex, pageSize));
            }
            else
            {
                return View(historial.ToPagedList(pageIndex, pageSize));
            }
        }
        // GET: Historial Clinico
        [HttpGet]
        public ActionResult Ficha(int id)
        {
            int pageSize = 10;
            int pageIndex = 1;

            var paciente = db.paciente.Where(x => x.id_paciente == id).FirstOrDefault();
            var cli = db.cliente.Include(x => x.persona).Where(x => x.id_cliente == paciente.id_cliente).FirstOrDefault();
            var cliente = cli.persona.nro_documento + " " + cli.persona.nombre + " " + cli.persona.apellido;
            //Muestra en la cabecera
            ViewBag.paciente = paciente.nombre;
            ViewBag.cliente = cliente;
            //ViewBag.visita = paciente.ult_visita;

            //Datos de la ficha de paciente
            ViewBag.id_paciente = paciente.id_paciente;
            var fichas = db.ficha.Where(x => x.id_paciente == paciente.id_paciente).FirstOrDefault();
            ViewBag.id_ficha = (fichas != null ? fichas.id_ficha : 0);
            ViewBag.vacunas = new SelectList(db.vacuna.ToList(), "id_vacuna", "nombre");
            ViewBag.ddlProcedimiento = new SelectList(db.producto.Where(x => x.id_tipo_producto == 2).ToList(), "id_producto", "descripcion");
            ViewBag.id_medico = new SelectList(db.empleado.Include("persona").Where(x => x.persona.id_cat_persona == 2).ToList(), "id_empleado", "persona.nombre");
            ViewBag.id_vacuna = new SelectList(db.vacuna.ToList(), "id_vacuna", "nombre");
            ViewBag.id_patologia = new SelectList(db.patologia, "id_patologia", "nombre");
            return View();
        }

        [HttpPost]
        public JsonResult guardarNuevoCliente([Bind(Include = "nombre,apellido,id_tipo_documento,nro_documento")] persona cliente)
        {
            if (ModelState.IsValid)
            {
                var clientes = db.persona.Where(x => x.nro_documento == cliente.nro_documento).FirstOrDefault();
                if (clientes == null)
                {
                    //desmenuzo el objeto para darle formato de primera letra en mayuscula
                    cliente.nombre = Jagua.Clases.Comun.UppercaseFirst(cliente.nombre);
                    cliente.apellido = Jagua.Clases.Comun.UppercaseFirst(cliente.apellido);
                    cliente.id_cat_persona = 1;
                    cliente.estado = "A";
                    cliente.fecha_alta = DateTime.Now;
                    cliente.usuario_alta = Clases.SessionManager.SessionData.usuario.ToLower();

                    cliente datosCliente = new cliente();
                    datosCliente.id_persona = cliente.id_persona;
                    datosCliente.usuario_alta = Clases.SessionManager.SessionData.usuario;
                    datosCliente.fecha_alta = DateTime.Now;
                    datosCliente.estado = "A";

                    db.persona.Add(cliente);
                    db.cliente.Add(datosCliente);
                    db.SaveChanges();


                    var data = db.cliente.ToList().OrderByDescending(x => x.id_cliente).First().id_cliente;
                    return Json(new { data, JsonRequestBehavior.AllowGet });
                }
                else
                {
                    // retorno cero para leer en el lado del cliente que ya existe la cedula
                    var data = "0";
                    return Json(data);
                }
            }
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public class Servicios
        {
            public string idServicio { get; set; }
            public string descripcion { get; set; }
        }
    }
}
