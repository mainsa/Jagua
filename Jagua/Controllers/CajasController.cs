﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Jagua.Models;
using JaguaEntityModel;
using PagedList;
using static Jagua.Clases.Reportes;

namespace Jagua.Controllers
{
    public class CajasController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Cajas
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var cajas = db.caja.Include(o => o.usuarios);
            return View(cajas.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var cajas = db.caja.Include(o => o.usuarios);
            var user = fc["txtUsuario"]; // A traves del fc, obtengo el valor de la caja de texto
            var estado = fc["ddlEstado"];

            //filtro de usuario
            if (!string.IsNullOrEmpty(user)) // pregunto si no esta vacio la variable nombre
            {
                cajas = cajas.Where(q => q.descripcion.ToLower().Contains(user.ToLower()));//pregunta si el campo es igual al filtro
            }
            //filtro de estado
            if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable nombre
            {
                cajas = cajas.Where(q => q.estado == estado);//pregunta si el campo es igual al filtro
            }

            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtUsuario = user;
            ViewBag.ddlestado = estado;

            //retorno la vista filtrada
            return View(cajas.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }

        // GET: Cajas/Create
        public ActionResult Create()
        {
            ViewBag.id_usuario = new SelectList(db.usuarios.ToList(), "id_usuario", "usuario");
            return View();
        }

        // POST: Cajas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_caja,descripcion,id_usuario, estado")] caja caja)
        {
            if (ModelState.IsValid)
            {
                //Aplico Case Sensitive para descripcion de caja
                caja.descripcion = Jagua.Clases.Comun.UppercaseFirst(caja.descripcion);
                caja.fecha_alta = DateTime.Now;
                caja.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                db.caja.Add(caja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(caja);
        }

        // GET: Cajas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            caja caja = db.caja.Find(id);
            if (caja == null)
            {
                return HttpNotFound();
            }
			else
			{
				ViewBag.id_usuario = new SelectList(db.usuarios, "id_usuario", "usuario", caja.id_usuario);
				ViewBag.ddlEstado = caja.estado;

				return View(caja);
			}
            
        }

        // POST: Cajas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_caja,descripcion,id_usuario,estado,usuario_alta,fecha_alta")] caja caja)
        {
            if (ModelState.IsValid)
            {
                caja.descripcion = Jagua.Clases.Comun.UppercaseFirst(caja.descripcion);
                caja.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
                caja.fecha_mod = DateTime.Now;
                db.Entry(caja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(caja);
        }

        public JsonResult getCajas(int id_usuario)
        {
            var cajas = db.caja.Where(x => x.id_usuario == id_usuario).ToList();
            if (cajas.Count() > 0)
            {
                var data = "0";
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = "0";
                return Json(data, JsonRequestBehavior.AllowGet);

            }

        }

        public ActionResult AperturaCierre()
        {
            var id_usuario = Clases.SessionManager.SessionData.id_usuario;
            var cajas = db.caja.Where(x => x.id_usuario == id_usuario).ToList();
            if (cajas.Count() > 0)
            {
                List<Models.Aperturas> lista = new List<Models.Aperturas>();
                var query = from a in db.caja
                            join b in db.apertura_cierre on a.id_caja equals b.id_caja
                            into AllCajas
                            from x in AllCajas.DefaultIfEmpty()
                            where a.id_usuario == id_usuario
                            select new Aperturas
                            {
                                IdApertura = x.id_apertura_cierre,
                                IdCaja = a.id_caja,
                                Caja = a.descripcion,
                                FechaApertura = x.fecha_apertura,
                                FechaCierre = x.fecha_cierre,
                                UsuarioApertura = x.usuario_apertura,
                                UsuarioCierre = x.usuario_cierre
                            };
                lista = query.OrderByDescending(n => n.FechaApertura).ToList();
                return View(lista);
            }
            else
            {
                TempData["msg"] = "<script>swal({text: 'No tiene cajas disponibles, habilite una caja',type: 'warning'});</script>";
                return RedirectToAction("Create", "Cajas");

            }
        }

        [HttpPost]
        public JsonResult guardarAperturaCaja([Bind(Include = "id_caja,descripcion_caja,fecha_apertura,usuario_apertura,monto_inicial,observaciones")] apertura_cierre apertura)
        {
            if (ModelState.IsValid)
            {
                apertura_cierre obj = new apertura_cierre();
                obj.id_caja = apertura.id_caja;
                obj.descripcion_caja = apertura.descripcion_caja;
                obj.fecha_apertura = DateTime.Now;
                obj.usuario_apertura = apertura.usuario_apertura;
                obj.monto_inicial = apertura.monto_inicial;
                obj.observaciones = apertura.observaciones;
               
                db.apertura_cierre.Add(obj);
                    db.SaveChanges();

                    return Json(new { Status = "Success" });
            }
            else
            {
                return null;
            }
        }

        public JsonResult getApertura(int IdApertura)
        {
            if (IdApertura > 0)
            {
                var apertura = db.apertura_cierre.Where(x => x.id_apertura_cierre == IdApertura).FirstOrDefault();
                if (apertura != null)
                {
                    var data = from N in db.apertura_cierre
                               where N.id_apertura_cierre == IdApertura
                               select new {N.id_apertura_cierre, N.id_caja, N.descripcion_caja, N.fecha_apertura, N.fecha_cierre, N.usuario_apertura, N.monto_inicial };
                    
                        return Json(data, JsonRequestBehavior.AllowGet);
                }
                return null;

            }
            else
            {
                return null;
            }
        }

        public JsonResult getRecaudacion(int IdApertura)
        {
            if (IdApertura > 0)
            {
                var rec = db.Recaudacion_Caja(IdApertura).ToList();
                if (rec[0] != null)
                {
                    var data = from a in db.Recaudacion_Caja(IdApertura)
                               select new { a };
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var data = "0";
                    return Json(data, JsonRequestBehavior.AllowGet);

                }
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult guardarCierreCaja([Bind(Include = "id_apertura,id_caja,descripcion_caja,monto_caja,recaudacion,faltante")] arqueo cierre)
        {
            if (ModelState.IsValid)
            {
                arqueo obj = new arqueo();
                obj.id_apertura = cierre.id_apertura;
                obj.id_caja = cierre.id_caja;
                obj.descripcion_caja = cierre.descripcion_caja;
                obj.monto_caja = cierre.monto_caja;
                obj.recaudacion = cierre.recaudacion;
                obj.faltante = cierre.faltante;
                obj.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                obj.fecha_alta = DateTime.Now;

                db.arqueo.Add(obj);
                db.SaveChanges();

                return Json(new { Status = "Success" });
            }
            else
            {
                return null;
            }
        }

        // GET: Cajas
        public ActionResult Arqueo(int? page)
        {
            //Jagua.Datasets.DTables.ArticulosDataTable.Ar
            //        //        Web.Gambling.Reportes.DtsReportes.DtsInfVendedores dts = new Reportes.DtsReportes.DtsInfVendedores();

            //        var dt = new Jagua.Datasets.DTables.ArticulosDataTable
            ////            //Web.Gambling.Reportes.DtsReportes.DtsInfVendedores.DtInfVendedoresDataTable();
            //        var row = dt.NewDtInfVendedoresRow();ombre = d.usuario;

            ////Carga de los datatables en el dataset
            //dts.Tables["Articulos"].Merge(dt);
            Jagua.Clases.Comun.ReportesParametros Pmt = new Jagua.Clases.Comun.ReportesParametros();
            Pmt.ReportPath = Server.MapPath("~/Reportes/ListadoArticulos.rpt");
            Pmt.ReportSource = "";//(object)dts;
            Pmt.NombreArchivo = "Informe de Arqueo";
            Jagua.Clases.Comun.ReportParameters.DatosReporte = Pmt;
            return View();
        }

        // GET: Cajas
        public ActionResult ArqueosCaja(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            ViewBag.ddlCaja = new SelectList(db.caja.ToList(), "id_caja", "descripcion");
            ViewBag.ddlUsuario = new SelectList(db.usuarios.ToList(), "id_usuario", "usuario");

            List<ArqueosCaja> lista = new List<ArqueosCaja>();
            var query = from a in db.apertura_cierre
                        join b in db.arqueo on a.id_apertura_cierre equals b.id_apertura
                        select new ArqueosCaja
                        {
                            id_arqueo = b.id_arqueo,
                            id_caja = a.id_caja,
                            descripcion_caja = a.descripcion_caja,
                            fecha_apertura = a.fecha_apertura,
                            fecha_cierre = a.fecha_cierre,
                            usuario_apertura = a.usuario_apertura,
                            usuario_cierre = a.usuario_cierre,
                            monto_inicial = a.monto_inicial,
                            observaciones = a.observaciones,
                            monto_caja = b.monto_caja,
                            recaudacion = b.recaudacion,
                            faltante = b.faltante
                        };
            lista = query.OrderByDescending(n => n.fecha_apertura).ToList();

            return View(lista.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult ArqueosCaja(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var cajas = db.caja.Include(o => o.usuarios);
            var user = fc["txtUsuario"]; // A traves del fc, obtengo el valor de la caja de texto
            var estado = fc["ddlEstado"];

            //filtro de usuario
            if (!string.IsNullOrEmpty(user)) // pregunto si no esta vacio la variable nombre
            {
                cajas = cajas.Where(q => q.descripcion.ToLower().Contains(user.ToLower()));//pregunta si el campo es igual al filtro
            }
            //filtro de estado
            if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable nombre
            {
                cajas = cajas.Where(q => q.estado == estado);//pregunta si el campo es igual al filtro
            }

            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtUsuario = user;
            ViewBag.ddlestado = estado;

            //retorno la vista filtrada
            return View(cajas.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }


        // GET: Cajas
        public ActionResult ArqueosCajaImprimir(int? page)
        {
            ArqueosCaja dts = new ArqueosCaja();
            //Jagua.Datasets.DTables.ArticulosDataTable.Ar
            //        //        Web.Gambling.Reportes.DtsReportes.DtsInfVendedores dts = new Reportes.DtsReportes.DtsInfVendedores();

            //        var dt = new Jagua.Datasets.DTables.ArticulosDataTable
            ////            //Web.Gambling.Reportes.DtsReportes.DtsInfVendedores.DtInfVendedoresDataTable();
            //        var row = dt.NewDtInfVendedoresRow();ombre = d.usuario;

            ////Carga de los datatables en el dataset
            //dts.Tables["Articulos"].Merge(dt);
            Jagua.Clases.Comun.ReportesParametros Pmt = new Jagua.Clases.Comun.ReportesParametros();
            Pmt.ReportPath = Server.MapPath("~/Reportes/ArqueoCaja.rpt");
            Pmt.ReportSource = "";//(object)dts;
            Pmt.NombreArchivo = "Informe de Arqueo";
            Jagua.Clases.Comun.ReportParameters.DatosReporte = Pmt;
            return View();
        }


        // GET: Cajas
        public ActionResult ReportesCaja(int? page)
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            caja caja = db.caja.Find(id);
            caja.estado = "I";
			caja.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            caja.fecha_mod = DateTime.Now;
			db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
