﻿using Jagua.Clases;
using JaguaEntityModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;




namespace Jagua.Controllers
{
    public class LoginController : Controller
    {
        private JaguaEntities db = new JaguaEntities();
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View("Index", new usuarios());
        }
        // GET: Login/Create
        // POST: Login/Create
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Autenticar(usuarios usuarios, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var usuario = login(usuarios.usuario, usuarios.password);//Obtenemos el usuario 
                    if (usuario != null)
                    {
                        if (usuario.estado != "I")
                        {
                            //Guardamos los datos del usuario el la Session
                            Session["Usuario_" + Clases.SessionManager.getRemoteAddresApp] = usuario;
                            string req = Request.Url.Authority;
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ModelState.AddModelError("", "No se pudo acceder, usuario inactivo, comuníquese con el administrador.");
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "No se pudo acceder, verifique sus datos.");
                    }
                }

                return View("Index", usuarios);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error en el servicio de autenticación.");
                Clases.Logger.Log.Debug(ex.Message);
                return View("Index", usuarios);
            }
        }
        /// <summary>
        /// login
        /// </summary>
        /// <param name="user"></param>
        /// <param name="pass"></param>
        /// <returns></returns>
        private usuarios login(string user, string pass)
        {

            string usrpassword = Clases.Comun.MD5(pass);//Encriptamos el password
            usuarios datos = db.usuarios.Where(x => x.usuario == user && x.password == usrpassword).FirstOrDefault();
            return datos;

        }
        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            SalirSistema();
            return RedirectToAction("Index", "Login");
        }

        /// <summary>
        /// SalirSistema
        /// </summary>
        private void SalirSistema()
        {
            try
            {
                Session.Remove("Usuario_" + Clases.SessionManager.getRemoteAddresApp);

            }
            catch (Exception ex)
            {

                //throw;
            }

        }
        [AllowAnonymous]
        public ActionResult RecuperarPassword()
        {
            ViewBag.recuperar = "recuperar";
            return View(new usuarios());
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult RecuperarPassword(usuarios usuarios)
        {
            string passnuevo = GenerateToken(8);           
            try
            {
                usuarios recuperar = db.usuarios.Where(x => x.usuario == usuarios.usuario).FirstOrDefault();//Recuperamos el Usuario
                if (recuperar != null)
                {
                    var estado = recuperar.estado;
                    if (estado == "A")
                    {                       
                        string email = recuperar.email.ToString();
                        if (email != string.Empty)
                        {
                            if (Enviar_Correo(recuperar.usuario, passnuevo, email))//Enviamos el Correo al Usuario
                            {
                                recuperar.password = Clases.Comun.MD5(passnuevo);
                                recuperar.estado = "A";
                                db.Entry(recuperar).State = EntityState.Modified;
                                db.SaveChanges();
                                //En el Log Podemos verificar el passnuevo
                                Logger.Log.InfoFormat("Recuperar contraseña - Se envio un correo a:{0}, NuevoPassword: {1}" + usuarios.usuario , passnuevo);
                            }
                        }
                    }
                    ViewBag.recuperar = "recuperado";
                    return View("RecuperarPassword", usuarios);

                }
                else
                {
                    ViewBag.recuperar = "recuperar";
                    ModelState.AddModelError("", "Su usuario esta inactivo o eliminado, por favor contacte con un administrador");
                    return View("RecuperarPassword", usuarios);
                }

            }
            catch (Exception ex)
            {
                ViewBag.recuperar = "recuperar";
                ModelState.AddModelError("", "Error en el sistema de recuperación, consulte con el adminsitrador");
                return View("RecuperarPassword", usuarios);

            }
        }
        /// <summary>
        /// Enviar_Correo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <param name="correoPara"></param>
        /// <returns></returns>
        private bool Enviar_Correo(string usuario, string password, string correoPara)
        {
            bool retorno = false;

            string mensaje = "<p>Se a solicitado recuperación de contraseña, sus credenciales temporales son: </p>";

            mensaje += " Usuario: " + usuario + "<br />";
            mensaje += " Contraseña: " + password + "<br />";
            mensaje += "<p>Ahora ya puede acceder al sistema, le recomendamos cambiar su contraseña.</p>";
            //LOGS DE DATOS ENVIADOS
            Logger.Log.InfoFormat("Datos Email: {0}", mensaje);
            retorno = Comun.EnviarCorreo(mensaje, usuario, password, correoPara);
            return retorno;

        }
        /// <summary>
        /// GenerateToken
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public string GenerateToken(int length)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
            byte[] tokenBuffer = new byte[length];
            cryptRNG.GetBytes(tokenBuffer);
            return Convert.ToBase64String(tokenBuffer);
        }


        [AllowAnonymous]
        public ActionResult ModificarPassword()
        {
            ViewBag.usuario = Jagua.Clases.SessionManager.SessionData.usuario;
            ViewBag.modificar = false;
            ViewBag.recuperar = "modificar";
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult ModificarPassword(usuarios usuarios, FormCollection form)
        {
            try
            {
                ViewBag.modificar = false;
                if (ModelState.IsValid)
                {
                    var cambiar = login(Jagua.Clases.SessionManager.SessionData.usuario, usuarios.password);
                    if (cambiar != null && !string.IsNullOrEmpty(form["nuevopassword"]) && !string.IsNullOrEmpty(form["repetirpassword"]))
                    {
                        string passnuevo = form["nuevopassword"].Trim();
                        string repetirpassword = form["repetirpassword"].Trim();
                        if (passnuevo.Equals(repetirpassword))
                        {
                            if (cambiar.estado != "I")
                            {
                                passnuevo = Comun.MD5(passnuevo);
                                if (!cambiar.password.Equals(passnuevo))
                                {
                                    cambiar.password = passnuevo;
                                    cambiar.estado = "A";
                                    db.Entry(cambiar).State = EntityState.Modified;
                                    db.SaveChanges();
                                    Session["Usuario_" + SessionManager.getRemoteAddresApp] = cambiar;
                                    string req = Request.Url.Authority;
                                    ViewBag.modificar = true;
                                    TempData["msg"] = "<script>swal({text:'Contraseña modificada correctamente',type:'successs'});</script>";
                                    return RedirectToAction("Index", "Home");
                                }
                                else
                                {
                                    ViewBag.modificar = false;
                                    ModelState.AddModelError("", "La contraseña debe ser diferente a la actual, comuníquese con el administrador.");
                                }
                            }
                            else
                            {
                                ViewBag.modificar = false;
                                ModelState.AddModelError("", "No se pudo acceder, usuario inactivo, comuníquese con el administrador.");
                            }
                        }
                        else
                        {
                            ViewBag.modificar = false;
                            ModelState.AddModelError("", "La contraseña nueva no coincide!");
                        }

                    }
                    else
                    {
                        ViewBag.modificar = false;
                        ModelState.AddModelError("", "No se pudo acceder, verifique sus datos.");
                    }
                }

                return View(usuarios);
            }
            catch (Exception ex)
            {
                ViewBag.modificar = false;
                ModelState.AddModelError("", "Error en el servicio de autenticación.");
                Clases.Logger.Log.ErrorFormat("Error en el servicio de autenticación: {0}", ex.Message);
                return View(usuarios);
            }

        }
    }
}
