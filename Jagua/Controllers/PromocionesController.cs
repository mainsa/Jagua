﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    //[ControlFilter]
    public class PromocionesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: promocions
        public ActionResult Index(int? page, string currentFilter)
        {
            int pageSize = 10;
            int pageIndex = 1;
            var promocion = db.promocion.Where(t => t.estado.Equals("A")).Include(p => p.producto).OrderByDescending(n => n.id_promocion).ToList();
            ViewBag.CurrentFilter = currentFilter;
            if (!string.IsNullOrEmpty(currentFilter))
            {
                var ArrcurrentFilter = currentFilter.Split('_');
                if (ArrcurrentFilter.Length > 0)
                {
                    var nombre = ArrcurrentFilter[0];
                    var estado = ArrcurrentFilter[1];
                    var desde = ArrcurrentFilter[2];
                    var hasta = ArrcurrentFilter[3];
                    if (!string.IsNullOrEmpty(nombre))
                    {
                        promocion = promocion.Where(q => q.descripcion.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable estado
                    {
                        promocion = promocion.Where(t => t.estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
                    }
                    if (!string.IsNullOrEmpty(desde) && !string.IsNullOrEmpty(hasta))
                    {
                        DateTime fdesde = DateTime.Parse(desde + " 00:00:00");
                        DateTime fhasta = DateTime.Parse(hasta + " 23:59:59");
                        promocion = promocion.Where(t => t.fecha_desde >= fdesde && t.fecha_hasta <= fhasta).ToList();
                    }
                    ViewBag.searchString = nombre;
                    ViewBag.estado = estado;
                    ViewBag.desde = desde;
                    ViewBag.hasta = hasta;
                }
            }
            return View(promocion.ToPagedList(pageIndex, pageSize));
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            ViewBag.estado = string.Empty;
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            var promocion = db.promocion.Include(p => p.producto).OrderByDescending(n => n.id_promocion).ToList();
            string nombre = fc["searchString"]; // A traves del fc, obtengo el valor de la caja de texto
            string estado = string.IsNullOrEmpty(fc["searchEstado"]) ? "" : fc["searchEstado"];
            string desde = string.IsNullOrEmpty(fc["desde"]) ? "" : fc["desde"];
            string hasta = string.IsNullOrEmpty(fc["hasta"]) ? "" : fc["hasta"];
            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                promocion = promocion.Where(q => q.descripcion.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(estado)) // pregunto si no esta vacio la variable estado
            {
                promocion = promocion.Where(t => t.estado.Equals(estado)).ToList();//pregunta si el campo es igual al filtro
            }
            if (!string.IsNullOrEmpty(desde) && !string.IsNullOrEmpty(hasta))
            {
                DateTime fdesde = DateTime.Parse(desde + " 00:00:00");
                DateTime fhasta = DateTime.Parse(hasta + " 23:59:59");
                promocion = promocion.Where(t => t.fecha_desde >= fdesde && t.fecha_hasta <= fhasta).ToList();
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.searchString = nombre;
            ViewBag.estado = estado;
            ViewBag.desde = desde;
            ViewBag.hasta = hasta;

            ViewBag.CurrentFilter = nombre + "_" + estado + "_" + desde + "_" + hasta;
            //retorno la vista filtrada
            return View(promocion.ToPagedList(pageIndex, pageSize));

        }

        // GET: promocions/Details/5
        public async Task<ActionResult> PartialShow(int id)
        {
            promocion promocion = await db.promocion.FindAsync(id);
            if (promocion == null)
            {
                return HttpNotFound();
            }
            ViewBag.puntos = Convert.ToInt32(Math.Floor(promocion.puntos));
            if (promocion.fecha_desde != null)
                ViewBag.Desde = promocion.fecha_desde.Value.ToString("yyyy-MM-dd");
            if (promocion.fecha_hasta != null)
                ViewBag.Hasta = promocion.fecha_hasta.Value.ToString("yyyy-MM-dd");
            ViewBag.estado = promocion.estado;
            ViewBag.id_producto = new SelectList(db.producto, "id_producto", "descripcion", promocion.id_producto);
            return PartialView("Details", promocion);
        }

        // GET: promocions/Create
        public ActionResult Create()
        {
            ViewBag.estado = "A";
            ViewBag.id_producto = new SelectList(db.producto, "id_producto", "descripcion");
            return View();
        }

        // POST: promocions/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_promocion,id_producto,descripcion,puntos,fecha_desde,fecha_hasta,estado,usuario_alta,fecha_alta,usuario_mod,fecha_mod")] promocion promocion)
        {
            if (ModelState.IsValid)
            {
                promocion.estado = "A";
                promocion.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                promocion.fecha_alta = DateTime.Now;
                db.promocion.Add(promocion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_producto = new SelectList(db.producto, "id_producto", "descripcion", promocion.id_producto);
            return View(promocion);
        }

        // GET: promocions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            promocion promocion = db.promocion.Find(id);
            if (promocion == null)
            {
                return HttpNotFound();
            }
            ViewBag.puntos = Convert.ToInt32(Math.Floor(promocion.puntos));
            if (promocion.fecha_desde != null)
                ViewBag.Desde = promocion.fecha_desde.Value.ToString("yyyy-MM-dd");
            if (promocion.fecha_hasta != null)
                ViewBag.Hasta = promocion.fecha_hasta.Value.ToString("yyyy-MM-dd");
            ViewBag.estado = promocion.estado;
            ViewBag.id_producto = new SelectList(db.producto, "id_producto", "descripcion", promocion.id_producto);
            return View(promocion);
        }

        // POST: promocions/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_promocion,id_producto,descripcion,puntos,fecha_desde,fecha_hasta,estado,usuario_alta,fecha_alta,usuario_mod,fecha_mod")] promocion promocion)
        {
            if (ModelState.IsValid)
            {

                using (var context = new JaguaEntities())
                {
                    try
                    {
                        promocion promocionmod = context.promocion.Find(promocion.id_promocion);
                        if (promocion == null)
                        {
                            return HttpNotFound();
                        }
                        promocionmod.descripcion = promocion.descripcion;
                        promocionmod.id_producto = promocion.id_producto;
                        promocionmod.puntos = promocion.puntos;
                        promocionmod.fecha_desde = promocion.fecha_desde;
                        promocionmod.fecha_hasta = promocion.fecha_hasta;
                        promocionmod.estado = promocion.estado;
                        promocionmod.usuario_mod = SessionManager.SessionData.id_usuario.ToString();
                        promocionmod.fecha_mod = DateTime.Now;
                        context.promocion.Attach(promocionmod);
                        context.Entry(promocionmod).State = EntityState.Modified;
                        context.SaveChanges();

                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        Logger.Log.ErrorFormat("ERROR al Editar: {0}, {1}", ex.Message, ex.InnerException);
                    }
                }
            }
            ViewBag.Desde = promocion.fecha_desde.Value.ToString("yyyy-MM-dd");
            ViewBag.Hasta = promocion.fecha_hasta.Value.ToString("yyyy-MM-dd");
            ViewBag.estado = promocion.estado;
            ViewBag.puntos = Convert.ToInt32(Math.Floor(promocion.puntos));
            ViewBag.id_producto = new SelectList(db.producto, "id_producto", "descripcion", promocion.id_producto);
            return View(promocion);
        }

        // GET: promocions/Delete/5
        public ActionResult Delete(int? id)
        {
            bool success = true;
            string mensaje = "Registro Eliminado con Exito!";
            try
            {
                if (id == null)
                {
                    success = false;
                    mensaje = "No se pudo eliminar el registro";
                    return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
                }
                promocion promocion = db.promocion.Find(id);
                if (promocion != null)
                {
                    //db.promocion.Remove(promocion);
                    promocion.estado = "I";
                    db.Entry(promocion).State = EntityState.Modified;
                    db.SaveChanges();
                    success = true;
                    mensaje = "Registro Eliminado con Exito!";

                }
            }
            catch (Exception ex)
            {
                success = false;
                mensaje = "No se pudo eliminar el registro";
                Logger.Log.ErrorFormat("ERROR al Eliminar el registro: {0}, {1}", ex.Message, ex.InnerException);
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        //// POST: promocions/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    promocion promocion = db.promocion.Find(id);
        //    db.promocion.Remove(promocion);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
