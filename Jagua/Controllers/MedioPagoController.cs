﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class MedioPagoController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: MedioPago
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            return View(db.medio_pago.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }
        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var mp = db.medio_pago.ToList();
            var nombre = fc["txtNombre"].ToLower(); // A traves del fc, obtengo el valor de la caja de texto

            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                mp = mp.Where(q => q.descripcion.ToLower().Contains(nombre)).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.txtNombre = fc["txtNombre"];

            //retorno la vista filtrada
            return View(mp.OrderByDescending(n => n.descripcion).ToPagedList(pageIndex, pageSize));

        }


        // GET: MedioPago/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MedioPago/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_medio_pago,descripcion")] medio_pago medio_pago)
        {
            if (ModelState.IsValid)
            {
                medio_pago.descripcion= Jagua.Clases.Comun.UppercaseFirst(medio_pago.descripcion);
                medio_pago.fecha_alta = DateTime.Now;
                medio_pago.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                medio_pago.estado = "A";
                db.medio_pago.Add(medio_pago);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medio_pago);
        }

        // GET: MedioPago/Edit/5
        public ActionResult Editar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            medio_pago medio_pago = db.medio_pago.Find(id);
            if (medio_pago == null)
            {
                return HttpNotFound();
            }
            return View(medio_pago);
        }

        // POST: MedioPago/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar([Bind(Include = "id_medio_pago,descripcion,estado,usuario_alta,fecha_alta")] medio_pago medio_pago)
        {
            if (ModelState.IsValid)
            {
                medio_pago.descripcion = Jagua.Clases.Comun.UppercaseFirst(medio_pago.descripcion);
                medio_pago.fecha_mod = DateTime.Now;
                medio_pago.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
                db.Entry(medio_pago).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(medio_pago);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            medio_pago medio_pago = db.medio_pago.Find(id);
            medio_pago.estado = "I";
            medio_pago.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            medio_pago.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
