﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class LaboratoriosController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Laboratorios
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            return View(db.laboratorio.ToList().OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta

            var laboratorios = db.laboratorio.ToList();
            var nombre = fc["txtDescripcion"];

            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                laboratorios = laboratorios.Where(q => q.nombre.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            ViewBag.txtDescripcion = nombre;
            return View(laboratorios.OrderBy(n => n.descripcion).ToPagedList(pageIndex, pageSize));
        }


        // GET: Laboratorios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Laboratorios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_laboratorio,nombre,descripcion,estado")] laboratorio laboratorio)
        {
            if (ModelState.IsValid)
            {
                laboratorio.nombre = Jagua.Clases.Comun.UppercaseFirst(laboratorio.nombre);
                laboratorio.descripcion = Jagua.Clases.Comun.UppercaseFirst(laboratorio.descripcion);
                laboratorio.usuario_alta = Jagua.Clases.SessionManager.SessionData.usuario;
                laboratorio.fecha_alta = DateTime.Now;
                laboratorio.estado = "A";
                db.laboratorio.Add(laboratorio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(laboratorio);
        }

        // GET: Laboratorios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            laboratorio laboratorio = db.laboratorio.Find(id);
            if (laboratorio == null)
            {
                return HttpNotFound();
            }
            else
            {
                ViewBag.ddlEstado = laboratorio.estado;
            }
            return View(laboratorio);
        }

        // POST: Laboratorios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_laboratorio,nombre,descripcion,estado")] laboratorio laboratorio)
        {
            if (ModelState.IsValid)
            {
                laboratorio.nombre = Jagua.Clases.Comun.UppercaseFirst(laboratorio.nombre);
                laboratorio.descripcion = Jagua.Clases.Comun.UppercaseFirst(laboratorio.descripcion);
                laboratorio.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
                laboratorio.fecha_mod = DateTime.Now;
                db.Entry(laboratorio).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(laboratorio);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableConfirmed(int id)
        {
            laboratorio laboratorio= db.laboratorio.Find(id);
            laboratorio.estado = "I";
            laboratorio.usuario_mod = Jagua.Clases.SessionManager.SessionData.usuario;
            laboratorio.fecha_mod = DateTime.Now;
            db.SaveChanges();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
