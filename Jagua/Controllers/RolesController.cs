﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Jagua.Clases;
using JaguaEntityModel;
using PagedList;

namespace Jagua.Controllers
{
    public class RolesController : Controller
    {
        private JaguaEntities db = new JaguaEntities();

        // GET: Roles
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1; //operador ternario de consulta
            var datos = db.rol.OrderBy(n => n.nombre).ToList();
            return View(datos.ToPagedList(pageIndex, pageSize));
        }

        [HttpPost]
        public ActionResult Index(FormCollection fc, int? page)
        {
            int pageSize = 10;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            var datos = db.rol.OrderBy(n => n.nombre).ToList();
            var nombre = fc["searchString"]; // A traves del fc, obtengo el valor de la caja de texto


            if (!string.IsNullOrEmpty(nombre)) // pregunto si no esta vacio la variable nombre
            {
                datos = datos.Where(q => q.nombre.ToLower().Contains(nombre.ToLower())).ToList();//pregunta si el campo es igual al filtro
            }
            //aca le asigno a la cajita la variable que contiene la busqueda, para que aparezca al retornar
            ViewBag.searchString = nombre;

            //retorno la vista filtrada
            return View(datos.OrderBy(n => n.nombre).ToPagedList(pageIndex, pageSize));

        }


        // GET: Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_rol,nombre,descripcion")] rol rol)
        {
            if (ModelState.IsValid)
            {
                db.rol.Add(rol);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rol);
        }

        // GET: Roles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rol rol = db.rol.Find(id);
            if (rol == null)
            {
                return HttpNotFound();
            }
            return View(rol);
        }

        // POST: Roles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_rol,nombre,descripcion")] rol rol)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rol).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rol);
        }

        // GET: Roles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            rol rol = db.rol.Find(id);
            if (rol == null)
            {
                return HttpNotFound();
            }
            return View(rol);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            using (var context = new JaguaEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        IList<rol_pagina> rolPagina = (from rp in context.rol_pagina
                                                       where rp.id_rol == id
                                                       select rp).ToList();
                        if (rolPagina.Any())
                        {
                            context.rol_pagina.RemoveRange(rolPagina);
                            context.SaveChanges();
                        }
                        rol rol = context.rol.Find(id);
                        if (rol != null)
                        {
                            context.rol.Remove(rol);
                            context.SaveChanges();
                        }

                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {

                        dbContextTransaction.Rollback();
                        Clases.Logger.Log.ErrorFormat("ERROR DeleteConfirmed RolesController: {0}, {1}, {2}", ex.Message, ex.InnerException, ex.StackTrace);

                    }
                }
            }


            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public async Task<ActionResult> AgregarPermisos(int id)
        {
            rol rol_permiso = await db.rol.FindAsync(id);
            int idRol = 0;
            if (rol_permiso != null)
            {
                idRol = rol_permiso.id_rol;
            }
            var permisos = await db.paginas.ToListAsync();
            var Listado = new List<DatosPermisos>();
            rol_pagina valaccion = new rol_pagina();
            foreach (var item in permisos)
            {
                valaccion = ObtenerPermisos(item.id_pagina, idRol);
                Listado.Add(new DatosPermisos
                {
                    id_pagina = item.id_pagina,
                    nombre = item.pagina,
                    insert = (valaccion != null ? valaccion.insertar.Value : false),
                    update = (valaccion != null ? valaccion.modificar.Value : false),
                    delete = (valaccion != null ? valaccion.eliminar.Value : false),
                    ejecutar = (valaccion != null ? valaccion.ejecutar.Value : false)
                });


            }
            ViewBag.id_rol = new SelectList(db.rol, "id_rol", "nombre", idRol);
            ViewBag.Listado = Listado;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> AgregarPermisos(int id, int idPermiso, int idRol, string accion, string insertar,
            string actualizar, string eliminar, string ejecutar)
        {
            bool success = true;
            string mensaje = string.Empty;
            try
            {
                var rolpermiso = await db.rol_pagina.Where(t => t.id_pagina == idPermiso && t.id_rol == idRol).ToListAsync();
                if (!rolpermiso.Any())
                {
                    rol_pagina rolpagina = new rol_pagina();
                    rolpagina.id_rol = idRol;
                    rolpagina.id_pagina = idPermiso;
                    rolpagina.insertar = (insertar == "S" ? true : false);
                    rolpagina.modificar = (actualizar == "S" ? true : false);
                    rolpagina.eliminar = (eliminar == "S" ? true : false);
                    rolpagina.ejecutar = (ejecutar == "S" ? true : false);
                    db.rol_pagina.Add(rolpagina);
                    await db.SaveChangesAsync();

                }
                if (rolpermiso.Any())
                {
                    foreach (var item in rolpermiso)
                    {
                        item.insertar = (insertar == "S" ? true : false);
                        item.modificar = (actualizar == "S" ? true : false);
                        item.eliminar = (eliminar == "S" ? true : false);
                        item.ejecutar = (ejecutar == "S" ? true : false);
                        db.Entry(item).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                    }
                }
                mensaje = "Permisos Agregado con exito!";

            }
            catch (Exception ex)
            {
                Logger.Log.Error("Error AgregarPermisos: " + ex.InnerException);
                success = false;
                mensaje = "Error al tratar de Agregar Permisos";
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> AllAgregarPermisos(int id, int idPermiso, int idRol, string insertar,
           string actualizar, string eliminar, string ejecutar)
        {
            bool success = true;
            string mensaje = string.Empty;
            try
            {
                var rolpermiso = await db.rol_pagina.Where(t => t.id_pagina == idPermiso && t.id_rol == idRol).ToListAsync();
                if (!rolpermiso.Any())
                {
                    rol_pagina rolpagina = new rol_pagina();
                    rolpagina.id_rol = idRol;
                    rolpagina.id_pagina = idPermiso;
                    rolpagina.insertar = (insertar == "S" ? true : false);
                    rolpagina.modificar = (actualizar == "S" ? true : false);
                    rolpagina.eliminar = (eliminar == "S" ? true : false);
                    rolpagina.ejecutar = (ejecutar == "S" ? true : false);
                    db.rol_pagina.Add(rolpagina);
                    await db.SaveChangesAsync();

                }
                if (rolpermiso.Any())
                {
                    foreach (var item in rolpermiso)
                    {
                        item.insertar = (insertar == "S" ? true : false);
                        item.modificar = (actualizar == "S" ? true : false);
                        item.eliminar = (eliminar == "S" ? true : false);
                        item.ejecutar = (ejecutar == "S" ? true : false);
                        db.Entry(item).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                    }
                }
                mensaje = "Permisos Agregado con exito!";
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Error AgregarPermisos: " + ex.InnerException);
                success = false;
                mensaje = "Error al tratar de Agregar Permisos";
            }
            return Json(new { success, mensaje }, JsonRequestBehavior.AllowGet);
        }

        public rol_pagina ObtenerPermisos(int IdPagina, int idRol)
        {
            return db.rol_pagina.Where(t => t.id_pagina == IdPagina && t.id_rol == idRol).FirstOrDefault();
        }

        public async Task<ActionResult> Details(int id)
        {
            rol rol = await db.rol.FindAsync(id);
            if (rol == null)
            {
                return HttpNotFound();
            }
            return PartialView("Details", rol);
        }
    }
    public class DatosPermisos
    {
        public int id_pagina { get; set; }
        public string nombre { get; set; }
        public bool insert { get; set; }
        public bool update { get; set; }
        public bool delete { get; set; }
        public bool ejecutar { get; set; }
    }
}
