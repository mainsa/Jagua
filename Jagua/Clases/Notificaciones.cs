﻿
using JaguaEntityModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace Jagua.Clases
{
    public class Notificaciones
    {
        public static bool EnviarNotificaciones()
        {
            bool retorno = false;
            JaguaEntities db = new JaguaEntities();
            try
            {
                DateTime fechaNotifDesde = DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") + " 00:00:00");
                DateTime fechaNotifHasta = DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") + " 23:59:59");
                List<cita> citas = db.cita.Where(t => (t.fecha_cita.Value >= fechaNotifDesde && t.fecha_cita.Value <= fechaNotifHasta) && t.id_estado == 2).ToList();
                cita objcitas;
                cliente personas;
                var clientes = db.cliente.Where(t => t.persona.email != null).ToList();
                foreach (var item in citas)
                {
                    personas = clientes.Where(t => t.id_cliente.Equals(item.id_cliente)).FirstOrDefault();
                    if (personas != null)
                    {
                        if (Enviar_Correo(item.cliente.persona.nombre, item.paciente.nombre, item.hora.ToString(), item.fecha_cita.Value.ToString("dd/MM/yyyy"), personas.persona.email))
                        {
                            objcitas = item;
                            objcitas.id_estado = 7;
                            db.Entry(objcitas).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                retorno = true;
            }
            catch (Exception ex)
            {
                retorno = false;
                Logger.Log.ErrorFormat("ERROR EnviarNotificaciones(): {0}, {1}", ex.Message, ex.InnerException);
            }
            finally
            {
                db.Dispose();
            }
            return retorno;
        }
        public static bool NotificacionesProximaConsulta()
        {
            bool retorno = false;
            JaguaEntities dbpc = new JaguaEntities();
            try
            {
                DateTime fechaNotifDesde = DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") + " 00:00:00");
                DateTime fechaNotifHasta = DateTime.Parse(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") + " 23:59:59");
                List<paciente_consulta> consulta = dbpc.paciente_consulta.Where(t => (t.prox_consulta.Value >= fechaNotifDesde && t.prox_consulta.Value <= fechaNotifHasta && t.estado != "E")).ToList();
                paciente_consulta objconsulta;
                foreach (var item in consulta)
                {
                    if (Enviar_Correo(item.paciente.cliente.persona.nombre, item.paciente.nombre, "Llamar para Agendar", item.prox_consulta.Value.ToString("dd/MM/yyyy"), item.paciente.cliente.persona.email))
                    {
                        objconsulta = item;
                        objconsulta.estado = "E";
                        dbpc.Entry(objconsulta).State = EntityState.Modified;
                        dbpc.SaveChanges();
                    }
                }
                retorno = true;
            }
            catch (Exception ex)
            {
                retorno = false;
                Logger.Log.ErrorFormat("ERROR NotificacionesProximaConsulta(): {0}, {1}", ex.Message, ex.InnerException);
            }
            finally
            {
                dbpc.Dispose();
            }
            return retorno;
        }
        private static bool Enviar_Correo(string usuario, string paciente, string hora, string fechaCita, string correoPara)
        {
            bool retorno = false;

            string mensaje = "<p>Usted tiene una cita programada</p>";
            mensaje += " Cliente : " + usuario + "<br />";
            mensaje += " Paciente : " + paciente + "<br />";
            mensaje += " Fecha Cita : " + fechaCita + "<br />";
            mensaje += " Hora Cita : " + hora + "<br />";
            string asunto = "Citas Agendadas";
            //LOGS DE DATOS ENVIADOS
            Logger.Log.InfoFormat("Datos Email: {0}", mensaje);
            retorno = EnviarCorreo(mensaje, usuario, "", correoPara, asunto);
            return retorno;

        }
        private static bool EnviarPromociones()
        {
            bool retorno = false;
            JaguaEntities dbpromo = new JaguaEntities();
            try
            {
                string AsuntoPromocion = "";
                string mensaje = "";
                DateTime fechaNotifDesde = DateTime.Parse(DateTime.Now.AddDays(5).ToString("dd/MM/yyyy") + " 00:00:00");
                DateTime fechaNotifHasta = DateTime.Parse(DateTime.Now.AddDays(5).ToString("dd/MM/yyyy") + " 23:59:59");
                List<promocion> promos = dbpromo.promocion.Where(t => (t.fecha_desde.Value >= fechaNotifDesde && t.fecha_desde.Value <= fechaNotifHasta && t.estado != "E" && t.estado != "I")).ToList();
                promocion objconsulta;
                AsuntoPromocion = "Día de Promociones en Jagua";
                List<persona> clientes = (from c in dbpromo.cliente
                                          join p in dbpromo.persona on c.id_persona equals p.id_persona
                                          where c.estado == "A"
                                          where p.estado == "A"
                                          where p.email != null
                                          where p.email != ""
                                          select p).ToList();

                List<string> direcciones = new List<string>();
                foreach (var itemc in clientes)
                {
                    direcciones.Add(itemc.email);
                }
                mensaje = "<p>Día de Promociones en Jagua no te lo pierdas</p>";
                mensaje += "Estamos en promoción con los siguientes productos: <br />";
                foreach (var item in promos)
                {
                    mensaje += item.producto.descripcion + "<br />";
                    mensaje += "Usted podra acceder a los descuentos con los sigueintes puntos : " + item.puntos + "<br />";
                    retorno = EnviarCorreo(mensaje, "", "", AsuntoPromocion, direcciones);
                    if (retorno)
                    {
                        objconsulta = item;
                        objconsulta.estado = "E";
                        dbpromo.Entry(objconsulta).State = EntityState.Modified;
                        dbpromo.SaveChanges();
                    }
                }

                retorno = true;
            }
            catch (Exception ex)
            {
                retorno = false;
                Logger.Log.ErrorFormat("ERROR EnviarPromociones(): {0}, {1}", ex.Message, ex.InnerException);
            }
            finally
            {
                dbpromo.Dispose();
            }

            return retorno;

        }
        private static bool EnviarCorreo(string mensaje, string usuario, string password, string correoPara, string asunto)
        {
            bool retorno = false;

            try
            {

                string smtp1 = string.Empty;
                string smtpuser = string.Empty;
                string smtppass = string.Empty;
                string smtpport = string.Empty;
                smtp1 = "smtp-mail.outlook.com";
                smtpuser = "agustinatrinidad@hotmail.es";
                smtppass = "Agust!naTrinidad92*";
                smtpport = "587";

                string fromAddress = smtpuser;
                string mailPassword = smtppass;
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(smtpport);
                client.Host = smtp1;
                client.EnableSsl = true;
                client.Timeout = 20000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(fromAddress, mailPassword);
                var send_mail = new MailMessage();
                send_mail.IsBodyHtml = true;
                send_mail.From = new MailAddress(fromAddress);
                send_mail.To.Add(new MailAddress(correoPara));
                send_mail.Subject = asunto;
                send_mail.Body = mensaje;
                client.Send(send_mail);
                retorno = true;
            }
            catch (Exception ex)
            {
                Logger.Log.Error("ERROR EN APLICACION ENVIAR CORREO " + ex.Message);
                retorno = false;
            }

            return retorno;
        }
        private static bool EnviarCorreo(string mensaje, string usuario, string password, string asunto, List<string> direcciones)
        {
            bool retorno = false;

            try
            {

                string smtp1 = string.Empty;
                string smtpuser = string.Empty;
                string smtppass = string.Empty;
                string smtpport = string.Empty;
                smtp1 = "smtp-mail.outlook.com";
                smtpuser = "agustinatrinidad@hotmail.es";
                smtppass = "Agust!naTrinidad92*";
                smtpport = "587";

                string fromAddress = smtpuser;
                string mailPassword = smtppass;
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(smtpport);
                client.Host = smtp1;
                client.EnableSsl = true;
                client.Timeout = 20000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(fromAddress, mailPassword);
                var send_mail = new MailMessage();
                send_mail.IsBodyHtml = true;
                send_mail.From = new MailAddress(fromAddress);
                foreach (var item in direcciones)
                    send_mail.To.Add(item);
                send_mail.Subject = asunto;
                send_mail.Body = mensaje;
                client.Send(send_mail);
                retorno = true;
            }
            catch (Exception ex)
            {
                Logger.Log.Error("ERROR EN APLICACION ENVIAR CORREO " + ex.Message);
                retorno = false;
            }

            return retorno;
        }

        public static async void SendEmailToContacts()
        {
            int cantSend = 0;
            while (true)
            {
                cantSend++;
                NotificacionesProximaConsulta();
                EnviarNotificaciones();
                EnviarPromociones();
                Logger.Log.InfoFormat("Sending email #{0}", cantSend);
                //1min = 60000 milseg
                await Task.Delay(TimeSpan.FromMinutes(10));
                //await Task.Delay(TimeSpan.FromHours(6)); // We are doing something hard here
            }
        }
       
        
    }
}