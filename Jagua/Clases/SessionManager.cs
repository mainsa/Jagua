﻿using Jagua.Models;
using JaguaEntityModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace Jagua.Clases
{
    /// <summary>
    /// Administrador de session
    /// <autor> Martin Torres</autor>
    /// </summary>
    public static class SessionManager
    {

        /// <summary>
        /// Propiedad el cual es accesible sin instancia y determina  hay ssession valida
        /// </summary>
        public static bool isSessionValid
        {
            get
            {
                return ControlSession();
            }
        }

        /// <summary>
        /// Esta propiedad optiene el Ip Addres del caller.
        /// </summary>
        public static string getRemoteAddresApp
        {
            get
            {
                return HttpContext.Current.Session.SessionID;
            }
        }
        /// <summary>
        /// Esta propiedad Obtiene los datos (GetFileName(FilePath)) de con el que obtenemos el nombre de la pagina que ingresa al sitio
        /// </summary>
        public static string getGetFileName
        {
            get
            {
                return System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.FilePath);
            }
        }
        /// <summary>
        /// Esta propiedad retorna datoss del usuario logueado
        /// </summary>
        public static usuarios SessionData
        {
            get
            {
                usuarios retorno = new usuarios();               
                if (HttpContext.Current.Session["Usuario_" + getRemoteAddresApp] != null)
                {
                    retorno = (usuarios)HttpContext.Current.Session["Usuario_" + getRemoteAddresApp];
                }
                return retorno;
            }

        }        

        /// <summary>
        /// Este metodo indica si los valores de cache session estan activos corresponde a session activa , caso contrario la session expiro
        /// </summary>
        /// <returns></returns>
        private static bool ControlSession()
        {
            bool retorno = true;
            try
            {
                if (HttpContext.Current.Session["Usuario_" + getRemoteAddresApp] != null)
                {
                    retorno = true;
                }
                else
                {
                    retorno = false;
                }
            }
            catch (Exception e)
            {
                retorno = false;
            }
            return retorno;
        }
        public static PermisosModels ValidarPermisos(string pagina)
        {
            JaguaEntities db = new JaguaEntities();
            PermisosModels result = new PermisosModels();
            if(isSessionValid)
            {
                try
                {
                    string query = "SELECT ur.id_usuario_rol, ur.id_rol, ur.id_usuario, r.nombre, rp.id_rol_pagina, " +
                        " rp.id_pagina, p.pagina,p.url, rp.insertar, rp.modificar, rp.eliminar, rp.ejecutar " +
                        " FROM usuario_rol AS ur " +
                        " INNER JOIN rol AS r ON ur.id_rol = r.id_rol " +
                        " INNER JOIN rol_pagina AS rp ON r.id_rol = rp.id_rol " +
                        " INNER JOIN paginas AS p ON rp.id_pagina = p.id_pagina " +
                        " WHERE (ur.id_usuario = " + SessionData.id_usuario + " and p.pagina = '" + pagina + "') ";
                    var datos = db.Database.SqlQuery<PermisosModels>(query).FirstOrDefaultAsync();

                    result = datos.Result;
                }
                catch (Exception ex)
                {
                    Logger.Log.Error("ERROR ValidarPermisos " + ex.InnerException);
                    result = new PermisosModels();

                }
                finally {
                    db.Dispose();
                }
            }
            return result;
        }
        public static List<PermisosModels> ValidarPermisos()
        {
            JaguaEntities db = new JaguaEntities();
            List<PermisosModels> result = new List<PermisosModels>();
            if (isSessionValid)
            {
                try
                {
                    string query = "SELECT ur.id_usuario_rol, ur.id_rol, ur.id_usuario, r.nombre, rp.id_rol_pagina, " +
                        " rp.id_pagina, p.pagina,p.url, rp.insertar, rp.modificar, rp.eliminar, rp.ejecutar " +
                        " FROM usuario_rol AS ur " +
                        " INNER JOIN rol AS r ON ur.id_rol = r.id_rol " +
                        " INNER JOIN rol_pagina AS rp ON r.id_rol = rp.id_rol " +
                        " INNER JOIN paginas AS p ON rp.id_pagina = p.id_pagina " +
                        " WHERE (ur.id_usuario = " + SessionData.id_usuario + ") ";
                    var datos = db.Database.SqlQuery<PermisosModels>(query).ToListAsync();

                    result = datos.Result;
                }
                catch (Exception ex)
                {
                    Logger.Log.Error("ERROR ValidarPermisos " + ex.InnerException);
                    result = new List<PermisosModels>();

                }
                finally
                {
                    db.Dispose();
                }
            }
            return result;
        }
    }
}