﻿using JaguaEntityModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Jagua.Clases
{
    public class Puntajes
    {
        public static bool VerificarPuntajes(int idCleinte, int idVenta)
        {
            bool retorno = false;

            using (var context = new JaguaEntities())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        //El valor a calcular es de 10.000 gs esta en el webconfig se puede cambiar.
                        //SI SE CREA UNA TABLA DE PARAMETROS OBTENER Y REEMPLAZA AQUI 
                        //decimal ValorPuntos = decimal.Parse(System.Configuration.ConfigurationManager.AppSettings["ValorPuntos"]);
                        decimal ValorPuntos = context.parametros.ToList().Count() > 0 ? Convert.ToDecimal(context.parametros.OrderByDescending(x => x.id_parametro).First().monto): 0;
                        decimal totalComp = 0;
                        decimal puntajePar = context.parametros.ToList().Count() > 0 ? Convert.ToDecimal(context.parametros.OrderByDescending(x => x.id_parametro).First().puntos) : 0;
                        int totalpuntos = 0;
                        int puntos_ = 0;
                        puntaje_cabecera pc;
                        puntaje_detalle pd;
                        //buscar las ventas del cliente y sumar los puntos
                        var clientes = context.cliente.Where(t => t.estado.Equals("A") && t.id_cliente == idCleinte).ToList();//Todos los clientes Activos eb este caso tenemos el id cliente
                        foreach (var item in clientes)
                        {
                            //todas las ventas por clientes, filtrado por id cliente y el id de la venta
                            var ventas = context.ventas_cabecera.Where(t => t.id_cliente == item.id_cliente && t.id_ventas_cabecera == idVenta).ToList();
                            if (ventas.Any())//si existe
                            {
                                foreach (var itm in ventas)
                                {
                                    //preguntamos si es que ya se inserto esta venta en la tabla puntaje_detalle, si no se inserto entra en el if
                                    if (!context.puntaje_detalle.Any(t => t.id_venta.Equals(itm.id_ventas_cabecera)))
                                    {
                                        //preguntamos si tiene detalle la venta y si el subtotal en mayor o igual a 10.000
                                        if (itm.ventas_detalle.Any(t => t.id_venta_cabecera.Equals(itm.id_ventas_cabecera) &&
                                        t.subtotal.Value >= ValorPuntos))
                                        {
                                            //CREAMOS LA CABECERA DE PUNTAJES
                                            pc = new puntaje_cabecera();
                                            pc.id_cliente = item.id_cliente;
                                            pc.cantidad_puntos = 0;//DESPUES DE SUMAR EL DETALLE LO VAMOS A ACTUALIZAR
                                            pc.feha_alta = DateTime.Now;
                                            pc.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                            context.puntaje_cabecera.Add(pc);
                                            context.SaveChanges();

                                            totalpuntos = 0;
                                            foreach (var idet in itm.ventas_detalle)
                                            {
                                                //preguntamos si el subtotal en mayor o igual a 10.000 PARA PODER SUMAR
                                                if (idet.subtotal.Value >= ValorPuntos)
                                                {
                                                    totalComp = idet.subtotal.Value / ValorPuntos * puntajePar;//DIVIDIMOS PARA SABER CUANTOS PUNTOS TIENE
                                                    puntos_ = Convert.ToInt32(Math.Floor(totalComp));//CONVERTIMOS A INT
                                                    //CREAMOS UN DETALLE DEL PUNTAJE POR VENTAS DETALLE
                                                    pd = new puntaje_detalle();
                                                    pd.id_puntaje = pc.id_puntaje;
                                                    pd.id_producto = idet.id_producto.Value;
                                                    pd.cantidad_puntos = puntos_;//PUNTOS 
                                                    pd.fecha_alta = DateTime.Now;
                                                    pd.id_venta = itm.id_ventas_cabecera;
                                                    pd.usuario_alta = SessionManager.SessionData.id_usuario.ToString();
                                                    context.puntaje_detalle.Add(pd);
                                                    context.SaveChanges();
                                                    totalpuntos += puntos_;//SUMAMOS PARA DESPUES ACTUALIZAR EN EL CABECERA
                                                }
                                            }
                                            //ACTUALIZAMOS LA CABECERA CON EL TOTAL DE PUNTOS DEL DETALLE
                                            pc.cantidad_puntos = totalpuntos;
                                            context.Entry(pc).State = EntityState.Modified;
                                            context.SaveChanges();
                                            //-----------------------------------------------------------
                                            ////VERIFICAR LOS PUNTOS CANJEADOS POR ID PUNTAJE
                                            //var puntosCanjeados = context.puntaje_canjeado.Where(t => t.id_puntaje.Equals(pc.id_puntaje)).ToList();
                                            //int puntosCanj = 0;
                                            //if (puntosCanjeados.Any())//SI EXISTE ENTRA
                                            //{
                                            //    puntosCanj = puntosCanjeados.Sum(t => t.cantidad).Value;//SUMA DE PUNTOS CANJEADOS
                                            //    //RESTAMOS LOS PUNTOS CANJEADOS
                                            //    if (totalpuntos >= puntosCanj)//PARA EVITAR NEGATIVOS PREGUNTAMOS CUAL ES MAYOR
                                            //        pc.cantidad_puntos = totalpuntos - puntosCanj;
                                            //    else
                                            //        pc.cantidad_puntos = puntosCanj - totalpuntos;
                                            //    context.Entry(pc).State = EntityState.Modified;
                                            //    context.SaveChanges();
                                            //}
                                            //else
                                            //{
                                            ////SI NO EXISTE CANJES DIRECTO GUARDA EL TOTAL DE PUTNOS
                                            //pc.cantidad_puntos = totalpuntos;
                                            //context.Entry(pc).State = EntityState.Modified;
                                            //context.SaveChanges();
                                            //}

                                        }
                                    }
                                }

                            }
                        }
                        retorno = true;
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        retorno = false;
                        Logger.Log.ErrorFormat("ERROR EnviarNotificaciones(): {0}, {1}", ex.Message, ex.InnerException);
                    }
                    finally
                    {
                        //db.Dispose();
                    }
                }
            }
            return retorno;
        }
        //public static async void ProcesoVerificarPuntajes()
        //{
        //    while (true)
        //    {
        //        Puntajes.VerificarPuntajes();
        //        //1min = 60000 milseg
        //        await Task.Delay(TimeSpan.FromSeconds(20));
        //        //await Task.Delay(TimeSpan.FromHours(6)); // We are doing something hard here
        //    }
        //}
    }
}
