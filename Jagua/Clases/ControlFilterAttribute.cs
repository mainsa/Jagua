﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Jagua.Clases
{
    /// <summary>
    ///
    /// </summary>
    public class ControlFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {          
            if (!SessionManager.isSessionValid)
            {
                HttpContext.Current.Response.Redirect("~/Login/Index");
            }
            else
            {
                // seccion apra validacion de controladores/metodos por perfil
                var descriptor = filterContext.ActionDescriptor;
                var actionName = descriptor.ActionName;
                var controllerName = descriptor.ControllerDescriptor.ControllerName;

            }

        }

    }
}