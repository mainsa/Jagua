﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jagua.Clases
{
    public class Reportes
    {
        #region ArqueosCaja
        public class ArqueosCaja
        {
            public int id_arqueo { get; set; }
            public int? id_caja { get; set; }
            public string descripcion_caja { get; set; }
            public DateTime? fecha_apertura { get; set; }
            public DateTime? fecha_cierre { get; set; }
            public string usuario_apertura { get; set; }
            public string usuario_cierre { get; set; }
            public int? monto_inicial { get; set; }
            public string observaciones { get; set; }
            public int? monto_caja { get; set; }
            public int? recaudacion { get; set; }
            public int? faltante { get; set; }
            public string usuario_impresion { get; set; }

        }
        #endregion

        #region Stock
        public class Stock
        {
            public int id_producto { get; set; }
            public string codigo_barra { get; set; }
            public string categoria { get; set; }
            public string marca { get; set; }
            public string descripcion { get; set; }
            public int stock { get; set; }
            public int precio { get; set; }
            public int precio_ult_compra { get; set; }
        }
        #endregion

    }
}