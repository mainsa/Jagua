﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public class Historial
    {
        public int id { get; set; }
		public int id_paciente { get; set; }
		public string descripcion { get; set; }
        public DateTime? fecha { get; set; }
		public string resultado { get; set; }
        public string procedimiento { get; set; }
	}
}