﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public class Comentarios
    {
        public partial class comentarios
        {
            [Key]
            public int id { get; set; }
            public string contenido { get; set; }
            public string autor { get; set; }
            public Nullable<int> blogPostId { get; set; }

            public virtual blogPost blogPost { get; set; }
        }
    }
}