﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public class Stock
    {
        [Key]
        public int id_producto { get; set; }
        public string codigo_barra { get; set; }
        public int? id_cat_producto { get; set; }
        public string categoria { get; set; }
        public string marca { get; set; }
        public string descripcion { get; set; }
		public decimal? stock { get; set; }
        public int? precio { get; set; }
        public int? precio_ult_compra { get; set; }
    }
}