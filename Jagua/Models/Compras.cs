﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public class Compras
    {
        public int id_compras { get; set; }
        public DateTime fecha { get; set; }
        public string nro_factura { get; set; }
        public string ruc { get; set; }
        public string razon_social { get; set; }
        public string tipo_pago { get; set; }
        public int importe_total { get; set; }
		public string estado { get; set; }
        public string usuario_alta { get; set; }
        public DateTime? fecha_alta { get; set; }
    }
}