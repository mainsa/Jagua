﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public class Aperturas
    {
        public int? IdApertura { get; set; }
        public int IdCaja { get; set; }
        public string Caja { get; set; }
        public DateTime? FechaApertura { get; set; }
        public DateTime? FechaCierre { get; set; }
        public string UsuarioApertura { get; set; }
        public string UsuarioCierre { get; set; }
    }
}