﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public partial class blogPost
    {
        public int id { get; set; }
        public string titulo { get; set; }
        public string contenido { get; set; }
        public string autor { get; set; }
        public Nullable<System.DateTime> publicacion { get; set; }
        public List<Comentarios> comentarios { get; set; }
    }
}