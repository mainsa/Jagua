﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using JaguaEntityModel;

namespace Jagua.Models
{
    public class CitaModel
    {

    }

    public class CitaCabecera
    {
        [Key]
        public int id_cita { get; set; }
        public int id_usuario_alta { get; set; }
        public int id_cliente { get; set; }
        public System.DateTime fecha_cita { get; set; }
        public System.TimeSpan hora { get; set; }
        public int id_paciente { get; set; }
        public int id_estado { get; set; }
    }

    public class CitaDetalle
    {
        [Key]
        public int id_cita_detalle { get; set; }
        public Nullable<int> id_cita { get; set; }
        public Nullable<int> id_servicio { get; set; }
        public Nullable<int> id_encargado { get; set; }
    }

}