﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public class Ticket_Cabecera
    {
        public Ticket_Cabecera()
        {
        }
        public string id_ventas_cabecera { get; set; }
        public string fecha { get; set; }
        public string caja { get; set; }
        public string comprobante { get; set; }
        public string cliente { get; set; }
        public string cant_articulos { get; set; }
        public string medio_pago { get; set; }
        public string importe_total { get; set; }
        public string usuario { get; set; }
    }

    public class Ticket_Detalle
    {
        public Ticket_Detalle()
        {
        }
        public string id_venta_cabecera { get; set; }
        public string nro_item { get; set; }
        public string id_producto { get; set; }
        public string codigo_barra { get; set; }
        public string descripcion { get; set; }
        public string cantidad { get; set; }
        public string precio { get; set; }
        public string iva5 { get; set; }
        public string iva10 { get; set; }
        public string gravadas { get; set; }
        public string exentas { get; set; }
        public string subtotal { get; set; }
    }
}