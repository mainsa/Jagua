﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Jagua.Models
{
    public class PromocionCabecera
    {
        public int id_promocion_cabecera { get; set; }
        public int puntos { get; set; }
        public string fecha_desde { get; set; }
        public string fecha_hasta { get; set; }
        public string estado { get; set; }
        public string usuario_alta { get; set; }
        public string fecha_alta { get; set; }
        public string usuario_mod { get; set; }
        public string fecha_mod { get; set; }
        public string descripcion { get; set; }
        public string nombre { get; set; }
    }
}