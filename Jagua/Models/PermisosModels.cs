﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Jagua.Models
{
    public partial class PermisosModels
    {
        public int id_usuario_rol { get; set; }
        public int id_rol { get; set; }
        public int id_usuario { get; set; }
        public string nombre { get; set; }
        public int id_rol_pagina { get; set; }
        public int id_pagina { get; set; }
        public string pagina{ get; set; }
        public string url { get; set; }
        public bool insertar { get; set; }
        public bool modificar { get; set; }
        public bool eliminar { get; set; }
        public bool ejecutar { get; set; }
    }
}