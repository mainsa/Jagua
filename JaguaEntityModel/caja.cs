//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JaguaEntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class caja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public caja()
        {
            this.apertura_cierre = new HashSet<apertura_cierre>();
            this.arqueo = new HashSet<arqueo>();
            this.ventas_cabecera = new HashSet<ventas_cabecera>();
        }
    
        public int id_caja { get; set; }
        public string descripcion { get; set; }
        public int id_usuario { get; set; }
        public string estado { get; set; }
        public string usuario_alta { get; set; }
        public Nullable<System.DateTime> fecha_alta { get; set; }
        public string usuario_mod { get; set; }
        public Nullable<System.DateTime> fecha_mod { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<apertura_cierre> apertura_cierre { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<arqueo> arqueo { get; set; }
        public virtual usuarios usuarios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ventas_cabecera> ventas_cabecera { get; set; }
    }
}
