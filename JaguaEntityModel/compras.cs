//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JaguaEntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class compras
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public compras()
        {
            this.compras_detalle = new HashSet<compras_detalle>();
        }
    
        public int id_compras { get; set; }
        public System.DateTime fecha { get; set; }
        public string nro_factura { get; set; }
        public Nullable<int> id_proveedor { get; set; }
        public Nullable<decimal> descuento { get; set; }
        public int importe_total { get; set; }
        public string tipo_pago { get; set; }
        public string estado { get; set; }
        public string usuario_alta { get; set; }
        public Nullable<System.DateTime> fecha_alta { get; set; }
        public string usuario_mod { get; set; }
        public Nullable<System.DateTime> fecha_mod { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<compras_detalle> compras_detalle { get; set; }
        public virtual proveedor proveedor { get; set; }
    }
}
