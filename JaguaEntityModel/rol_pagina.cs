//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JaguaEntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class rol_pagina
    {
        public int id_rol_pagina { get; set; }
        public Nullable<int> id_rol { get; set; }
        public Nullable<int> id_pagina { get; set; }
        public Nullable<bool> insertar { get; set; }
        public Nullable<bool> modificar { get; set; }
        public Nullable<bool> eliminar { get; set; }
        public Nullable<bool> ejecutar { get; set; }
    
        public virtual paginas paginas { get; set; }
        public virtual rol rol { get; set; }
    }
}
