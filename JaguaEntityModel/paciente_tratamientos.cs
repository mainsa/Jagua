//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JaguaEntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class paciente_tratamientos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public paciente_tratamientos()
        {
            this.paciente_tratamientos_detalles = new HashSet<paciente_tratamientos_detalles>();
        }
    
        public int id_ficha_trat { get; set; }
        public Nullable<int> id_ficha { get; set; }
        public int id_paciente { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<int> id_patologia { get; set; }
        public Nullable<int> id_medico { get; set; }
        public string estado_patologia { get; set; }
        public string nombre_tratamiento { get; set; }
        public Nullable<System.DateTime> fecha_inicio { get; set; }
        public Nullable<System.DateTime> fecha_fin { get; set; }
        public string estado { get; set; }
        public string usuario_alta { get; set; }
        public Nullable<System.DateTime> fecha_alta { get; set; }
        public string usuario_mod { get; set; }
        public Nullable<System.DateTime> fecha_mod { get; set; }
    
        public virtual empleado empleado { get; set; }
        public virtual ficha ficha { get; set; }
        public virtual paciente paciente { get; set; }
        public virtual patologia patologia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<paciente_tratamientos_detalles> paciente_tratamientos_detalles { get; set; }
    }
}
