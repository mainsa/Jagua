//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JaguaEntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class producto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public producto()
        {
            this.cita_detalle = new HashSet<cita_detalle>();
            this.compras_detalle = new HashSet<compras_detalle>();
            this.promocion_detalle = new HashSet<promocion_detalle>();
            this.promocion = new HashSet<promocion>();
            this.puntaje_detalle = new HashSet<puntaje_detalle>();
            this.usuario_horarios = new HashSet<usuario_horarios>();
            this.usuario_servicios = new HashSet<usuario_servicios>();
            this.ventas_detalle = new HashSet<ventas_detalle>();
        }
    
        public int id_producto { get; set; }
        public int id_tipo_producto { get; set; }
        public Nullable<int> id_cat_producto { get; set; }
        public string descripcion { get; set; }
        public string codigo_barra { get; set; }
        public string marca { get; set; }
        public Nullable<int> iva { get; set; }
        public Nullable<decimal> stock { get; set; }
        public Nullable<decimal> stock_minimo { get; set; }
        public Nullable<int> precio { get; set; }
        public Nullable<int> precio_ult_compra { get; set; }
        public string controla_stock { get; set; }
        public Nullable<System.TimeSpan> duracion { get; set; }
        public string usuario_alta { get; set; }
        public Nullable<System.DateTime> fecha_alta { get; set; }
        public string usuario_mod { get; set; }
        public Nullable<System.DateTime> fecha_mod { get; set; }
        public string estado { get; set; }
        public byte[] imagen { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cita_detalle> cita_detalle { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<compras_detalle> compras_detalle { get; set; }
        public virtual tipo_producto tipo_producto { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<promocion_detalle> promocion_detalle { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<promocion> promocion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<puntaje_detalle> puntaje_detalle { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usuario_horarios> usuario_horarios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usuario_servicios> usuario_servicios { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ventas_detalle> ventas_detalle { get; set; }
        public virtual categoria_producto categoria_producto { get; set; }
    }
}
