//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JaguaEntityModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class paciente_patologia
    {
        public int id_ficha_pat { get; set; }
        public Nullable<int> id_ficha { get; set; }
        public Nullable<int> id_paciente { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<int> id_patologia { get; set; }
        public Nullable<System.DateTime> fecha_diagnostico { get; set; }
        public string estado_patologia { get; set; }
        public string usuario_alta { get; set; }
        public Nullable<System.DateTime> fecha_alta { get; set; }
        public string usuario_mod { get; set; }
        public Nullable<System.DateTime> fecha_mod { get; set; }
        public string estado { get; set; }
    
        public virtual ficha ficha { get; set; }
        public virtual paciente paciente { get; set; }
        public virtual patologia patologia { get; set; }
    }
}
